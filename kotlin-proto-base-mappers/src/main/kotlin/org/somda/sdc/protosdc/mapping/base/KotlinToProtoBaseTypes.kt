package org.somda.sdc.protosdc.mapping.base

import com.google.protobuf.*
import org.apache.logging.log4j.kotlin.Logging
import java.math.BigDecimal
import java.net.URI
import java.time.*
import java.time.Duration
import javax.xml.namespace.QName

@Suppress("unused", "MemberVisibilityCanBePrivate")
class KotlinToProtoBaseTypes {

    companion object: Logging {
        fun mapURI(uri: URI): String {
            return uri.toString()
        }

        fun mapURIOptional(uri: URI): StringValue {
            return mapToStringValue(mapURI(uri))
        }

        fun mapToUInt32(value: Int): UInt32Value {
            return UInt32Value.of(value)
        }

        fun mapToUInt64(value: Long): UInt64Value {
            return UInt64Value.of(value)
        }

        fun mapToInt32Value(value: Int): Int32Value {
            return Int32Value.of(value)
        }

        fun mapToInt64Value(value: Long): Int64Value {
            return Int64Value.of(value)
        }

        fun mapToStringValue(value: String): StringValue {
            return StringValue.of(value)
        }

        fun mapToBoolValue(value: Boolean): BoolValue {
            return BoolValue.of(value)
        }

        fun mapBigDecimal(value: BigDecimal): String {
            return value.toPlainString()
        }

        fun mapBigDecimalOptional(value: BigDecimal): StringValue {
            return mapToStringValue(mapBigDecimal(value))
        }

        fun mapQName(value: QName): String {
            return value.toString()
        }

        fun mapQNameOptional(value: QName): StringValue {
            return mapToStringValue(mapQName(value))
        }

        fun mapDuration(value: Duration): com.google.protobuf.Duration {
            return com.google.protobuf.Duration.newBuilder().setSeconds(value.seconds).setNanos(value.nano).build()
        }

        fun mapLocalDateTime(value: LocalDateTime): String {
            return value.toString() // TODO: Correct?
        }

        fun mapLocalDateTimeOptional(value: LocalDateTime): StringValue {
            return mapToStringValue(mapLocalDateTime(value))
        }

        fun mapLocalDate(value: LocalDate): String {
            return value.toString() // TODO: Correct?
        }

        fun mapLocalDateOptional(value: LocalDate): StringValue {
            return mapToStringValue(mapLocalDate(value))
        }

        fun mapYearMonth(value: YearMonth): String {
            return value.toString() // TODO: Correct?
        }

        fun mapYearMonthOptional(value: YearMonth): StringValue {
            return mapToStringValue(mapYearMonth(value))
        }

        fun mapYear(value: Year): String {
            return value.toString() // TODO: Correct?
        }

        fun mapYearOptional(value: Year): StringValue {
            return mapToStringValue(mapYear(value))
        }

    }
}