package org.somda.sdc.protosdc.mapping.base

import com.google.protobuf.*
import org.apache.logging.log4j.kotlin.Logging
import java.lang.Exception
import java.math.BigDecimal
import java.math.BigInteger
import java.net.URI
import java.time.*
import java.time.Duration
import javax.xml.namespace.QName

@Suppress("MemberVisibilityCanBePrivate", "unused")
class ProtoToKotlinBaseTypes {

    companion object: Logging {
        fun mapURI(uri: String): URI {
            return try {
                URI(uri)
            } catch (e: Exception) {
                logger.error { "Error mapping to URI: $e" }
                throw e
            }
        }

        fun mapUInt64Value(value: UInt64Value): Long {
            return value.value
        }

        fun mapUInt32Value(value: UInt32Value): Int {
            return value.value
        }

        fun mapStringValue(value: StringValue): String {
            return value.value
        }

        fun mapInt64Value(value: Int64Value): Long {
            return value.value
        }

        fun mapInt32Value(value: Int32Value): Int {
            return value.value
        }

        fun mapBoolValue(value: BoolValue): Boolean {
            return value.value
        }

        fun mapDuration(value: com.google.protobuf.Duration): Duration {
            return Duration.ofSeconds(value.seconds, value.nanos.toLong())
        }

        fun mapBigInteger(value: String): BigInteger {
            return BigInteger(value)
        }

        fun mapOptionalQName(value: StringValue): QName {
            return mapQName(mapStringValue(value))
        }

        fun mapBigDecimal(value: String): BigDecimal {
            return BigDecimal(value)
        }

        fun mapOptionalBigDecimal(value: StringValue): BigDecimal {
            return BigDecimal(mapStringValue(value))
        }

        fun mapOptionalUri(value: StringValue): URI {
            return mapURI(mapStringValue(value))
        }

        fun mapLocalDateTime(value: String): LocalDateTime {
            return LocalDateTime.parse(value)
        }

        fun mapLocalDate(value: String): LocalDate {
            return LocalDate.parse(value)
        }

        fun mapYearMonth(value: String): YearMonth {
            return YearMonth.parse(value)
        }

        fun mapYear(value: String): Year {
            return Year.parse(value)
        }

        fun mapQName(value: String): QName {
            return QName.valueOf(value)
        }

        fun mapOptionalLocalDateTime(value: StringValue): LocalDateTime {
            return mapLocalDateTime(value.value)
        }
    }

}