plugins {
    `java-library`
    id("org.somda.sdc.proto_converter.deploy")
}

dependencies {
    implementation(group = "com.google.protobuf", name = "protobuf-java", version = "3.15.5")
}