package org.somda.protosdc_converter.converter

import org.apache.logging.log4j.kotlin.Logging
import org.somda.protosdc_converter.converter.Proto3Generator.Companion.camelCaseToConstant
import org.somda.protosdc_converter.xmlprocessor.BaseNode
import org.somda.protosdc_converter.xmlprocessor.NodeType
import org.somda.protosdc_converter.xmlprocessor.OutputLanguage
import org.somda.sdc.protosdc.mapping.base.KotlinToProtoBaseTypes
import org.somda.sdc.protosdc.mapping.base.ProtoToKotlinBaseTypes
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.PrintWriter
import kotlin.reflect.KFunction
import kotlin.reflect.full.companionObject
import kotlin.reflect.full.declaredFunctions

typealias Mapping = Pair<String, String>
typealias Mapper = String // just to indicate this must be a fully qualified name

class KotlinProtoMapping(
    val tree: BaseNode,
    val protoCustomTypesTree: BaseNode,
    val mappingPackage: String
) {

    companion object : Logging {

        const val GOOGLE_JAVA_PREFIX = "com.google.protobuf"
        const val GOOGLE_PROTO_PREFIX = "google.protobuf"
        const val KOTLIN_PREFIX = "kotlin"

        private const val INDENT = "    "
        private const val VALUE = "value"
        private const val PROTO_LIST_SUFFIX = "List"
        private const val TO_LIST = "toList()"
        private const val MAP = "map"
        private const val IT = "it"
        private const val CASE = "Case"
        private const val CHOICE = KotlinGenerator.CHOICE
        private const val BUILDER = "builder"
        private const val BUILD = "build()"
        private const val RETURN = "return"
        private const val VAL = "val"
        private const val NEW_BUILDER = "newBuilder()"
        private const val ADD_ALL = "addAll"

        fun classStart(indent: Int, className: String) = "${INDENT.repeat(indent)}class $className {"
        fun classEnd(indent: Int) = "${INDENT.repeat(indent)}}"

        fun companionStart(indent: Int) = "${INDENT.repeat(indent)}companion object {"
        fun companionEnd(indent: Int) = "${INDENT.repeat(indent)}}"

        fun mapFunctionName(functionName: String) = "map_$functionName"

        fun mapFunctionStart(indent: Int, functionName: String, targetType: String, sourceType: String) =
            "${INDENT.repeat(indent)}fun $functionName(value: $sourceType): $targetType {"

        fun mapFunctionEnd(indent: Int) = "${INDENT.repeat(indent)}}"

        fun kotlinReturnStart(indent: Int, targetType: String) = "${INDENT.repeat(indent)}return $targetType("
        fun kotlinReturnEnd(indent: Int) = "${INDENT.repeat(indent)})"

        fun kotlinFromProtoField(indent: Int, kotlinField: String, protoField: String, mappingFunction: String? = null): String {
            val prefix = "${INDENT.repeat(indent)}$kotlinField = "
            return when (mappingFunction != null) {
                true -> "$prefix$mappingFunction($VALUE.$protoField),"
                false -> "$prefix$VALUE.$protoField,"
            }
        }

        fun kotlinFromProtoList(indent: Int, kotlinField: String, protoField: String, mappingFunction: String? = null): String {
            val prefix = "${INDENT.repeat(indent)}$kotlinField = $VALUE.$protoField$PROTO_LIST_SUFFIX.$TO_LIST.$MAP { "
            val body = when (mappingFunction != null) {
                true -> "$mappingFunction($IT)"
                false -> IT
            }
            return "$prefix$body }.$TO_LIST,"
        }

        fun protoFromKotlinList(indent: Int, kotlinField: String, protoField: String, mappingFunction: String? = null): String {
            val prefix = "${INDENT.repeat(indent)}$BUILDER.$ADD_ALL$protoField($VALUE.$kotlinField.$MAP { "
            val body = mappingFunction?.let { "$it($IT)" } ?: IT
            return "$prefix$body }.$TO_LIST)"
        }

        fun protoFromKotlinListOptional(indent: Int, kotlinField: String, protoField: String, mappingFunction: String? = null): String {
            val lst = "lst"
            val prefix = "${INDENT.repeat(indent)}$VALUE.$kotlinField?.let { $lst -> $BUILDER.$ADD_ALL$protoField(lst.$MAP { "
            val body = mappingFunction?.let { "$it($IT)" } ?: IT
            return "$prefix$body }.$TO_LIST) }"
        }

        fun kotlinFromProtoOneOfStart(indent: Int, protoTypeName: String): String {
            return "${INDENT.repeat(indent)}return when ($VALUE.${KotlinGenerator.toParameterName(protoTypeName)}$CASE) {"
        }

        fun protoFromKotlinOneOfStart(indent: Int): String {
            return "${INDENT.repeat(indent)}return when ($VALUE) {"
        }

        fun kotlinFromProtoOneOfCase(indent: Int, protoCase: String, protoField: String, kotlinChoice: String, mappingFunction: String? = null): String {
            val prefix = "${INDENT.repeat(indent)}$protoCase -> $kotlinChoice("
            val fieldAccess = "$VALUE.$protoField"
            val main = mappingFunction?.let { "$it($fieldAccess)" } ?: fieldAccess
            return "$prefix$main)"
        }

        fun protoFromKotlinOneOfCase(
            indent: Int,
            kotlinChoice: String, kotlinField: String,
            protoCase: String, protoField: String,
            mappingFunction: String? = null
        ): String {
            val prefix = "${INDENT.repeat(indent)}is $kotlinChoice -> $protoCase.$NEW_BUILDER.set$protoField("
            val body = mappingFunction?.let { "$mappingFunction($VALUE.$kotlinField)" } ?: "$VALUE.$kotlinField"
            return "$prefix$body).$BUILD"
        }

        fun kotlinFromOptionalProto(indent: Int, kotlinField: String, protoField: String): String {
            return "${INDENT.repeat(indent)}$kotlinField = when ($VALUE.has$protoField()) {"
        }

        fun kotlinFromOptionalProtoTrue(indent: Int, protoField: String, mappingFunction: String?): String {
            val prefix = "${INDENT.repeat(indent)}true -> "
            return mappingFunction?.let { "$prefix$mappingFunction($VALUE.$protoField)" } ?: "$prefix$protoField"
        }
        fun kotlinFromOptionalProtoFalse(indent: Int) = "${INDENT.repeat(indent)}false -> null"

        fun kotlinFromOptionalProtoEnd(indent: Int) = "${INDENT.repeat(indent)}},"


        fun kotlinFromProtoEnumStart(indent: Int) =
            "${INDENT.repeat(indent)}$RETURN when ($VALUE) {"

        fun kotlinFromProtoEnumCase(
            indent: Int,
            sourceType: String,
            sourceValue: String,
            targetType: String,
            targetValue: String
        ) = "${INDENT.repeat(indent)}$sourceType.$sourceValue -> $targetType.$targetValue"

        fun kotlinFromProtoEnumElse(indent: Int) = "${INDENT.repeat(indent)}else -> throw Exception(\"Unknown enum value \$value\")"

        fun kotlinFromProtoEnumEnd(indent: Int) = "${INDENT.repeat(indent)}}"


        fun protoBuilderStart(indent: Int, builderName: String): String {
            return "${INDENT.repeat(indent)}$VAL $BUILDER = $builderName.$NEW_BUILDER"
        }

        fun protoBuilderEnd(indent: Int): String {
            return "${INDENT.repeat(indent)}$RETURN $BUILDER.$BUILD"
        }

        fun protoFieldFromOptionalKotlin(indent: Int, kotlinField: String, protoField: String, mappingFunction: String?): String {
            val field = "field"
            val prefix = "${INDENT.repeat(indent)}$VALUE.$kotlinField?.let { $field -> $BUILDER.$protoField = "
            val body = when (mappingFunction != null) {
                true -> "$mappingFunction($field)"
                false -> field
            }
            return "$prefix$body }"
        }

        fun protoFieldFromKotlin(indent: Int, kotlinField: String, protoField: String, mappingFunction: String?): String {
            val prefix = "${INDENT.repeat(indent)}$BUILDER.$protoField = "
            val body = when (mappingFunction != null) {
                true -> "$mappingFunction($VALUE.$kotlinField)"
                false -> "$VALUE.$kotlinField"
            }
            return "$prefix$body"
        }

        fun packageToFunctionSuffix(packageName: String): String {
            return packageName.replace(".", "_")
        }

        val PROTO_COMPILED_TO_KOTLIN_MAP = mapOf(
            "string" to "kotlin.String",
            "int64" to "kotlin.Long",
            "uint64" to "kotlin.Long",
            "bool" to "kotlin.Boolean",
            "uint32" to "kotlin.Int",
            "int32" to "kotlin.Int",
        )

    }

    private val protoToKotlinMapperMap: MutableMap<Mapping, Mapper> = mutableMapOf()
    private val kotlinToProtoMapperMap: MutableMap<Mapping, Mapper> = mutableMapOf()

    private fun registerSignature(fullyQualifiedName: String, func: KFunction<*>, mapperMap: MutableMap<Mapping, Mapper>) {
        // why can't I access the name in a sane manner somewhere? Why the is toString needed? This is stupid.
        val functionName = func.name
        check(func.parameters.size == 2) { "Mappers may only have one parameter! $functionName has ${func.parameters.size}" }
        val actualParam = func.parameters[1]
        val functionParameterName = actualParam.name

        val functionParameterType = actualParam.type.toString()
        val functionReturnType = func.returnType.toString()

        logger.info("$fullyQualifiedName::$functionName($functionParameterName: $functionParameterType): $functionReturnType")

        val fullyQualifiedCall = "$fullyQualifiedName.$functionName"

        val mapping = Mapping(functionParameterType, functionReturnType)

        mapperMap[mapping] = fullyQualifiedCall
    }

    init {

        // find builtin mappers
        run {
            val protoToKotlinFun = ProtoToKotlinBaseTypes::class.companionObject?.declaredFunctions ?: emptyList()
            val fullyQualifiedName = ProtoToKotlinBaseTypes::class.qualifiedName!!

            protoToKotlinFun.forEach { func ->
                registerSignature(fullyQualifiedName, func, protoToKotlinMapperMap)
            }
        }

        run {
            val kotlinToProtoFun = KotlinToProtoBaseTypes::class.companionObject?.declaredFunctions ?: emptyList()
            val fullyQualifiedName = KotlinToProtoBaseTypes::class.qualifiedName!!

            kotlinToProtoFun.forEach { func ->
                registerSignature(fullyQualifiedName, func, kotlinToProtoMapperMap)
            }
        }

        logger.info("Builtin mappers registered")
    }

    private fun getProtoTypeName(proto: Proto3Generator.ProtoEntry): String {
        return when (proto) {
            is Proto3Generator.ProtoEntry.ProtoType -> {
                proto.typeName
            }
            is Proto3Generator.ProtoEntry.ProtoParameter -> {
                getProtoTypeName(proto.protoType)
            }
            is Proto3Generator.ProtoEntry.ProtoEnum -> {
                // TODO LDe: Fully qualify?
                proto.enumName
            }
            is Proto3Generator.ProtoEntry.ProtoOneOf -> {
                proto.oneOfName
            }
        }
    }

    private fun getProtoEntry(node: BaseNode): Proto3Generator.ProtoEntry {
        return node.languageType[OutputLanguage.Proto] as Proto3Generator.ProtoEntry
    }

    private fun getKotlinEntry(node: BaseNode): KotlinGenerator.KotlinEntry {
        return node.languageType[OutputLanguage.Kotlin] as KotlinGenerator.KotlinEntry
    }

    private fun getParentForNode(node: BaseNode): BaseNode? {
        if (tree.children.contains(node)) {
            return tree
        }
        tree.traversePreOrder().forEach { child ->
            // referential equality, yay
            child.children.forEach { megaChild ->
                if (node === megaChild) {
                    return child
                }
            }
        }
        // TODO LDe: This shouldn't be, it's hacky
        protoCustomTypesTree.traversePreOrder().forEach { child ->
            child.children.forEach { megaChild ->
                if (node === megaChild) {
                    return child
                }
            }
        }
        return null
    }

    private fun getNodeForProtoEntry(protoEntry: Proto3Generator.ProtoEntry): BaseNode? {
        tree.traversePreOrder().forEach { child ->
            if (child.nodeType !is NodeType.Root) {
                val nodeProtoEntry = getProtoEntry(child)
                if (protoEntry === nodeProtoEntry) {
                    return child
                }
            }
        }
        protoCustomTypesTree.traversePreOrder().forEach { child ->
            if (child.nodeType !is NodeType.Root) {
                val nodeProtoEntry = getProtoEntry(child)
                if (protoEntry === nodeProtoEntry) {
                    return child
                }
            }
        }
        return null
    }

    private fun getNodeForKotlinEntry(kotlinEntry: KotlinGenerator.KotlinEntry): BaseNode? {
        tree.traversePreOrder().forEach { child ->
            if (child.nodeType !is NodeType.Root) {
                val nodeKotlinEntry = getKotlinEntry(child)
                if (kotlinEntry === nodeKotlinEntry) {
                    return child
                }
            }
        }
        return null
    }

    private fun getFullyQualifiedProtoName(baseNode: BaseNode): String {
        val protoEntry = getProtoEntry(baseNode)
        // every node has a parent, unless you're asking for the root parent, which isn't a valid node for mapping anyway
        val parentOpt = getParentForNode(baseNode)
        val parent = parentOpt!!
        val parentIsRoot = parent.nodeType is NodeType.Root
        when (protoEntry) {
            is Proto3Generator.ProtoEntry.ProtoType -> {
                // if this is a node under the parent, this is correct
                if (parentIsRoot) {
                    return protoEntry.fullyQualifiedName()
                }
                // resolve the parents fully qualified name and return that with the type name appended
                return getFullyQualifiedProtoName(parent) + "." + protoEntry.typeName
            }
            is Proto3Generator.ProtoEntry.ProtoOneOf -> {
                // these literally only exist in the context of their parent, we need to resolve it
                return getFullyQualifiedProtoName(parent) + "." + protoEntry.oneOfName
            }
            is Proto3Generator.ProtoEntry.ProtoEnum -> {
                // these literally only exist in the context of their parent, we need to resolve it
                return getFullyQualifiedProtoName(parent) + "." + protoEntry.enumName
            }
            is Proto3Generator.ProtoEntry.ProtoParameter -> {
                // TODO LDe: Handle case we're a OneOf parameter?
                // find the node of the parameter type
                val protoTypeNode = getNodeForProtoEntry(protoEntry.protoType)
                return getFullyQualifiedProtoName(protoTypeNode!!)
            }
        }
    }

    private fun getFullyQualifiedKotlinName(baseNode: BaseNode): String {
        val kotlinEntry = getKotlinEntry(baseNode)
        // every node has a parent, unless you're asking for the root parent, which isn't a valid node for mapping anyway
        val parent = getParentForNode(baseNode)!!
        val parentIsRoot = parent.nodeType is NodeType.Root

        val parentIsSkip = when (parentIsRoot) {
            false -> when (val parentLanguageType = getKotlinEntry(parent)) {
                is KotlinGenerator.KotlinEntry.DataClass -> parentLanguageType.skipType
                else -> false
            }
            true -> false
        }

        when (kotlinEntry) {
            is KotlinGenerator.KotlinEntry.DataClass -> {
                // if this is a node under the parent, this is correct
                if (parentIsRoot) {
                    // distinguish between builtin types (which implies "kotlin." prefix) and other types
                    return kotlinEntry.packagePath?.let {
                        kotlinEntry.importPath()
                    } ?: KOTLIN_PREFIX + "." + kotlinEntry.importPath()
                }
                // resolve the parents fully qualified name and return that with the type name appended
                return getFullyQualifiedKotlinName(parent) + "." + kotlinEntry.typeName
            }
            is KotlinGenerator.KotlinEntry.KotlinOneOf -> {
                // if the parent is skipped, we are the fully resolved path, else we're just nested
                if (parentIsSkip) {
                    // if the OneOf is unwrapped, it has the exact same name as the parent
                    return getFullyQualifiedKotlinName(parent)
                }
                return getFullyQualifiedKotlinName(parent) + "." + kotlinEntry.oneOfName
            }
            is KotlinGenerator.KotlinEntry.KotlinOneOfParameter -> {
                // fully qualified parent is the suffix here
                val kotlinTypeNode = getNodeForKotlinEntry(kotlinEntry.kotlinType)
                return getFullyQualifiedKotlinName(kotlinTypeNode!!)
            }
            is KotlinGenerator.KotlinEntry.KotlinStringEnumeration -> {
                // fully qualified parent is the suffix here
                return getFullyQualifiedKotlinName(parent) + "." + kotlinEntry.enumName
            }
            is KotlinGenerator.KotlinEntry.KotlinParameter -> {
                val kotlinTypeNode = getNodeForKotlinEntry(kotlinEntry.kotlinType)
                return getFullyQualifiedKotlinName(kotlinTypeNode!!)

            }
        }
    }

    private fun getKotlinFieldName(kotlin: KotlinGenerator.KotlinEntry): String {
        return when (kotlin) {
            is KotlinGenerator.KotlinEntry.KotlinParameter -> {
                kotlin.parameterName
            }
            else -> throw Exception("Handle $kotlin")
        }
    }

    private fun getProtoFieldName(proto: Proto3Generator.ProtoEntry, pascalCase: Boolean = false): String {
        return when (proto) {
            is Proto3Generator.ProtoEntry.ProtoParameter -> {
                // snake to camel case the parameter
                val name = proto.parameterName.split('_')
                    .joinToString("", transform = String::capitalize)
                when (pascalCase) {
                    true -> name
                    false -> KotlinGenerator.toParameterName(name)
                }

            }
            else -> throw Exception("Handle $proto")
        }
    }

    private fun lookupProtoToKotlinType(proto: String): String {
        return PROTO_COMPILED_TO_KOTLIN_MAP[proto] ?: run {
            when (proto.startsWith(GOOGLE_PROTO_PREFIX)) {
                true -> GOOGLE_JAVA_PREFIX + proto.removePrefix(GOOGLE_PROTO_PREFIX)
                false -> proto
            }
        }
    }

    private fun toProtoEnumName(value: String): String {
        return value.camelCaseToConstant()
    }

    private fun processNodeToKotlin(
        node: BaseNode, target: PrintWriter,
        level: Int = 0,
        parentNode: BaseNode? = null
    ) {

        val protoLanguageType = node.languageType[OutputLanguage.Proto] as Proto3Generator.ProtoEntry
        val kotlinLanguageType = node.languageType[OutputLanguage.Kotlin] as KotlinGenerator.KotlinEntry

        // so we need to consider a couple of cases here
        // 1. This is a top level element which has the full name as its type name
        // 2. This is a nested element which is only defined within the context of the parent message
        // 3. This is a parameter which points to
        //    1. A top level element
        //    2. A nested message which is defined within the same context

        val qualifiedProtoTypeName = lookupProtoToKotlinType(getFullyQualifiedProtoName(node))
        val qualifiedKotlinTypeName = getFullyQualifiedKotlinName(node)

        node.clusteredTypes?.let { cluster ->
            when (cluster.isNotEmpty()) {
                true -> {
                    logger.info { "processNodeToKotlin: Encountered cluster ${node.clusteredTypes}" }

                    if (node.clusterHandled[OutputLanguage.ProtoToKotlinMapper] == true) {
                        logger.info { "processNodeToKotlin: Encountered cluster ${node.clusteredTypes} is already handled" }
                        return@let
                    }
                    logger.info { "processNodeToKotlin: Encountered cluster ${node.clusteredTypes} is new" }
                    logger.info { "processNodeToKotlin: Adding cluster mappers all at once to map" }

                    (listOf(node) + cluster).forEach { clusterType ->
                        clusterType.clusterHandled[OutputLanguage.ProtoToKotlinMapper] = true

                        // create mappers
                        val protoName = getFullyQualifiedProtoName(clusterType)
                        val kotlinName = getFullyQualifiedKotlinName(clusterType)

                        val functionName = mapFunctionName(packageToFunctionSuffix(protoName))

                        val clusterTypeMapping =
                            Mapping(protoName, kotlinName)
                        protoToKotlinMapperMap[clusterTypeMapping] = functionName
                    }

                }
                false -> {}
            }
        }

        // lookup if mapper is already present
        val mapper = protoToKotlinMapperMap[Mapping(
            qualifiedProtoTypeName,
            qualifiedKotlinTypeName
        )]

        when (val nodeType = node.nodeType) {
            is NodeType.Message -> {

                if (mapper != null && node.clusteredTypes == null) {
                    logger.error { "Not generating mapper from non clustered Message $qualifiedProtoTypeName to $qualifiedKotlinTypeName, already present" }
                    return
                }

                // separate parameter children from nested message children
                val parameters = node.children.filter { it.nodeType is NodeType.Parameter }.toList()
                val nestedTypes = node.children - parameters

                val kotlinLanguageTypeSkip = when (kotlinLanguageType) {
                    is KotlinGenerator.KotlinEntry.DataClass -> kotlinLanguageType.skipType
                    else -> false
                }

                // check if nested is only a oneof, this is a special case in kotlin
                val isOnlyOneOfWrapper = nestedTypes.all { it.nodeType is NodeType.OneOf }
                        && nestedTypes.isNotEmpty()
                        && kotlinLanguageTypeSkip
                val isOneOf = node.nodeType is NodeType.OneOf

                // we need to generate nested types first
                nestedTypes.forEach { child ->
                    processNodeToKotlin(node = child, target = target, level = level, parentNode = node)
                }

                if (isOnlyOneOfWrapper) {
                    // bail out if this was only a oneof, but create mapper from the msg to the nested type
                    return
                }

                // create mapper
                val functionName = mapFunctionName(packageToFunctionSuffix(qualifiedProtoTypeName))

                protoToKotlinMapperMap[Mapping(
                    qualifiedProtoTypeName,
                    qualifiedKotlinTypeName
                )] = functionName

                target.println(
                    mapFunctionStart(
                        level,
                        functionName = functionName,
                        targetType = qualifiedKotlinTypeName,
                        sourceType = qualifiedProtoTypeName,
                    )
                )

                target.println(
                    kotlinReturnStart(
                        level + 1,
                        targetType = qualifiedKotlinTypeName
                    )
                )

                // only for non oneOfs
                if (!isOneOf) {
                    parameters.forEach { child ->
                        processNodeToKotlin(
                            node = child, target = target, level = level + 2,
                            parentNode = node
                        )
                    }
                }

                target.println(kotlinReturnEnd(level + 1))
                target.println(mapFunctionEnd(level))

            }
            is NodeType.Parameter -> {
                check(qualifiedProtoTypeName == qualifiedKotlinTypeName || mapper != null) {
                    "Cannot map from parameter $qualifiedProtoTypeName to $qualifiedKotlinTypeName, no mapper!"
                }

                when {
                    (!nodeType.optional && nodeType.list) || (nodeType.optional && nodeType.list) -> {
                        // there is no such thing as an optional (nullable) list, so these can be merged
                        target.println(
                            kotlinFromProtoList(
                                indent = level,
                                kotlinField = getKotlinFieldName(kotlinLanguageType),
                                protoField = getProtoFieldName(protoLanguageType),
                                mappingFunction = mapper
                            )
                        )

                    }
                    (nodeType.optional && !nodeType.list) -> {
                        target.println(
                            kotlinFromOptionalProto(
                                indent = level,
                                kotlinField = getKotlinFieldName(kotlinLanguageType),
                                protoField = getProtoFieldName(protoLanguageType, pascalCase = true),
                            )
                        )
                        target.println(
                            kotlinFromOptionalProtoTrue(
                                indent = level + 1,
                                protoField = getProtoFieldName(protoLanguageType),
                                mappingFunction = mapper
                            )
                        )
                        target.println(kotlinFromOptionalProtoFalse(indent = level + 1))
                        target.println(kotlinFromOptionalProtoEnd(indent = level))

                    }
                    (!nodeType.optional && !nodeType.list) -> {
                        //
                        target.println(
                            kotlinFromProtoField(
                                indent = level,
                                kotlinField = getKotlinFieldName(kotlinLanguageType),
                                protoField = getProtoFieldName(protoLanguageType),
                                mappingFunction = mapper
                            )
                        )
                    }
                }

            }

            is NodeType.StringEnumeration -> {
                // create mapper
                val functionName = mapFunctionName(packageToFunctionSuffix(qualifiedProtoTypeName))

                protoToKotlinMapperMap[Mapping(
                    qualifiedProtoTypeName,
                    qualifiedKotlinTypeName
                )] = functionName

                target.println(
                    mapFunctionStart(
                        level,
                        functionName = functionName,
                        targetType = qualifiedKotlinTypeName,
                        sourceType = qualifiedProtoTypeName,
                    )
                )

                target.println(kotlinFromProtoEnumStart(level + 1))

                nodeType.values.forEach { value ->
                    target.println(
                        kotlinFromProtoEnumCase(
                            indent = level + 2,
                            sourceType = qualifiedProtoTypeName,
                            sourceValue = toProtoEnumName(value),
                            targetType = qualifiedKotlinTypeName,
                            targetValue = value
                        )
                    )
                }
                target.println(kotlinFromProtoEnumElse(indent = level + 2))

                target.println(kotlinFromProtoEnumEnd(level + 1))
                target.println(mapFunctionEnd(level))
            }

            is NodeType.BuiltinType -> {
                logger.error { "Handle builtin type ${node.nodeName}" }
            }

            is NodeType.OneOf -> {
                check(getKotlinEntry(node) is KotlinGenerator.KotlinEntry.KotlinOneOf)
                val protoOneOf = getProtoEntry(node) as Proto3Generator.ProtoEntry.ProtoOneOf

                // retrieve the fully qualified parent node name here for proto
                val oneOfQualifiedProtoTypeName = getFullyQualifiedProtoName(parentNode!!)

                // create mapper
                val functionName = mapFunctionName(packageToFunctionSuffix(oneOfQualifiedProtoTypeName))

                protoToKotlinMapperMap[Mapping(
                    oneOfQualifiedProtoTypeName,
                    qualifiedKotlinTypeName
                )] = functionName

                target.println(
                    mapFunctionStart(
                        level,
                        functionName = functionName,
                        targetType = qualifiedKotlinTypeName,
                        sourceType = oneOfQualifiedProtoTypeName,
                    )
                )

                val protoTypeName = getProtoTypeName(protoOneOf)
                val whenName = KotlinGenerator.toParameterName(protoTypeName)

                target.println(kotlinFromProtoOneOfStart(level + 1, whenName))

                node.children.forEach { child ->
                    check(child.nodeType is NodeType.Parameter)

                    val kotlinChild = child.languageType[OutputLanguage.Kotlin]
                    val protoChild = child.languageType[OutputLanguage.Proto]

                    check(kotlinChild is KotlinGenerator.KotlinEntry.KotlinOneOfParameter)
                    check(protoChild is Proto3Generator.ProtoEntry.ProtoParameter)

                    val childProtoTypeName = lookupProtoToKotlinType(getFullyQualifiedProtoName(child))
                    val childKotlinTypeName = getFullyQualifiedKotlinName(child)

                    // generate oneof parameters
                    val childMapper = protoToKotlinMapperMap[Mapping(
                        childProtoTypeName,
                        childKotlinTypeName
                    )]

                    check(childProtoTypeName == childKotlinTypeName || childMapper != null) {
                        "Cannot map from oneof parameter $childProtoTypeName to $childKotlinTypeName, no mapper!"
                    }

                    val protoCaseName = qualifiedProtoTypeName + CASE + "." + protoChild.parameterName.camelCaseToConstant()
                    val kotlinCaseName = qualifiedKotlinTypeName + ".$CHOICE" + KotlinGenerator.typeName(kotlinChild.kotlinType)

                    val protoFieldName = KotlinGenerator.toParameterName(child.nodeName)

                    target.println(
                        kotlinFromProtoOneOfCase(
                            indent = level + 2,
                            protoCase = protoCaseName,
                            protoField = protoFieldName,
                            kotlinChoice = kotlinCaseName,
                            mappingFunction = childMapper
                        )
                    )
                }
                target.println(kotlinFromProtoEnumElse(indent = level + 2))

                target.println(kotlinFromProtoEnumEnd(level + 1))
                target.println(mapFunctionEnd(level))
            }

            else -> throw Exception("Cannot handle nodeType ${node.nodeType}")
        }
    }


    private fun processNodeToProto(
        node: BaseNode, target: PrintWriter,
        level: Int = 0,
        parentNode: BaseNode? = null
    ) {
        val protoLanguageType = node.languageType[OutputLanguage.Proto] as Proto3Generator.ProtoEntry
        val kotlinLanguageType = node.languageType[OutputLanguage.Kotlin] as KotlinGenerator.KotlinEntry

        // so we need to consider a couple of cases here
        // 1. This is a top level element which as the full name as its type name
        // 2. This is a nested element which is only defined within the context of the parent message
        // 3. This is a parameter which points to
        //    1. A top level element
        //    2. A nested message which is defined within the same context

        val qualifiedProtoTypeName = lookupProtoToKotlinType(getFullyQualifiedProtoName(node))
        val qualifiedKotlinTypeName = getFullyQualifiedKotlinName(node)

        node.clusteredTypes?.let { cluster ->
            when (cluster.isNotEmpty()) {
                true -> {
                    logger.info { "processNodeToProto: Encountered cluster ${node.clusteredTypes}" }

                    if (node.clusterHandled[OutputLanguage.KotlinToProtoMapper] == true) {
                        logger.info { "processNodeToProto: Encountered cluster ${node.clusteredTypes} is already handled" }
                        return@let
                    }
                    logger.info { "processNodeToProto: Encountered cluster ${node.clusteredTypes} is new" }
                    logger.info { "processNodeToProto: Adding cluster mappers all at once to map" }

                    (listOf(node) + cluster).forEach { clusterType ->
                        clusterType.clusterHandled[OutputLanguage.KotlinToProtoMapper] = true

                        // create mappers
                        val protoName = getFullyQualifiedProtoName(clusterType)
                        val kotlinName = getFullyQualifiedKotlinName(clusterType)

                        val functionName = mapFunctionName(packageToFunctionSuffix(kotlinName))

                        val clusterTypeMapping =
                            Mapping(kotlinName, protoName)
                        kotlinToProtoMapperMap[clusterTypeMapping] = functionName
                    }

                }
                false -> {}
            }
        }

        // lookup if mapper is already present
        val mapper = kotlinToProtoMapperMap[Mapping(
            qualifiedKotlinTypeName,
            qualifiedProtoTypeName
        )]

        when (val nodeType = node.nodeType) {
            is NodeType.Message -> {

                if (mapper != null && node.clusteredTypes == null) {
                    logger.error { "Not generating mapper from non clustered Message $qualifiedProtoTypeName to $qualifiedKotlinTypeName, already present" }
                    return
                }

                // separate parameter children from nested message children
                val parameters = node.children.filter { it.nodeType is NodeType.Parameter }.toList()
                val nestedTypes = node.children - parameters

                val kotlinLanguageTypeSkip = when (kotlinLanguageType) {
                    is KotlinGenerator.KotlinEntry.DataClass -> kotlinLanguageType.skipType
                    else -> false
                }

                // check if nested is only a oneof, this is a special case in kotlin
                val isOnlyOneOfWrapper = nestedTypes.all { it.nodeType is NodeType.OneOf }
                        && nestedTypes.isNotEmpty()
                        && kotlinLanguageTypeSkip
                val isOneOf = node.nodeType is NodeType.OneOf

                // we need to generate nested types first
                nestedTypes.forEach { child ->
                    processNodeToProto(node = child, target = target, level = level, parentNode = node)
                }

                if (isOnlyOneOfWrapper) {
                    // bail out if this was only a oneof, but create mapper from the msg to the nested type
                    return
                }

                // create mapper
                val functionName = mapFunctionName(packageToFunctionSuffix(qualifiedKotlinTypeName))

                kotlinToProtoMapperMap[Mapping(
                    qualifiedKotlinTypeName,
                    qualifiedProtoTypeName
                )] = functionName

                target.println(
                    mapFunctionStart(
                        level,
                        functionName = functionName,
                        targetType = qualifiedProtoTypeName,
                        sourceType = qualifiedKotlinTypeName,
                    )
                )

                target.println(
                    protoBuilderStart(
                        level + 1,
                        builderName = qualifiedProtoTypeName
                    )
                )

                // only for non oneOfs
                if (!isOneOf) {
                    parameters.forEach { child ->
                        processNodeToProto(
                            node = child, target = target, level = level + 1,
                            parentNode = node
                        )
                    }
                }

                target.println(protoBuilderEnd(level + 1))
                target.println(mapFunctionEnd(level))

            }
            is NodeType.Parameter -> {

                check(qualifiedProtoTypeName == qualifiedKotlinTypeName || mapper != null) {
                    "Cannot map from parameter $qualifiedKotlinTypeName to $qualifiedProtoTypeName, no mapper!"
                }

                when {
                    (nodeType.optional && nodeType.list) || (!nodeType.optional && nodeType.list) -> {
                        // there is no such thing as an optional (nullable) list, so these can be merged

                        target.println(
                            protoFromKotlinList(
                                indent = level,
                                protoField = getProtoFieldName(protoLanguageType, pascalCase = true),
                                kotlinField = getKotlinFieldName(kotlinLanguageType),
                                mappingFunction = mapper
                            )
                        )
                    }
                    (nodeType.optional && !nodeType.list) -> {
                        target.println(
                            protoFieldFromOptionalKotlin(
                                indent = level,
                                protoField = getProtoFieldName(protoLanguageType),
                                kotlinField = getKotlinFieldName(kotlinLanguageType),
                                mappingFunction = mapper
                            )
                        )
                    }
                    (!nodeType.optional && !nodeType.list) -> {
                        target.println(
                            protoFieldFromKotlin(
                                indent = level,
                                protoField = getProtoFieldName(protoLanguageType),
                                kotlinField = getKotlinFieldName(kotlinLanguageType),
                                mappingFunction = mapper
                            )
                        )
                    }
                }

            }
            is NodeType.StringEnumeration -> {

                // create mapper
                val functionName = mapFunctionName(packageToFunctionSuffix(qualifiedKotlinTypeName))

                kotlinToProtoMapperMap[Mapping(
                    qualifiedKotlinTypeName,
                    qualifiedProtoTypeName
                )] = functionName

                target.println(
                    mapFunctionStart(
                        level,
                        functionName = functionName,
                        targetType = qualifiedProtoTypeName,
                        sourceType = qualifiedKotlinTypeName,
                    )
                )

                target.println(kotlinFromProtoEnumStart(level + 1))

                nodeType.values.forEach { value ->
                    target.println(
                        kotlinFromProtoEnumCase(
                            indent = level + 2,
                            sourceType = qualifiedKotlinTypeName,
                            sourceValue = value,
                            targetType = qualifiedProtoTypeName,
                            targetValue = toProtoEnumName(value)
                        )
                    )
                }
                target.println(kotlinFromProtoEnumElse(indent = level + 2))

                target.println(kotlinFromProtoEnumEnd(level + 1))
                target.println(mapFunctionEnd(level))

            }
            is NodeType.BuiltinType -> {
                logger.error { "Handle builtin type ${node.nodeName}" }
            }
            is NodeType.OneOf -> {
                check(getKotlinEntry(node) is KotlinGenerator.KotlinEntry.KotlinOneOf)
                check(getProtoEntry(node) is Proto3Generator.ProtoEntry.ProtoOneOf)

                // retrieve the fully qualified parent node name here for proto
                val oneOfQualifiedProtoTypeName = getFullyQualifiedProtoName(parentNode!!)

                // create mapper
                val functionName = mapFunctionName(packageToFunctionSuffix(qualifiedKotlinTypeName))

                kotlinToProtoMapperMap[Mapping(
                    qualifiedKotlinTypeName,
                    oneOfQualifiedProtoTypeName
                )] = functionName

                target.println(
                    mapFunctionStart(
                        level,
                        functionName = functionName,
                        targetType = oneOfQualifiedProtoTypeName,
                        sourceType = qualifiedKotlinTypeName,
                    )
                )

//                val protoTypeName = getProtoTypeName(protoOneOf)
//                val whenName = KotlinGenerator.toParameterName(protoTypeName)

                target.println(protoFromKotlinOneOfStart(level + 1))

                node.children.forEach { child ->
                    check(child.nodeType is NodeType.Parameter)

                    val kotlinChild = child.languageType[OutputLanguage.Kotlin]
                    val protoChild = child.languageType[OutputLanguage.Proto]

                    check(kotlinChild is KotlinGenerator.KotlinEntry.KotlinOneOfParameter)
                    check(protoChild is Proto3Generator.ProtoEntry.ProtoParameter)

                    val childProtoTypeName = lookupProtoToKotlinType(getFullyQualifiedProtoName(child))
                    val childKotlinTypeName = getFullyQualifiedKotlinName(child)

                    // generate oneof parameters
                    val childMapper = kotlinToProtoMapperMap[Mapping(
                        childKotlinTypeName,
                        childProtoTypeName
                    )]

                    check(childProtoTypeName == childKotlinTypeName || childMapper != null) {
                        "Cannot map from oneof parameter $childKotlinTypeName to $childProtoTypeName, no mapper!"
                    }

                    val protoFieldName = getProtoFieldName(protoChild, pascalCase = true)

                    val kotlinChoiceName = KotlinGenerator.typeName(kotlinChild.kotlinType)
                    val kotlinCaseName = "$qualifiedKotlinTypeName.$CHOICE$kotlinChoiceName"

                    target.println(
                        protoFromKotlinOneOfCase(
                            indent = level + 2,
                            protoCase = oneOfQualifiedProtoTypeName,
                            protoField = protoFieldName,
                            kotlinChoice = kotlinCaseName,
                            kotlinField = kotlinChild.parameterName,
                            mappingFunction = childMapper
                        )
                    )

                }

                target.println(kotlinFromProtoEnumElse(indent = level + 2))

                target.println(kotlinFromProtoEnumEnd(level + 1))
                target.println(mapFunctionEnd(level))
            }
            else -> throw Exception("Cannot handle nodeType ${node.nodeType}")
        }
    }

    fun generate() {
        logger.info("Creating Kotlin <-> Proto Mapping")

//        // TODO LDe: Make output folder configurable
        val outputFolder = File("proto_kotlin_out")
        outputFolder.deleteRecursively()
        outputFolder.mkdirs()


        run {
            val protoToKotlinFileName = "ProtoToKotlin"
            val protoToKotlinFile = File(outputFolder, "$protoToKotlinFileName.kt")

            val os = ByteArrayOutputStream()
            PrintWriter(os).use { printWriter ->
                printWriter.println(KotlinGenerator.kotlinPackageDeclaration(mappingPackage))
                printWriter.println()

                printWriter.println(classStart(indent = 0, className = protoToKotlinFileName))
                printWriter.println(companionStart(indent = 1))
            }

            protoToKotlinFile.appendText(os.toString())

            tree.children.forEach { node ->
                os.reset()

                PrintWriter(os).use { printWriter ->
                    processNodeToKotlin(node, level = 2, target = printWriter)
                }

                logger.debug { os.toString() }

                protoToKotlinFile.appendText(os.toString())
            }

            os.reset()
            PrintWriter(os).use { printWriter ->
                printWriter.println(companionEnd(indent = 1))
                printWriter.println(classEnd(indent = 0))
            }
            protoToKotlinFile.appendText(os.toString())
        }


        run {

            val kotlinToProtoFileName = "KotlinToProto"
            val kotlinToProtoFile = File(outputFolder, "$kotlinToProtoFileName.kt")

            val os = ByteArrayOutputStream()
            PrintWriter(os).use { printWriter ->
                printWriter.println(KotlinGenerator.kotlinPackageDeclaration(mappingPackage))
                printWriter.println()

                printWriter.println(classStart(indent = 0, className = kotlinToProtoFileName))
                printWriter.println(companionStart(indent = 1))
            }

            kotlinToProtoFile.appendText(os.toString())

            tree.children.forEach { node ->
                os.reset()

                PrintWriter(os).use { printWriter ->
                    processNodeToProto(node, level = 2, target = printWriter)
                }

                logger.debug { os.toString() }

                kotlinToProtoFile.appendText(os.toString())
            }

            os.reset()
            PrintWriter(os).use { printWriter ->
                printWriter.println(companionEnd(indent = 1))
                printWriter.println(classEnd(indent = 0))
            }
            kotlinToProtoFile.appendText(os.toString())

        }
    }

}