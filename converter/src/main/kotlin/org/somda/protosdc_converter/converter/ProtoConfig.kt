package org.somda.protosdc_converter.converter

data class ProtoConfig(
    val protoPackage: String,
    val importPrefix: String,
    val messageNameSuffix: String,
    val optionOuterJavaClass: Boolean,
    val outerJavaClassNameSuffix: String,
    val optionJavaMultipleFiles: Boolean,
    val optionJavaPackage: String
)