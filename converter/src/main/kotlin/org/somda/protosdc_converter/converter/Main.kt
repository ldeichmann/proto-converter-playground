package org.somda.protosdc_converter.converter

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.arguments.validate
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.types.file
import com.github.ajalt.clikt.parameters.types.int
import org.apache.logging.log4j.Level
import org.apache.logging.log4j.core.config.Configurator
import org.apache.logging.log4j.kotlin.Logging
import org.somda.protosdc_converter.xmlprocessor.BaseTypeProcessor
import org.somda.protosdc_converter.xmlprocessor.SchemaPostProcessor


fun main(args: Array<String>) = Main().main(args)

class Main : CliktCommand(name = "protosdc-converter") {

    companion object : Logging

    private val schemaFile by argument(name = "FILE", help = "Schema input file")
        .file()
        .validate { require(it.exists()) { "'$it' does not exist." } }
    private val loopThreshold by option("--threshold", "-t", help = "Loop threshold").int().default(1000)
    private val protoPackage by option(
        "--proto-package",
        "-pp",
        help = "Custom proto package"
    ).default("sdc.biceps.proto.model")
    private val kotlinPackage by option(
        "--kotlin-package",
        "-kp",
        help = "Custom kotlin package"
    ).default("sdc.biceps.kotlin.model")
    private val rustMappingProtoModule by option(
        "--rust-mapping-proto-module",
        "-rmpm",
        help = "Custom rust module"
    ).default("proto::biceps")
    private val rustMappingBicepsModule by option(
        "--rust-mapping-biceps-module",
        "-rmbm"
    ).default("biceps_model::biceps")
    private val mappingPackage by option(
        "--kotlin-proto-mapping-package",
        "-mp",
        help = "Kotlin-Proto mapping package"
    ).default("sdc.biceps.proto.mapping")
    private val importPrefix by option("--import-prefix", "-ip", help = "Prefix for imports").default("")
    private val attributeSuffix by option(
        "--attribute-suffix",
        "-as",
        help = "Suffix for XML attributes in CamelCase"
    ).default("Attr")
    private val optionOuterJavaClass by option(
        "--java-outer-class",
        "-joc",
        help = "Enables Proto's Java outer class option"
    ).flag(default = false)
    private val outerJavaClassNameSuffix by option(
        "--java-outer-class-suffix",
        "-jcs",
        help = "A suffix that is appended to each outer class name, if -joc is enabled"
    ).default("Proto")
    private val optionJavaMultipleFiles by option(
        "--java-multi-files",
        "-jmf",
        help = "Proto's Java multiple files expansion flag"
    ).flag(default = false)
    private val messageNameSuffix by option(
        "--message-name-suffix",
        "-mns",
        help = "A suffix that is appended to each proto message name"
    ).default("")
    private val optionJavaPackage by option(
        "--java-package",
        "-jp",
        help = "Proto's Java package option value"
    ).default("")

    override fun run() {
        Configurator.setRootLevel(Level.INFO)
        val filePath = schemaFile.absolutePath
        SchemaPostProcessor(filePath, loopThreshold).resolve()

        val depTree = SchemaPostProcessor.dependencyTree

        var count = 0
        depTree.traverseInorder().forEach {
            logger.debug("in order ${it.nodeName}")
            count++
        }

        val baseTypeNodes = BaseTypeProcessor(SchemaPostProcessor.dependencyTree, attributeSuffix).generate()
        baseTypeNodes.validateTree()

        logger.info("Types to be generated: ${baseTypeNodes.children.size}")
        logger.debug { "Types to be generated (concrete): ${baseTypeNodes.children.joinToString { it.nodeName }}" }

        val protoConfig = ProtoConfig(
            protoPackage = protoPackage,
            importPrefix = importPrefix.let {
                when (it.endsWith('/')) {
                    true -> it
                    false -> when (it.isBlank()) {
                        true -> ""
                        false -> "$it/"
                    }
                }
            },
            messageNameSuffix = messageNameSuffix,
            optionJavaMultipleFiles = optionJavaMultipleFiles,
            optionJavaPackage = optionJavaPackage,
            optionOuterJavaClass = optionOuterJavaClass,
            outerJavaClassNameSuffix = outerJavaClassNameSuffix
        )

        val protoGen = Proto3Generator(
            tree = baseTypeNodes,
            protoConfig = protoConfig,
            attributeSuffix = attributeSuffix
        )
        protoGen.generate()

        KotlinGenerator(
            tree = baseTypeNodes,
            kotlinPackage = kotlinPackage,
            attributeSuffix = attributeSuffix
        ).generate()

        KotlinProtoMapping(
            tree = baseTypeNodes,
            protoCustomTypesTree = protoGen.optionalCompanionsRoot,
            mappingPackage = mappingPackage
        ).generate()

        RustGenerator(
            tree = baseTypeNodes,
            attributeSuffix = attributeSuffix
        ).generate()

        RustProtoMapping(
            tree = baseTypeNodes,
            protoCustomTypesTree = protoGen.optionalCompanionsRoot,
            rustModelModule = rustMappingBicepsModule,
            protoModelModule = rustMappingProtoModule
        ).generate()

    }
}