package org.somda.protosdc_converter.converter

import org.apache.logging.log4j.kotlin.Logging
import org.somda.protosdc_converter.xmlprocessor.*
import org.somda.protosdc_converter.xmlprocessor.Constants.EXT_NAMESPACE
import java.io.*
import java.nio.file.Paths
import javax.xml.namespace.QName

class Proto3Generator(
    private val tree: BaseNode,
    private val protoConfig: ProtoConfig,
    private val attributeSuffix: String
) {

    companion object : Logging {

        // file constants
        const val GOOGLE_IMPORT_PREFIX = "google/protobuf/"
        const val GOOGLE_PACKAGE_SUFFIX = "google.protobuf"
        const val PROTO3_SYNTAX_PREAMBLE = "syntax = \"proto3\";"
        const val PACKAGE_NAME_TEMPLATE = "package %s;"
        const val OPTION_JAVA_PACKAGE = "option java_package = \"%s\";"
        const val OPTION_JAVA_MULTIPLE_FILES = "option java_multiple_files = true;"
        const val OPTION_JAVA_OUTER_CLASSNAME = "option java_outer_classname = \"%s%s\";"
        const val IMPORT_TEMPLATE = "import \"%s%s\";"


        val BUILT_IN_TYPES = mapOf(
            BuiltinTypes.XSD_STRING.qname to ProtoEntry.ProtoType("string", null),
            BuiltinTypes.XSD_DECIMAL.qname to ProtoEntry.ProtoType("string", null),
            BuiltinTypes.XSD_INTEGER.qname to ProtoEntry.ProtoType("int64", null),
            BuiltinTypes.XSD_ULONG.qname to ProtoEntry.ProtoType("uint64", null),
            BuiltinTypes.XSD_BOOL.qname to ProtoEntry.ProtoType("bool", null),
            BuiltinTypes.XSD_DATE.qname to ProtoEntry.ProtoType("string", null),
            BuiltinTypes.XSD_DATE_TIME.qname to ProtoEntry.ProtoType("string", null),
            BuiltinTypes.XSD_G_YEAR_MONTH.qname to ProtoEntry.ProtoType("string", null),
            BuiltinTypes.XSD_G_YEAR.qname to ProtoEntry.ProtoType("string", null),
            BuiltinTypes.XSD_QNAME.qname to ProtoEntry.ProtoType("string", null),

            BuiltinTypes.XSD_LANGUAGE.qname to ProtoEntry.ProtoType("string", null),
            BuiltinTypes.XSD_LONG.qname to ProtoEntry.ProtoType("int64", null),
            BuiltinTypes.XSD_UINT.qname to ProtoEntry.ProtoType("uint32", null),
            BuiltinTypes.XSD_ANY_URI.qname to ProtoEntry.ProtoType("string", null),
            BuiltinTypes.XSD_INT.qname to ProtoEntry.ProtoType("int32", null)
        )

        val OPTIONAL_COMPANIONS = mapOf(
            BUILT_IN_TYPES[BuiltinTypes.XSD_STRING.qname] to ProtoEntry.ProtoType(
                "google.protobuf.StringValue",
                "google.protobuf",
                fileName = "wrappers.proto"
            ),
            BUILT_IN_TYPES[BuiltinTypes.XSD_DECIMAL.qname] to ProtoEntry.ProtoType(
                "google.protobuf.StringValue",
                "google.protobuf",
                fileName = "wrappers.proto"
            ),
            BUILT_IN_TYPES[BuiltinTypes.XSD_INTEGER.qname] to ProtoEntry.ProtoType(
                "google.protobuf.Int32Value",
                "google.protobuf",
                fileName = "wrappers.proto"
            ),
            BUILT_IN_TYPES[BuiltinTypes.XSD_ULONG.qname] to ProtoEntry.ProtoType(
                "google.protobuf.UInt64Value",
                "google.protobuf",
                fileName = "wrappers.proto"
            ),
            BUILT_IN_TYPES[BuiltinTypes.XSD_BOOL.qname] to ProtoEntry.ProtoType(
                "google.protobuf.BoolValue",
                "google.protobuf",
                fileName = "wrappers.proto"
            ),
            BUILT_IN_TYPES[BuiltinTypes.XSD_LONG.qname] to ProtoEntry.ProtoType(
                "google.protobuf.Int64Value",
                "google.protobuf",
                fileName = "wrappers.proto"
            ),
            BUILT_IN_TYPES[BuiltinTypes.XSD_UINT.qname] to ProtoEntry.ProtoType(
                "google.protobuf.UInt32Value",
                "google.protobuf",
                fileName = "wrappers.proto"
            ),
            BUILT_IN_TYPES[BuiltinTypes.XSD_INT.qname] to ProtoEntry.ProtoType(
                "google.protobuf.Int32Value",
                "google.protobuf",
                fileName = "wrappers.proto"
            )
        )

        fun optionalCompanionLookup(type: ProtoEntry.ProtoType): ProtoEntry.ProtoType {
            return OPTIONAL_COMPANIONS[type] ?: type
        }

        // map key = QName to replace
        // map value = the replacement type
        val CUSTOM_TYPES = mapOf(
            BuiltinTypes.XSD_ANY_SIMPLE_TYPE.qname to
                    ProtoEntry.ProtoType(
                        "google.protobuf.Any",
                        "google.protobuf",
                        fileName = "any.proto"
                    ),
            BuiltinTypes.XSD_DURATION.qname to
                    ProtoEntry.ProtoType(
                        "google.protobuf.Duration",
                        "google.protobuf",
                        fileName = "duration.proto"
                    ),
            QName(EXT_NAMESPACE, "ExtensionType") to
                    ProtoEntry.ProtoType(
                        "google.protobuf.Any",
                        "google.protobuf",
                        fileName = "any.proto"
                    )
        )

        // map of forbidden names as keys and the prefix to add the name to resolve the conflict as value
        private val FORBIDDEN_PARAMETER_NAMES = mapOf(
            // descriptor is used internally in at least the java protobuf implementation
            "descriptor" to "p_"
        )

        const val PROTO_INDENT = "  "

        const val PROTO_MESSAGE_START = "message %s {"
        const val PROTO_MESSAGE_END = "}"
        const val PROTO_ONEOF_START = "oneof %s {"
        const val PROTO_ONEOF_END = "}"

        private const val PROTO_TYPE_BASE = "%s %s = %s"
        const val PROTO_TYPE = "$PROTO_TYPE_BASE;"

        const val PROTO_ENUM_START = "enum %s {"
        const val PROTO_ENUM_ENTRY = "%s = %s;"
        const val PROTO_ENUM_END = "}"

        private const val PROTO_REPEATED_PREFIX = "repeated"

        const val PROTO_REPEATED = "$PROTO_REPEATED_PREFIX $PROTO_TYPE_BASE;"

        private fun String.camelToSnakeCase() = fold(StringBuilder(length)) { acc, c ->
            if (c in 'A'..'Z') (if (acc.isNotEmpty()) acc.append('_') else acc).append(c + ('a' - 'A'))
            else acc.append(c)
        }.toString()

        fun String.camelCaseToConstant(): String {
            if (this == this.uppercase()) {
                return this
            }
            val bld = StringBuilder(length)
            for ((index, char) in this.withIndex()) {
                if (char in 'A'..'Z') {
                    bld.append(
                        when {
                            index == 0 -> "" // do nothing as there should not be a leading underscore
                            index == length - 1 -> "_"
                            this[index + 1] !in 'A'..'Z' -> "_"
                            else -> "" // uppercase character should not cause underscore
                        }
                    )
                }
                bld.append(Character.toUpperCase(char))
            }
            return bld.toString()
        }

    }

    val optionalCompanionsRoot = BaseNode("optionalCompanionsRoot")

    private fun determineParameterName(node: BaseNode, parameter: NodeType.Parameter): String {
        val name = when {
            parameter.wasAttribute -> "${node.nodeName}$attributeSuffix"
            else -> node.nodeName
        }.camelToSnakeCase()

        return FORBIDDEN_PARAMETER_NAMES[name.toLowerCase()]?.let {
            "$it$name"
        } ?: name
    }

    private fun messageName(name: String) = when (name) {
        in CUSTOM_TYPES.values.map { it.typeName } -> name
        in BUILT_IN_TYPES.values.map { it.typeName } -> name
        in OPTIONAL_COMPANIONS.values.map { it.typeName } -> name
        else -> "$name${protoConfig.messageNameSuffix}"
    }

    /**
     * Data class for modelling proto data and converting it into actual proto schema data.
     */
    sealed class ProtoEntry : BaseLanguageType() {

        abstract fun toProtoEnter(level: Int = 0): String

        abstract fun toProtoExit(level: Int = 0, fieldNumber: Int = 0): Pair<String, Int>

        abstract fun toProtoMain(level: Int = 0, fieldNumber: Int = 0): Pair<String, Int>

        /**
         * A type, as in a Message, also wrapped around enums.
         */
        data class ProtoType(
            val typeName: String,
            var packagePath: String? = null,
            val wasAttribute: Boolean = false,
            var fileName: String? = null,
            private val _typeNameRaw: String? = null
        ) :
            ProtoEntry() {

            val typeNameRaw: String = _typeNameRaw ?: typeName

            override fun toProtoEnter(level: Int): String {
                return PROTO_INDENT.repeat(level) + PROTO_MESSAGE_START.format(typeName)
            }

            override fun toProtoExit(level: Int, fieldNumber: Int): Pair<String, Int> {
                val endBlock = PROTO_INDENT.repeat(level) + PROTO_MESSAGE_END
                return Pair(endBlock, 0)
            }

            override fun toProtoMain(level: Int, fieldNumber: Int): Pair<String, Int> {
                // no "main" here, just the body
                return Pair("", 0)
            }

            fun importPath(): String = packagePath?.let {"$it.${actualFileName()}"} ?: actualFileName()

            fun fullyQualifiedName(): String {
                return packagePath?.let {
                    // if the packagepath is the same prefix as the typename, we're in fucky territory
                    // this is almost exlusively the case with google.protobuf
                    when (typeName.startsWith(it)) {
                        true -> typeName
                        false -> "$it.${typeName}"
                    }
                } ?: typeName
            }

            /**
             * Determines the full file name for a type, including .proto suffix.
             *
             * If a type is supposed to be clustered, the filename for the cluster is returned.
             *
             * @return full file name
             */
            fun actualFileName(): String {

                // If filename is set, this type should be clustered with another type
                return fileName ?: run {
                    val refName = typeNameRaw
                    val fullName = "${refName.toLowerCase()}.proto"
                    fullName
                }
            }

        }

        /**
         * Parameter within a [ProtoType] or [ProtoOneOf].
         */
        data class ProtoParameter(
            val protoType: ProtoEntry,
            val parameterName: String,
            val repeated: Boolean = false
        ) : ProtoEntry() {

            private fun typeName(): String {
                return when (val type = protoType) {
                    is ProtoType -> type.typeName
                    is ProtoEnum -> type.enumName
                    else -> throw Exception("Cannot handle $type in typeName")
                }
            }

            override fun toProtoEnter(level: Int): String {
                return ""
            }

            override fun toProtoExit(level: Int, fieldNumber: Int): Pair<String, Int> {
                return Pair("", 0)
            }

            override fun toProtoMain(level: Int, fieldNumber: Int): Pair<String, Int> {
                val data = PROTO_INDENT.repeat(level) + when (repeated) {
                    true -> {
                        PROTO_REPEATED.format(typeName(), parameterName, fieldNumber + 1)
                    }
                    false -> {
                        PROTO_TYPE.format(typeName(), parameterName, fieldNumber + 1)
                    }
                }
                return Pair(data, 1)
            }
        }

        /**
         * An enumeration of strings.
         */
        data class ProtoEnum(
            val enumName: String,
            val enumValues: List<String>
        ) : ProtoEntry() {
            override fun toProtoEnter(level: Int): String {
                return PROTO_INDENT.repeat(level) + PROTO_ENUM_START.format(enumName)
            }

            override fun toProtoExit(level: Int, fieldNumber: Int): Pair<String, Int> {
                return Pair(PROTO_INDENT.repeat(level) + PROTO_ENUM_END, 0)
            }

            override fun toProtoMain(level: Int, fieldNumber: Int): Pair<String, Int> {
                val enumBody = enumValues.withIndex().joinToString(separator = "\n", postfix = "\n") { (index, value) ->
                    PROTO_INDENT.repeat(level) + PROTO_ENUM_ENTRY.format(value.camelCaseToConstant(), index)
                }

                return Pair(enumBody, 0)
            }
        }

        /**
         * A oneOf element.
         */
        data class ProtoOneOf(
            val oneOfName: String,
        ) : ProtoEntry() {
            override fun toProtoEnter(level: Int): String {
                return PROTO_INDENT.repeat(level) + PROTO_ONEOF_START.format(oneOfName)
            }

            override fun toProtoExit(level: Int, fieldNumber: Int): Pair<String, Int> {
                return Pair(PROTO_INDENT.repeat(level) + PROTO_ONEOF_END, 0)
            }

            override fun toProtoMain(level: Int, fieldNumber: Int): Pair<String, Int> {
                // no "main" here, just the body
                return Pair("", 0)
            }
        }
    }

    init {
        // attach builtin types to their nodes
        BUILT_IN_TYPES.forEach { (typeQName, protoType) ->
            val node = tree.findQName(typeQName)!!
            node.languageType[OutputLanguage.Proto] = protoType
            logger.info("Attaching builtin type $typeQName to protobuf type ${protoType.typeName}")
        }
        CUSTOM_TYPES.forEach { (typeQName, protoType) ->
            val node = tree.findQName(typeQName)!!
            node.languageType[OutputLanguage.Proto] = protoType
            logger.info("Attaching custom type $typeQName to protobuf type ${protoType.typeName}")
        }
        // create nodes for optional companion nodes
        optionalCompanionsRoot.nodeType = NodeType.Root
        OPTIONAL_COMPANIONS.forEach { (type, optionalType) ->
            logger.info { "Adding optional companion type ${optionalType.typeName} for type ${type!!.typeName} to tree" }
            val node = BaseNode(optionalType.typeName)

            val qname = BUILT_IN_TYPES.filterValues { it == type }.keys.iterator().next()
            val nodeType = NodeType.BuiltinType(parentNode = node, origin = qname)
            node.nodeType = nodeType
            node.languageType[OutputLanguage.Proto] = optionalType

            optionalCompanionsRoot.children.add(node)
        }
    }

    /**
     * Processes a [BaseNode], i.e. attaches a [ProtoEntry] to the node and every node below it.
     * @param node to attach proto data to
     */
    private fun processNode(node: BaseNode) {
        // handle clustered types, they need at least their message types generated all at once to allow the usual
        // method of traversing depth first in order to continue working
        node.clusteredTypes?.let { cluster ->
            logger.info("Received clustered type ${node.nodeName}, bulk handling ${cluster.joinToString { it.nodeName }} as well")
            logger.info("Cluster node types are ${cluster.joinToString { it.nodeType!!::class.simpleName!! }}")
            if (node.clusterHandled[OutputLanguage.Proto] == true) {
                logger.info("Cluster with ${node.nodeName} is already handled, skipping")
                return@let
            }

            // generate all message language types so the references to not die. also enforce clustering in one file
            // order matters, this node is now enforcing naming rules, which is why it must be handled first
            val totalCluster = listOf(node) + cluster
            totalCluster.forEach { clusterNode ->
                when (val nodeType = clusterNode.nodeType) {
                    is NodeType.Message -> {
                        logger.info("Processing $clusterNode")

                        // set filename for all of these to the same one, based on Node
                        // attach type node
                        val languageType = ProtoEntry.ProtoType(
                            messageName(clusterNode.nodeName),
                            packagePath = protoConfig.protoPackage,
                            _typeNameRaw = clusterNode.nodeName
                        )
                        clusterNode.languageType[OutputLanguage.Proto] = languageType

                        val fileName = (node.languageType[OutputLanguage.Proto] as ProtoEntry.ProtoType).actualFileName()
                        languageType.fileName = fileName
                        logger.info("Fixing filename for clusterType $clusterNode to $fileName")

                    }
                    else -> throw Exception(
                        "Can only cluster nodes which are messages, not $nodeType." +
                                " How would that even work for anything else?"
                    )
                }
            }

            // mark cluster as resolved
            totalCluster.forEach { node -> node.clusterHandled[OutputLanguage.Proto] = true }

            // no return or modified branching, we've handled the cluster and normal processing can continue
        }
        when (val nodeType = node.nodeType) {
            is NodeType.Message -> {
                logger.info("Processing $node")
                // attach type node
                if (node.languageType[OutputLanguage.Proto] != null) {
                    check(
                        node.clusterHandled[OutputLanguage.Proto] == true
                                || CUSTOM_TYPES.containsValue(node.languageType[OutputLanguage.Proto])
                    ) {
                        "Message $node already has proto type attached, but isn't cluster. What?"
                    }
                } else {
                    node.languageType[OutputLanguage.Proto] =
                        ProtoEntry.ProtoType(
                            messageName(node.nodeName),
                            packagePath = protoConfig.protoPackage,
                            _typeNameRaw = node.nodeName
                        )
                }
            }
            is NodeType.Parameter -> {
                logger.debug("Processing parameter ${nodeType.parentNode?.nodeName}")

                val parameter: NodeType.Parameter = nodeType
                val parameterName = determineParameterName(node, parameter)
                val optionalAndNotList = parameter.optional && !parameter.list

                when (val parameterType = parameter.parameterType.parent!!.languageType[OutputLanguage.Proto]) {
                    is ProtoEntry.ProtoEnum -> {
                        node.languageType[OutputLanguage.Proto] = ProtoEntry.ProtoParameter(
                            parameterType,
                            parameterName,
                            repeated = parameter.list
                        )
                    }
                    is ProtoEntry.ProtoType -> {
                        if (optionalAndNotList) {
                            node.languageType[OutputLanguage.Proto] = ProtoEntry.ProtoParameter(
                                optionalCompanionLookup(parameterType),
                                parameterName,
                                repeated = parameter.list
                            )
                        } else {
                            node.languageType[OutputLanguage.Proto] = ProtoEntry.ProtoParameter(
                                parameterType,
                                parameterName,
                                repeated = parameter.list
                            )
                        }
                    }
                    else -> {
                        throw Exception("Unsupported language type ${parameter.parameterType.parent!!.languageType[OutputLanguage.Proto]}")
                    }
                }
            }
            is NodeType.BuiltinType -> {
                // ignored intentionally
            }
            is NodeType.StringEnumeration -> {
                val enum: NodeType.StringEnumeration = nodeType
                // TODO LDe: Proper name
                // pass values as list copy
                node.languageType[OutputLanguage.Proto] = ProtoEntry.ProtoEnum("EnumType", enum.values.toList())
            }
            is NodeType.OneOf -> {
                node.languageType[OutputLanguage.Proto] = ProtoEntry.ProtoOneOf(nodeType.parent!!.nodeName)
            }
            else -> {
                throw Exception("Handle unknown node type ${node.nodeType}")
            }
        }

        node.children.forEach {
            processNode(it)
        }
    }

    /**
     * Generates the actual .proto file content.
     *
     * @param node to generate proto schema for
     * @param level the level of the current node, used for indentation
     * @param target the target in which to write the proto content
     * @param fieldNumber counter for fieldNumbers, which must be unique within a message
     */
    private fun generateProto(node: BaseNode, level: Int = 0, target: PrintWriter, fieldNumber: Int = 0): Int {
        val protoEntry = node.languageType[OutputLanguage.Proto] as ProtoEntry
        var currentFieldNumber = fieldNumber

        val header = protoEntry.toProtoEnter(level = level)
        if (header.isNotBlank()) {
            // only write non empty lines, duh
            target.println(header)
        }

        // only apply a + 1 offset when we didn't generate a header, i.e. this is just a parameter or something
        val mainOffset = when (header.isBlank()) {
            true -> level
            false -> level + 1
        }

        node.children.forEach {
            currentFieldNumber += generateProto(it, level = mainOffset, target, fieldNumber = currentFieldNumber)
        }

        val (main, fields) = protoEntry.toProtoMain(level = mainOffset, fieldNumber = currentFieldNumber)
        currentFieldNumber += fields
        if (fields > 0) {
            target.println(main)
        } else {
            target.print(main)
        }

        val (footer, footerFields) = protoEntry.toProtoExit(level = level, fieldNumber = currentFieldNumber)
        currentFieldNumber += footerFields
        when {
            footer.isBlank() -> {
            }
            else -> {
                target.println(footer)
            }
        }

        return currentFieldNumber - fieldNumber // return difference only
    }

    /**
     * Determines the imports needed for a file.
     *
     * @param node the node for which to determine the required imports
     * @param fileName the fileName these imports will be written into, to avoid self referential imports
     * @return List of Pairs in which the key is the package for the import, and the value is the filename to import
     */
    private fun determineImports(node: BaseNode, fileName: String): List<Pair<String, String>> {
        val dependencies = mutableListOf<Pair<String, String>>()
        node.children.forEach { child ->
            when (val languageType = child.languageType[OutputLanguage.Proto]) {
                is ProtoEntry.ProtoParameter -> {
                    // determine if this elsewhere or a sibling
                    val parameterType = languageType.protoType
                    val siblings = node.children - child
                    val isSiblingType = siblings.any { it.languageType[OutputLanguage.Proto] == parameterType }
                    val (importPath, importFileName) = when (parameterType) {
                        is ProtoEntry.ProtoType -> {
                            when (parameterType.packagePath.isNullOrBlank()) {
                                false -> Pair(parameterType.packagePath, parameterType.actualFileName())
                                true -> Pair(null, null)
                            }
                        }
                        else -> Pair(null, null)
                    }
                    if (!isSiblingType && !importPath.isNullOrBlank() && importFileName != fileName) {
                        dependencies.add(Pair(importPath.replace(".", "/") + "/", importFileName!!))
                    }

                }
            }
        }

        node.children.forEach {
            dependencies.addAll(determineImports(it, fileName))
        }

        return dependencies.distinct()
    }

    fun generate() {

        // TODO LDe: Make output folder configurable
        val outputFolder = File("proto_out")
        outputFolder.deleteRecursively()
        outputFolder.mkdirs()

        // generate types for every "root" type, i.e. the one on depth 1
        // every other type is nested within those and will be covered by the generators
        tree.children.filter { it.nodeType !is NodeType.BuiltinType }.forEach { node ->
            processNode(node)
        }

        // track the imports used in every file
        // this is needed to not duplicate imports when multiple nodes are added into a single file
        // i.e. when a cluster happened and needs to be in a single file to avoid circular imports
        val fileImports = mutableMapOf<String, MutableSet<String>>()

        // write all the files!
        tree.children
            // dont write builtins though
            .filter { it.nodeType !is NodeType.BuiltinType }
            .filter {
                when (val lang = it.languageType[OutputLanguage.Proto]) {
                    is ProtoEntry.ProtoType -> !lang.typeName.startsWith(GOOGLE_PACKAGE_SUFFIX)
                    else -> true
                }
            }.forEach { node ->
                // determine file for this message
                check(node.languageType[OutputLanguage.Proto] is ProtoEntry.ProtoType) { "Expected ProtoType, was ${node.languageType[OutputLanguage.Proto]}" }
                val languageType = node.languageType[OutputLanguage.Proto] as ProtoEntry.ProtoType


                val fileName = languageType.actualFileName()
                // determine directory structure required based on package
                var directory = outputFolder
                if (protoConfig.protoPackage.isNotEmpty()) {
                    val segments = protoConfig.protoPackage.split(".").toTypedArray()
                    directory = File(Paths.get(outputFolder.absolutePath, *segments).toUri())
                    if (!directory.exists()) {
                        directory.mkdirs()
                    }
                }
                val file = File(directory, fileName)

                // determine the imports used by the node
                val imports = determineImports(node, fileName)

                val os = ByteArrayOutputStream()
                PrintWriter(os).use { printWriter ->

                    // only add this whole preamble stuff if the file is new, i.e. not an already existing clustered type
                    if (!file.exists()) {
                        printWriter.println(PROTO3_SYNTAX_PREAMBLE)
                        printWriter.println()
                        if (protoConfig.protoPackage.isNotEmpty()) {
                            printWriter.println(PACKAGE_NAME_TEMPLATE.format(protoConfig.protoPackage))
                            printWriter.println()
                        }
                        if (protoConfig.optionJavaMultipleFiles) {
                            printWriter.println(OPTION_JAVA_MULTIPLE_FILES)
                        }
                        if (protoConfig.optionJavaPackage.isNotEmpty()) {
                            printWriter.println(OPTION_JAVA_PACKAGE.format(protoConfig.optionJavaPackage))
                        }

                        printWriter.println(
                            OPTION_JAVA_OUTER_CLASSNAME.format(
                                languageType.typeNameRaw,
                                protoConfig.outerJavaClassNameSuffix
                            )
                        )

                        // spacing
                        printWriter.println()
                    }
                    val existingImports: MutableSet<String> = fileImports.getOrPut(fileName) { mutableSetOf() }
                    imports.forEach { (packagePath, fileName) ->
                        val import = when (packagePath.startsWith(GOOGLE_IMPORT_PREFIX)) {
                            true -> {
                                IMPORT_TEMPLATE.format(
                                    packagePath,
                                    fileName.split(".").takeLast(2).joinToString(separator = ".")
                                )
                            }
                            false -> {
                                IMPORT_TEMPLATE.format(packagePath, fileName)
                            }
                        }
                        if (!existingImports.contains(import)) {
                            printWriter.println(import)
                        }
                        existingImports.add(import)
                    }

                    // spacing
                    printWriter.println()

                    // actually generate the message into the stream
                    generateProto(node, target = printWriter)

                    // spacing
                    printWriter.println()
                }

                val result = os.toString()
                logger.debug { "--\n$fileName\n--\n$result" }

                file.appendText(os.toString())
            }
    }

}