package org.somda.protosdc_converter.converter

import org.apache.logging.log4j.kotlin.Logging
import org.somda.protosdc_converter.xmlprocessor.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.PrintWriter
import java.nio.file.Paths
import javax.xml.namespace.QName

class KotlinGenerator(
    private val tree: BaseNode,
    private val kotlinPackage: String,
    private val attributeSuffix: String) {

    companion object : Logging {

        fun kotlinPackageDeclaration(data: String) = "package $data"
        fun kotlinImportDeclaration(data: String) = "import $data"

        const val INDENT = "    "
        fun kotlinDataClassPrimaryConstructorStart(level: Int, data: String) = "${INDENT.repeat(level)}data class $data ("
        fun kotlinDataClassPrimaryConstructorEnd(level: Int) = "${INDENT.repeat(level)})"
        // this is on the same line as the constructor end, so no fun
        const val KOTLIN_DATA_CLASS_BODY_START = "{"
        fun kotlinDataClassBodyEnd(level: Int) = "${INDENT.repeat(level)}}"

        fun kotlinListParameter(data: String) = "List<$data> = listOf()"
        fun kotlinDataClassParameter(indent: Int, parameterName: String, parameterType: String, nullable: Boolean): String {
            val third = when (nullable) {
                true -> "$KOTLIN_NULLABLE = null"
                false -> ""
            }
            return "${INDENT.repeat(indent)}val $parameterName: $parameterType$third,"
        }
        private const val KOTLIN_NULLABLE = "?"

        fun kotlinEnumStart(indent: Int, data: String) = "${INDENT.repeat(indent)}enum class $data {"
        fun kotlinEnumEnd(indent: Int) = "${INDENT.repeat(indent)}}"
        const val KOTLIN_ENUM_SEPARATOR = ", "

        fun kotlinOneOfStart(indent: Int, data: String) = "${INDENT.repeat(indent)}sealed class $data {"
        fun kotlinOneOfEnd(indent: Int) = "${INDENT.repeat(indent)}}"
        fun kotlinOneOfEntry(indent: Int, typeName: String, parameterName: String, parameterType: String, parentType: String): String {
            return "${INDENT.repeat(indent)}data class $typeName(val $parameterName: $parameterType) : $parentType()"
        }

        const val CHOICE = "Choice"


        val BUILT_IN_TYPES = mapOf(
            BuiltinTypes.XSD_STRING.qname to KotlinEntry.DataClass("String"),
            BuiltinTypes.XSD_DECIMAL.qname to KotlinEntry.DataClass("BigDecimal", "java.math"),
            BuiltinTypes.XSD_INTEGER.qname to KotlinEntry.DataClass("Long"),
            BuiltinTypes.XSD_ULONG.qname to KotlinEntry.DataClass("Long"),
            BuiltinTypes.XSD_BOOL.qname to KotlinEntry.DataClass("Boolean"),
            BuiltinTypes.XSD_DATE.qname to KotlinEntry.DataClass("LocalDate", "java.time"),
            BuiltinTypes.XSD_DATE_TIME.qname to KotlinEntry.DataClass("LocalDateTime", "java.time"),
            BuiltinTypes.XSD_G_YEAR_MONTH.qname to KotlinEntry.DataClass("YearMonth", "java.time"),
            BuiltinTypes.XSD_G_YEAR.qname to KotlinEntry.DataClass("Year", "java.time"),
            BuiltinTypes.XSD_QNAME.qname to KotlinEntry.DataClass("QName", "javax.xml.namespace"),
            BuiltinTypes.XSD_LANGUAGE.qname to KotlinEntry.DataClass("String"),
            BuiltinTypes.XSD_LONG.qname to KotlinEntry.DataClass("Long"),
            BuiltinTypes.XSD_UINT.qname to KotlinEntry.DataClass("Int"),
            // java.net.URI type causes trouble, hence go for simple string
            BuiltinTypes.XSD_ANY_URI.qname to KotlinEntry.DataClass("String"),
            BuiltinTypes.XSD_INT.qname to KotlinEntry.DataClass("Int"),
            BuiltinTypes.XSD_ANY_SIMPLE_TYPE.qname to KotlinEntry.DataClass("Any"),
            BuiltinTypes.XSD_DURATION.qname to KotlinEntry.DataClass("Duration", "java.time"),
        )

        val CUSTOM_TYPES = mapOf(
            QName(Constants.EXT_NAMESPACE, "ExtensionType") to KotlinEntry.DataClass("Any")
        )

        fun toParameterName(string: String) = string.decapitalize()

        /**
         * Generates the actual .kt file content.
         *
         * @param node to generate proto schema for
         * @param level the level of the current node, used for indentation
         * @param target the target in which to write the proto content
         * @param isNestedCall when the call is for a nested body, i.e. the body of a data class
         */
        fun generateKotlin(node: BaseNode, level: Int = 0, target: PrintWriter, isNestedCall: Boolean = false) {
            val kotlinEntry = node.languageType[OutputLanguage.Kotlin] as KotlinEntry

            val skipNested = when (kotlinEntry) {
                is KotlinEntry.DataClass -> kotlinEntry.onlyGenerateNested
                is KotlinEntry.KotlinOneOf -> kotlinEntry.onlyGenerateNested
                is KotlinEntry.KotlinStringEnumeration -> kotlinEntry.onlyGenerateNested
                else -> false
            } && !isNestedCall

            if (skipNested) {
                return
            }

            val skipType = when (kotlinEntry) {
                is KotlinEntry.DataClass -> kotlinEntry.skipType
                else -> false
            }

            if (skipType) {
                node.children.forEach {
                    generateKotlin(it, level = level, target)
                }
                return
            }

            val header: String = when (skipNested) {
                false -> {
                    val hdr = kotlinEntry.toKotlinEnter(level = level)
                    if (hdr.isNotBlank()) {
                        // only write non empty lines, duh
                        target.println(hdr)
                    }
                    hdr
                }
                true -> ""
            }

            // only apply a + 1 offset when we didn't generate a header, i.e. this is just a parameter or something
            val mainOffset = when (header.isBlank()) {
                true -> level
                false -> level + 1
            }


            node.children.forEach {
                generateKotlin(it, level = mainOffset, target)
            }

            when (skipNested) {
                false -> {
                    val main = kotlinEntry.toKotlinMain(level = mainOffset)
                    if (main.isNotBlank()) {
                        target.println(main)
                    }

                    val footer = kotlinEntry.toKotlinExit(level = level)
                    if (footer.isNotEmpty()) {
                        target.println(footer)
                    }

                }
                true -> {
                }
            }

        }

        /**
         * Determine the name of a kotlin type, i.e. data class or enumeration.
         */
        fun typeName(kotlinType: KotlinEntry): String {
            return when (kotlinType) {
                is KotlinEntry.DataClass -> kotlinType.typeName
                is KotlinEntry.KotlinStringEnumeration -> kotlinType.enumName
                else -> throw Exception("Cannot handle $kotlinType in typeName")
            }
        }

        /**
         * Determines the full file name for a type, including .kt suffix.
         *
         * @return full file name
         */
        private fun fileNameFromType(kotlinType: KotlinEntry.DataClass): String {
            var name = kotlinType.fileName ?: kotlinType.typeNameRaw
            if (!name.endsWith(".kt")) {
                name += ".kt"
            }
            return name
        }
    }

    private fun determineParameterName(node: BaseNode, parameter: NodeType.Parameter): String {
        // first letter must be lower case
        return toParameterName(when {
            parameter.wasAttribute -> "${node.nodeName}$attributeSuffix"
            else -> node.nodeName
        })
    }

    /**
     * Class for modelling kotlin data classes and converting them into actual code.
     */
    sealed class KotlinEntry : BaseLanguageType() {

        abstract fun toKotlinEnter(level: Int = 0, nestedCall: Boolean = false): String

        abstract fun toKotlinExit(level: Int = 0, nestedCall: Boolean = false): String

        abstract fun toKotlinMain(level: Int = 0, nestedCall: Boolean = false): String

        /**
         * A type as in a data class.
         * @param nestedTypes list of types which are nested below this node and will be generated into the
         * body of the data class.
         * @param onlyGenerateNested marks a an class to be only generated when generating nested content, i.e. when
         * generating the body of a data class, in which nested classes are placed.
         * @param skipType marks a type to be skipped, only generating its children. Used to unnest wrapper types from
         * the abstract model.
         */
        data class DataClass(
            val typeName: String,
            var packagePath: String? = null,
            val wasAttribute: Boolean = false,
            var fileName: String? = null,
            private val _typeNameRaw: String? = null,
            val nestedTypes: MutableList<BaseNode> = mutableListOf(),
            var onlyGenerateNested: Boolean = false,
            var skipType: Boolean = false
        ) : KotlinEntry() {

            val typeNameRaw: String = _typeNameRaw ?: typeName

            override fun toKotlinEnter(level: Int, nestedCall: Boolean): String {
                return kotlinDataClassPrimaryConstructorStart(level = level, data = typeName)
            }

            override fun toKotlinExit(level: Int, nestedCall: Boolean): String {
                var returnValue = kotlinDataClassPrimaryConstructorEnd(level)
                if (nestedTypes.isNotEmpty()) {
                    returnValue += " $KOTLIN_DATA_CLASS_BODY_START"
                    val os = ByteArrayOutputStream()
                    PrintWriter(os).use { printWriter ->
                        nestedTypes.forEach { nestedType ->
                            generateKotlin(nestedType, target = printWriter, level = level + 1, isNestedCall = true)
                        }
                    }
                    returnValue += "\n"
                    returnValue += os.toString()
                    returnValue += kotlinDataClassBodyEnd(level)
                }

                return returnValue
            }

            override fun toKotlinMain(level: Int, nestedCall: Boolean): String {
                // no "main" here, just the body
                return ""
            }

            fun importPath(): String = packagePath?.let { "$it.$typeName" } ?: typeName

        }

        /**
         * A parameter within a OneOf, has different generating rules from [KotlinParameter].
         */
        data class KotlinOneOfParameter(
            val kotlinType: KotlinEntry,
            val parameterName: String,
            val oneOf: KotlinOneOf
        ) : KotlinEntry() {

            override fun toKotlinEnter(level: Int, nestedCall: Boolean) = ""
            override fun toKotlinExit(level: Int, nestedCall: Boolean) = ""

            override fun toKotlinMain(level: Int, nestedCall: Boolean): String {
                return kotlinOneOfEntry(
                    level,
                    // data class cannot have the same name as the type references,
                    // TODO LDe: Configurable prefix or something
                    CHOICE + typeName(kotlinType),
                    parameterName,
                    typeName(kotlinType),
                    oneOf.oneOfName
                )
            }

        }

        /**
         * A parameter in a data class.
         */
        data class KotlinParameter(
            val kotlinType: KotlinEntry,
            val parameterName: String,
            val list: Boolean = false,
            val nullable: Boolean = false,
        ) : KotlinEntry() {

            override fun toKotlinEnter(level: Int, nestedCall: Boolean) = ""
            override fun toKotlinExit(level: Int, nestedCall: Boolean): String = ""

            override fun toKotlinMain(level: Int, nestedCall: Boolean): String {
                return when {
                    list -> {
                        // there is no such thing as an optional (nullable) list, so these can be merged
                        val parameterType = kotlinListParameter(typeName(kotlinType))
                        kotlinDataClassParameter(level, parameterName, parameterType, false)
                    }
                    else -> kotlinDataClassParameter(level, parameterName, typeName(kotlinType), nullable)
                }
            }
        }

        /**
         * A string enumeration.
         *
         * @param onlyGenerateNested marks a an class to be only generated when generating nested content, i.e. when
         * generating the body of a data class, in which nested classes are placed.
         */
        data class KotlinStringEnumeration(
            val enumName: String,
            val enumValues: List<String>,
            var onlyGenerateNested: Boolean = false
        ) : KotlinEntry() {
            override fun toKotlinEnter(level: Int, nestedCall: Boolean): String {
                return kotlinEnumStart(level, enumName)
            }

            override fun toKotlinExit(level: Int, nestedCall: Boolean): String {
                return kotlinEnumEnd(level)
            }

            override fun toKotlinMain(level: Int, nestedCall: Boolean): String {
                return INDENT.repeat(level) + enumValues.joinToString(separator = KOTLIN_ENUM_SEPARATOR)
            }

        }

        /**
         * A OneOf, represented as a sealed class with nested data classes, see [KotlinOneOfParameter].
         *
         * @param onlyGenerateNested marks a an class to be only generated when generating nested content, i.e. when
         * generating the body of a data class, in which nested classes are placed.
         */
        data class KotlinOneOf(
            val oneOfName: String,
            var onlyGenerateNested: Boolean = false
        ) : KotlinEntry() {
            override fun toKotlinEnter(level: Int, nestedCall: Boolean): String {
                return kotlinOneOfStart(level, oneOfName)
            }

            override fun toKotlinExit(level: Int, nestedCall: Boolean): String {
                return kotlinOneOfEnd(level)
            }

            override fun toKotlinMain(level: Int, nestedCall: Boolean): String {
                // no "main" here, just the body
                return ""
            }

        }
    }

    init {
        // attach builtin types to their nodes
        BUILT_IN_TYPES.forEach { (typeQName, kotlinDataClass) ->
            val node = tree.findQName(typeQName)!!
            node.languageType[OutputLanguage.Kotlin] = kotlinDataClass
            logger.info("Attaching builtin type $typeQName to kotlin type ${kotlinDataClass.typeName}")
        }
        // attach builtin types to their nodes
        CUSTOM_TYPES.forEach { (typeQName, kotlinDataClass) ->
            val node = tree.findQName(typeQName)!!
            node.languageType[OutputLanguage.Kotlin] = kotlinDataClass
            logger.info("Attaching custom type $typeQName to kotlin type ${kotlinDataClass.typeName}")
        }
    }


    /**
     * Processes a [BaseNode], i.e. attaches a [KotlinEntry] to the node and every node below it.
     * @param node to attach proto data to
     */
    private fun processNode(node: BaseNode, parentNode: BaseNode?) {
        logger.info("Processing $node")
        node.clusteredTypes?.let { cluster ->
            logger.info("Received clustered type ${node.nodeName}, bulk handling ${cluster.joinToString { it.nodeName }} as well")
            logger.info("Cluster node types are ${cluster.joinToString { it.nodeType!!::class.simpleName!! }}")
            if (node.clusterHandled[OutputLanguage.Kotlin] == true) {
                logger.info("Cluster with ${node.nodeName} is already handled, skipping")
                return@let
            }

            // generate all message language types so the references to not die. also enforce clustering in one file
            // order matters, this node is now enforcing naming rules, which is why it must be handled first
            val totalCluster = listOf(node) + cluster
            totalCluster.forEach { clusterNode ->
                when (val nodeType = clusterNode.nodeType) {
                    is NodeType.Message -> {
                        logger.info("Processing cluster node $clusterNode")

                        // set filename for all of these to the same one, based on Node
                        // attach type node
                        val languageType = KotlinEntry.DataClass(
                            clusterNode.nodeName,
                            _typeNameRaw = clusterNode.nodeName,
                            packagePath = kotlinPackage
                        )
                        clusterNode.languageType[OutputLanguage.Kotlin] = languageType

                        // TODO LDe: Let's see if this is needed, probably isn't
//                        val fileName = fileNameFromType(node.languageType as Proto3Generator.ProtoEntry.ProtoType)
//                        languageType.fileName = fileName
//                        logger.info("Fixing filename for clusterType $clusterNode to $fileName")

                    }
                    else -> throw Exception(
                        "Can only cluster nodes which are messages, not $nodeType." +
                                " How would that even work for anything else?"
                    )
                }
            }

            // mark cluster as resolved
            totalCluster.forEach { node -> node.clusterHandled[OutputLanguage.Kotlin] = true }

            // no return or modified branching, we've handled the cluster and normal processing can continue
        }


        when (val nodeType = node.nodeType) {
            is NodeType.Message -> {
                logger.debug("Processing message $node")
                if (node.languageType[OutputLanguage.Kotlin] != null) {
                    logger.info("Node ${node.nodeName} already has language typed attached, skipping")
                } else {
                    node.languageType[OutputLanguage.Kotlin] = KotlinEntry.DataClass(
                        node.nodeName,
                        _typeNameRaw = node.nodeName,
                        packagePath = kotlinPackage
                    )
                }
            }
            is NodeType.Parameter -> {
                logger.debug("Processing parameter ${nodeType.parentNode?.nodeName}")
                val parameterName = determineParameterName(node, nodeType)

                when (val parentLanguageType = parentNode?.let { it.languageType[OutputLanguage.Kotlin] }) {
                    is KotlinEntry.KotlinOneOf -> {
                        val parameterType =
                            nodeType.parameterType.parent!!.languageType[OutputLanguage.Kotlin] as KotlinEntry.DataClass
                        node.languageType[OutputLanguage.Kotlin] = KotlinEntry.KotlinOneOfParameter(
                            parameterType, parameterName, parentLanguageType
                        )
                    }
                    else -> {
                        when (val parameterType = nodeType.parameterType.parent!!.languageType[OutputLanguage.Kotlin]) {
                            is KotlinEntry.KotlinStringEnumeration -> {
                                node.languageType[OutputLanguage.Kotlin] = KotlinEntry.KotlinParameter(
                                    parameterType, parameterName, list = nodeType.list, nullable = nodeType.optional
                                )
                            }
                            is KotlinEntry.DataClass -> {
                                node.languageType[OutputLanguage.Kotlin] = KotlinEntry.KotlinParameter(
                                    parameterType, parameterName, list = nodeType.list, nullable = nodeType.optional
                                )
                            }
//                            is KotlinEntry.KotlinOneOf -> {
//
//                            }
                            else -> {
                                throw Exception("Unsupported language type $parameterType")
                            }
                        }
                    }
                }
            }
            is NodeType.StringEnumeration -> {
                node.languageType[OutputLanguage.Kotlin] =
                    KotlinEntry.KotlinStringEnumeration("EnumType", nodeType.values.toList())
            }
            is NodeType.OneOf -> {
                node.languageType[OutputLanguage.Kotlin] = KotlinEntry.KotlinOneOf(nodeType.parent!!.nodeName)
            }
            is NodeType.BuiltinType -> { /* pass */
            }
            else -> {
                throw Exception("Handle unknown node type ${node.nodeType}")
            }
        }

        node.children.forEach { processNode(it, node) }
    }


    /**
     * Removes nesting of OneOfs which are the only element in a data class, using only KotlinType. No actual tree
     * structure is changed.
     */
    private fun restructureNesting(node: BaseNode) {
        logger.debug { "restructureNesting $node" }
        node.children.forEach {
            // fix nesting in children first, before we change things around
            restructureNesting(it)
        }

        when (val languageType = node.languageType[OutputLanguage.Kotlin]) {
            is KotlinEntry.DataClass -> {
                // determine if the data class is only a oneOf container
                val isOneOfContainer = node.children.none {
                    it.languageType[OutputLanguage.Kotlin] !is KotlinEntry.KotlinOneOf
                } && node.children.count {
                    it.languageType[OutputLanguage.Kotlin] is KotlinEntry.KotlinOneOf
                } == 1

                val isUnderRoot = tree.children.contains(node)

                if (isOneOfContainer) {
                    // tag DataClass as skip so we do not generate it
                    languageType.onlyGenerateNested = !isUnderRoot // only when not directly below root do we unnest
                    languageType.skipType = true
                } else {
                    // take out any messages in the children and attach them to the DataClass
                    val nestedTypes = node.children.filter {
                        when (it.languageType[OutputLanguage.Kotlin]) {
                            is KotlinEntry.DataClass -> {
                                true
                            }
                            is KotlinEntry.KotlinStringEnumeration, is KotlinEntry.KotlinOneOf -> true
                            else -> false
                        }
                    }.toList()

                    if (nestedTypes.isNotEmpty()) {
                        logger.info { "Restructuring nesting for $node, moving children $nestedTypes" }
                        nestedTypes.map { it.languageType[OutputLanguage.Kotlin]!! }
                            .forEach {
                                when (it) {
                                    is KotlinEntry.DataClass -> it.onlyGenerateNested = true
                                    is KotlinEntry.KotlinStringEnumeration -> it.onlyGenerateNested = true
                                    is KotlinEntry.KotlinOneOf -> it.onlyGenerateNested = true
                                }
                            }
                        languageType.nestedTypes.addAll(nestedTypes)
                    }
                }
            }
            else -> {
            }
        }
    }

    /**
     * Determines the imports needed for a node.
     *
     * @return List of fully qualified imports
     */
    private fun determineImports(node: BaseNode): List<String> {
        val dependencies = mutableListOf<String>()
        node.children.forEach { child ->
            when (val languageType = child.languageType[OutputLanguage.Kotlin]) {
                is KotlinEntry.KotlinParameter -> {
                    // determine if this elsewhere or a sibling
                    val parameterType = languageType.kotlinType
                    val siblings = node.children - child
                    val isSiblingType = siblings.any { it.languageType[OutputLanguage.Kotlin] == parameterType }
                    val importPath = when (parameterType) {
                        is KotlinEntry.DataClass -> {
                            when (parameterType.packagePath.isNullOrBlank()) {
                                false -> parameterType.importPath()
                                true -> null
                            }
                        }
                        else -> null
                    }
                    if (!isSiblingType && !importPath.isNullOrBlank()) {
                        dependencies.add(importPath)
                    }
                }
                is KotlinEntry.KotlinOneOfParameter -> {
                    val importPath = when (val parameterType = languageType.kotlinType) {
                        is KotlinEntry.DataClass -> {
                            when (parameterType.packagePath.isNullOrBlank()) {
                                false -> parameterType.importPath()
                                true -> null
                            }
                        }
                        else -> null
                    }
                    if (!importPath.isNullOrBlank()) {
                        dependencies.add(importPath)
                    }
                }
            }
        }

        node.children.forEach {
            dependencies.addAll(determineImports(it))
        }

        return dependencies.distinct()
    }

    fun generate() {

        // TODO LDe: Make output folder configurable
        val outputFolder = File("kotlin_out")
        outputFolder.deleteRecursively()
        outputFolder.mkdirs()


        // generate types for every "root" type, i.e. the one on depth 1
        // every other type is nested within those and will be covered by the generators
        tree.children.filter { it.nodeType !is NodeType.BuiltinType }.forEach { node ->
            processNode(node, null)
        }

        // determine all the imports!
        var nodeImports = tree.children.map { node ->
            Pair(node, determineImports(node))
        }.toMap()

        // restructure the nesting to match Kotlin code
        tree.children.filter { it.nodeType !is NodeType.BuiltinType }.forEach { node -> restructureNesting(node) }

        // TODO LDe: This is naught stuff:
        //  recalculate all hashes since we just changed data in the map
        nodeImports = nodeImports.toMap()

        // now generate the kotlin!
        tree.children
            .filter { it.nodeType !is NodeType.BuiltinType }
            .filter {
                when (val languageType = it.languageType[OutputLanguage.Kotlin]) {
                    is KotlinEntry.DataClass -> {
                        // filter out any empty data classes, they violate kotlin and are useless
                        val filter = languageType.nestedTypes.isNotEmpty() || it.children.isNotEmpty()
                        if (!filter) {
                            logger.warn { "Filtering out empty data class ${it.nodeName}" }
                        }
                        filter
                    }
                    else -> true
                }
            }
            .forEach { node ->

                // determine file for this message
                check(node.languageType[OutputLanguage.Kotlin] is KotlinEntry.DataClass) { "Expected DataClass, was ${node.languageType[OutputLanguage.Kotlin]}" }
                val languageType = node.languageType[OutputLanguage.Kotlin] as KotlinEntry.DataClass

                // make folders and output file
                val fileName = fileNameFromType(languageType)
                val segments = kotlinPackage.split(".").toTypedArray()
                val directory = File(Paths.get(outputFolder.absolutePath, *segments).toUri())
                if (!directory.exists()) {
                    directory.mkdirs()
                }
                val file = File(directory, fileName)

                logger.info("Creating file ${file.absolutePath}")

                val os = ByteArrayOutputStream()
                PrintWriter(os).use { printWriter ->

                    // add package declaration if present
                    languageType.packagePath?.let {
                        printWriter.println(kotlinPackageDeclaration(it))
                    }

                    printWriter.println()

                    nodeImports[node]?.let { dependencies ->
                        dependencies.forEach { dependency ->
                            printWriter.println(kotlinImportDeclaration(dependency))
                        }
                    }

                    printWriter.println()

                    generateKotlin(node, target = printWriter)
                }

                val result = os.toString()
                logger.debug { "--\n$result" }

                file.appendText(os.toString())

            }
    }
}