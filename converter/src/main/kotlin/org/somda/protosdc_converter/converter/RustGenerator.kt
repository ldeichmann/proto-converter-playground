package org.somda.protosdc_converter.converter

import org.apache.logging.log4j.kotlin.Logging
import org.somda.protosdc_converter.xmlprocessor.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.PrintWriter
import javax.xml.namespace.QName

class RustGenerator(
    private val tree: BaseNode,
    private val attributeSuffix: String
) {


    companion object : Logging {

        const val INDENT = "    "
        private fun idt(level: Int) = INDENT.repeat(level)
        const val ENUM_DERIVES = "#[derive(Clone, PartialEq, Debug)]"
        const val STRUCT_DERIVES = "#[derive(Clone, PartialEq, Debug)]"
        const val STRUCT_ALLOWS = "#[allow(non_camel_case_types)]"
        const val MODULE_SUFFIX = "Mod"
        const val MOD_SEP = "::"
        const val SUPER = "super${MOD_SEP}"

        // type format
        fun rustOption(data: String) = "::core::option::Option<$data>"
        fun rustVec(data: String) = "::std::vec::Vec<$data>"
        fun rustBox(data: String) = "::std::boxed::Box<$data>"

        // struct format
        fun rustStructDerive(level: Int) = "${idt(level)}${STRUCT_DERIVES}"
        fun rustStructAllow(level: Int) = "${idt(level)}${STRUCT_ALLOWS}"
        fun rustStructStart(level: Int, data: String) = "${idt(level)}pub struct $data {"
        fun rustStructEnd(level: Int) = "${idt(level)}}"

        // field format
        fun rustStructField(level: Int, parameterName: String, parameterType: String, optional: Boolean): String {
            val pType = when (optional) {
                true -> rustOption(parameterType)
                false -> parameterType
            }

            return "${idt(level)}pub $parameterName: $pType,"
        }

        // enum format
        fun rustEnumDerive(level: Int) = "${idt(level)}${ENUM_DERIVES}"
        fun rustEnumStart(level: Int, data: String) = "${idt(level)}pub enum $data {"
        fun rustEnumEnd(level: Int) = "${idt(level)}}"
        fun rustEnumFieldWithType(level: Int, name: String, type: String) = "${idt(level)}$name($type),"
        fun rustEnumField(level: Int, name: String) = "${idt(level)}$name,"

        // module format
        fun rustModuleStart(level: Int, data: String) = "${idt(level)}pub mod $data {"
        fun rustModuleEnd(level: Int) = "${idt(level)}}"

        fun rustModuleName(data: String): String {
            var replaced = "${data}${MODULE_SUFFIX}".camelToSnakeCase()
            // remove ::_ which is not wanted
            replaced = replaced.replace("::_", "::")
            return replaced
        }

        val BUILT_IN_TYPES = mapOf(
            BuiltinTypes.XSD_STRING.qname to RustEntry.RustStruct("String", builtinType = true),
            BuiltinTypes.XSD_DECIMAL.qname to RustEntry.RustStruct("String", builtinType = true),
            // requires separate crate, do later
//            BuiltinTypes.XSD_DECIMAL.qname to RustEntry.RustStruct("BigDecimal", "bigdecimal"),
            BuiltinTypes.XSD_INTEGER.qname to RustEntry.RustStruct("i64", builtinType = true),
            BuiltinTypes.XSD_ULONG.qname to RustEntry.RustStruct("u64", builtinType = true),
            BuiltinTypes.XSD_BOOL.qname to RustEntry.RustStruct("bool", builtinType = true),
            BuiltinTypes.XSD_DATE.qname to RustEntry.RustStruct("String", builtinType = true),
            BuiltinTypes.XSD_DATE_TIME.qname to RustEntry.RustStruct("String", builtinType = true),
            BuiltinTypes.XSD_G_YEAR_MONTH.qname to RustEntry.RustStruct("String", builtinType = true),
            BuiltinTypes.XSD_G_YEAR.qname to RustEntry.RustStruct("String", builtinType = true),
            // TODO: QName type
            BuiltinTypes.XSD_QNAME.qname to RustEntry.RustStruct("String", builtinType = true),
            BuiltinTypes.XSD_LANGUAGE.qname to RustEntry.RustStruct("String", builtinType = true),
            BuiltinTypes.XSD_LONG.qname to RustEntry.RustStruct("i64", builtinType = true),
            BuiltinTypes.XSD_UINT.qname to RustEntry.RustStruct("u32", builtinType = true),
            BuiltinTypes.XSD_INT.qname to RustEntry.RustStruct("i32", builtinType = true),
            BuiltinTypes.XSD_ANY_URI.qname to RustEntry.RustStruct("String", builtinType = true),
            // TODO: We really don't want any in our model I think
//            BuiltinTypes.XSD_ANY_SIMPLE_TYPE.qname to RustEntry.RustStruct("Any", builtinType = true, modulePath = "::std::any"),
            BuiltinTypes.XSD_ANY_SIMPLE_TYPE.qname to RustEntry.RustStruct("String", builtinType = true),
            BuiltinTypes.XSD_DURATION.qname to RustEntry.RustStruct("Duration", builtinType = true, modulePath = "::std::time"),
        )

        val CUSTOM_TYPES = mapOf(
//            QName(Constants.EXT_NAMESPACE, "ExtensionType") to RustEntry.RustStruct("Any", builtinType = true, modulePath = "::std::any"),
            QName(Constants.EXT_NAMESPACE, "ExtensionType") to RustEntry.RustStruct("String", builtinType = true),
        )

        // map of forbidden names as keys and the prefix to add the name to resolve the conflict as value
        val FORBIDDEN_PARAMETER_NAMES = mapOf(
            // ref is a rust keyword
            "ref" to "ref_p",
            // type is a rust keyword
            "type" to "type_p"
        )

        /**
         * Generates the actual .rs file content.
         *
         * @param node to generate rust for
         * @param level the indentation level of the current node
         * @param target the target in which to write the content
         * @param isNestedCall when the call is for a nested type, i.e. a module
         */
        fun generateRust(node: BaseNode, level: Int = 0, target: PrintWriter, isNestedCall: Boolean = false) {
            val rustEntry = node.languageType[OutputLanguage.Rust] as RustEntry

            val skipNested = when (rustEntry) {
                is RustEntry.RustStruct -> rustEntry.onlyGenerateNested
                is RustEntry.RustOneOf -> rustEntry.onlyGenerateNested
                is RustEntry.RustStringEnumeration -> rustEntry.onlyGenerateNested
                else -> false
            } && !isNestedCall

            if (skipNested) {
                return
            }

            val skipType = when (rustEntry) {
                is RustEntry.RustStruct -> rustEntry.skipType
                else -> false
            }

            if (skipType) {
                node.children.forEach { generateRust(it, level = level, target) }
                return
            }

            val header: String = when (skipNested) {
                false -> {
                    val hdr = rustEntry.toRustEnter(level = level)
                    if (hdr.isNotBlank()) {
                        // only write non empty lines, duh
                        target.println(hdr)
                    }
                    hdr
                }
                true -> ""
            }

            // only apply a + 1 offset when we didn't generate a header, i.e. this is just a parameter or something
            val mainOffset = when (header.isBlank()) {
                true -> level
                false -> level + 1
            }

            // generate all nested parameters
            node.children
                .filter { it.languageType[OutputLanguage.Rust] is RustEntry.RustParameter || it.languageType[OutputLanguage.Rust] is RustEntry.RustOneOfParameter }
                .forEach { generateRust(it, level = mainOffset, target) }

            when (skipNested) {
                false -> {
                    val main = rustEntry.toRustMain(level = mainOffset)
                    if (main.isNotBlank()) {
                        target.println(main)
                    }

                    val footer = rustEntry.toRustExit(level = level)
                    if (footer.isNotEmpty()) {
                        target.println(footer)
                    }
                }
            }

            // generate nested types
            node.children
                .filter { it.languageType[OutputLanguage.Rust] !is RustEntry.RustParameter
                        && it.languageType[OutputLanguage.Rust] !is RustEntry.RustOneOfParameter
//                        || it.languageType[OutputLanguage.Rust] !is RustEntry.RustStringEnumeration
                }
                .forEach { generateRust(it, level = level, target) }


        }

        fun typeName(rustType: RustEntry, nestedLevel: Int = 0): String {

            // is builtin
            val isBuiltin = when (rustType) {
                is RustEntry.RustStruct -> rustType.builtinType
                else -> false
            }

            val outnestSuper = when (isBuiltin) {
                false -> SUPER.repeat(nestedLevel)
                true -> ""
            }

            fun getRelevantModuleChunk(mod: String): String {
                if (isBuiltin) {
                    return "$mod::"
                }
                val parts = mod.split(MOD_SEP)
                return parts.slice(nestedLevel until parts.size).joinToString(separator = "::", postfix = "::")
            }

            return when (rustType) {
                is RustEntry.RustStruct -> (rustType.modulePath?.let { getRelevantModuleChunk(it) } ?: outnestSuper) + rustType.typeName
                is RustEntry.RustStringEnumeration -> (rustType.modulePath?.let { getRelevantModuleChunk(it) } ?: outnestSuper) + rustType.enumName
                else -> throw Exception("Cannot handle $rustType in typeName")
            }
        }

        private fun fileNameFromType(rustType: RustEntry.RustStruct): String {
            var name = rustType.fileName ?: rustType.typeNameRaw
            if (!name.endsWith(".rs")) {
                name += ".rs"
            }
            return name
        }

        fun String.camelToSnakeCase() = fold(StringBuilder(length)) { acc, c ->
            if (c in 'A'..'Z') (if (acc.isNotEmpty()) acc.append('_') else acc).append(c + ('a' - 'A'))
            else acc.append(c)
        }.toString()

        fun toParameterName(string: String) = string.camelToSnakeCase()
        fun toEnumParameterName(string: String) = string.capitalize();

    }

    private fun determineEnumParameterName(node: BaseNode, parameter: NodeType.Parameter): String {
        // first letter must be lower case
        val name = toEnumParameterName(
            when {
                parameter.wasAttribute -> "${node.nodeName}$attributeSuffix"
                else -> node.nodeName
            }
        )

        // swap forbidden names
        return FORBIDDEN_PARAMETER_NAMES[name.lowercase()] ?: name
    }

    private fun determineParameterName(node: BaseNode, parameter: NodeType.Parameter): String {
        // first letter must be lower case
        val name = toParameterName(
            when {
                parameter.wasAttribute -> "${node.nodeName}$attributeSuffix"
                else -> node.nodeName
            }
        )

        // swap forbidden names
        return FORBIDDEN_PARAMETER_NAMES[name.lowercase()] ?: name
    }


    /**
     * Class for modelling rust structs and converting them into actual code.
     */
    sealed class RustEntry : BaseLanguageType() {

        abstract fun toRustEnter(level: Int = 0, nestedCall: Boolean = false): String

        abstract fun toRustExit(level: Int = 0, nestedCall: Boolean = false): String

        abstract fun toRustMain(level: Int = 0, nestedCall: Boolean = false): String


        data class RustStruct(
            val typeName: String,
            val builtinType: Boolean = false,
            var modulePath: String? = null,
            val wasAttribute: Boolean = false,
            var fileName: String? = null,
            private val _typeNameRaw: String? = null,
            val nestedTypes: MutableList<BaseNode> = mutableListOf(),
            var onlyGenerateNested: Boolean = false,
            var skipType: Boolean = false
        ) : RustEntry() {
            val typeNameRaw: String = _typeNameRaw ?: typeName

            override fun toRustEnter(level: Int, nestedCall: Boolean): String {
                return "${rustStructDerive(level = level)}\n${rustStructStart(level = level, data = typeName)}"
            }

            override fun toRustExit(level: Int, nestedCall: Boolean): String {
                var returnValue = rustStructEnd(level = level)
                if (nestedTypes.isNotEmpty()) {
                    returnValue += "\n"
                    returnValue += rustModuleStart(level = level, data = rustModuleName(typeName))
                    returnValue += "\n"

                    val os = ByteArrayOutputStream()
                    PrintWriter(os).use { printWriter ->
                        nestedTypes.forEach { nestedType ->
                            generateRust(nestedType, target = printWriter, level = level + 1, isNestedCall = true)
                        }
                    }

                    returnValue += os.toString()
                    returnValue += rustModuleEnd(level = level)

                }
                // TODO: Handle nested types
                return returnValue
            }

            // no "main" here, just the body
            override fun toRustMain(level: Int, nestedCall: Boolean): String = ""

        }

        /**
         * Enum with typed parameters, different from normal enum.
         */
        data class RustOneOfParameter(
            val rustType: RustEntry,
            val parameterName: String,
            val nestingLevel: Int = 0,
            var onlyGenerateNested: Boolean = false,
            val box: Boolean = false
        ) : RustEntry() {
            override fun toRustEnter(level: Int, nestedCall: Boolean): String = ""

            override fun toRustExit(level: Int, nestedCall: Boolean): String = ""

            override fun toRustMain(level: Int, nestedCall: Boolean): String {
                return rustEnumFieldWithType(
                    level = level,
                    name = parameterName,
                    type = when (box) {
                        false -> typeName(rustType, nestedLevel = nestingLevel)
                        true -> rustBox(typeName(rustType, nestedLevel = nestingLevel))
                    })
            }
        }

        data class RustParameter(
            val rustType: RustEntry,
            val parameterName: String,
            val list: Boolean = false,
            val optional: Boolean = false,
            val nestingLevel: Int = 0,
            val box: Boolean = false
        ) : RustEntry() {
            override fun toRustEnter(level: Int, nestedCall: Boolean): String = ""

            override fun toRustExit(level: Int, nestedCall: Boolean): String = ""

            override fun toRustMain(level: Int, nestedCall: Boolean): String {
                return when {
                    list -> {
                        // there is no such thing as an optional (nullable) list, so these can be merged
                        val parameterType = rustVec(typeName(rustType, nestedLevel = nestingLevel))
                        rustStructField(
                            level = level, parameterName = parameterName,
                            parameterType = parameterType, optional = false
                        )
                    }
                    else -> {
                        val parameterTypeName = when (box) {
                            false -> typeName(rustType, nestedLevel = nestingLevel)
                            true -> rustBox(typeName(rustType, nestedLevel = nestingLevel))
                        }

                        rustStructField(
                            level = level, parameterName = parameterName,
                            parameterType = parameterTypeName, optional = optional
                        )
                    }
                }
            }
        }

        data class RustStringEnumeration(
            val enumName: String,
            val enumValues: List<String>,
            val modulePath: String?,
            var onlyGenerateNested: Boolean = false,
        ) : RustEntry() {
            override fun toRustEnter(level: Int, nestedCall: Boolean): String =
                "${rustStructAllow(level = level)}\n${rustEnumDerive(level = level)}\n${rustEnumStart(level = level, data = enumName)}"

            override fun toRustExit(level: Int, nestedCall: Boolean): String = rustEnumEnd(level = level)

            override fun toRustMain(level: Int, nestedCall: Boolean): String {
                return enumValues.map {
                    rustEnumField(level = level, name = it)
                }.joinToString(separator = "\n")
            }
        }

        data class RustOneOf(
            val oneOfName: String,
            var onlyGenerateNested: Boolean = false,
        ) : RustEntry() {
            override fun toRustEnter(level: Int, nestedCall: Boolean): String =
                "${rustEnumDerive(level = level)}\n${rustEnumStart(level = level, data = oneOfName)}"

            override fun toRustExit(level: Int, nestedCall: Boolean): String = rustEnumEnd(level = level)

            override fun toRustMain(level: Int, nestedCall: Boolean): String = ""

        }

    }

    init {
        // attach builtin types to their nodes
        BUILT_IN_TYPES.forEach { (typeQName, rustStruct) ->
            val node = tree.findQName(typeQName)!!
            node.languageType[OutputLanguage.Rust] = rustStruct
            logger.info("Attaching builtin type $typeQName to rust type ${rustStruct.typeName}")
        }
        // attach builtin types to their nodes
        CUSTOM_TYPES.forEach { (typeQName, rustStruct) ->
            val node = tree.findQName(typeQName)!!
            node.languageType[OutputLanguage.Rust] = rustStruct
            logger.info("Attaching custom type $typeQName to rust type ${rustStruct.typeName}")
        }
    }

    private fun getModulePath(parentNode: BaseNode?): String? {
        // is nested when parent present and parent is a Struct too
        val parentModule = parentNode?.let { it ->
            when (val parentType = it.languageType[OutputLanguage.Rust]) {
                is RustEntry.RustStruct -> {
                    val prefix = parentType.modulePath?.let {
                        "${it}$MOD_SEP"
                    } ?: ""
                    // struct was nested too, append
                    "$prefix${rustModuleName(parentType.typeName)}"
                }
                else -> ""
            }
        }

        return parentModule
    }

    private fun isRecursiveNode(targetNode: BaseNode, currentNode: BaseNode): Boolean {
        val visited = HashSet<BaseNode>()
        return isRecursiveNode(targetNode, currentNode, visited)
    }


    private fun isRecursiveNode(targetNode: BaseNode, currentNode: BaseNode, visited: MutableSet<BaseNode>): Boolean {
        if (currentNode == targetNode) {
            return true
        }

        if (visited.contains(currentNode)) {
            return false;
        }

        visited.add(currentNode)
        val nodeType = currentNode.nodeType!!

        return when (nodeType) {
            is NodeType.Message -> currentNode.children.any { isRecursiveNode(targetNode, it, visited) }
            is NodeType.OneOf -> currentNode.children.any { isRecursiveNode(targetNode, it, visited) }
            is NodeType.Parameter -> nodeType.parameterType.parent?.let { isRecursiveNode(targetNode, it, visited) } ?: false
            is NodeType.BuiltinType -> false
            is NodeType.Root -> false
            is NodeType.StringEnumeration -> false
        }
    }

    private fun processNode(node: BaseNode, parentNode: BaseNode?) {
        logger.info("Processing $node")

        node.clusteredTypes?.let { cluster ->
            logger.info("Received clustered type ${node.nodeName}, bulk handling ${cluster.joinToString { it.nodeName }} as well")
            logger.info("Cluster node types are ${cluster.joinToString { it.nodeType!!::class.simpleName!! }}")

            if (node.clusterHandled[OutputLanguage.Rust] == true) {
                logger.info("Cluster with ${node.nodeName} is already handled, skipping")
                return@let
            }

            // generate all message language types so the references to not die. also enforce clustering in one file
            // order matters, this node is now enforcing naming rules, which is why it must be handled first
            val totalCluster = listOf(node) + cluster
            totalCluster.forEach { clusterNode ->
                when (val nodeType = clusterNode.nodeType) {
                    is NodeType.Message -> {
                        logger.info("Processing cluster node $clusterNode")

                        // is nested when parent present and parent is a Struct too
                        val parentModule = getModulePath(parentNode)

                        val languageType = RustEntry.RustStruct(
                            typeName = clusterNode.nodeName,
                            _typeNameRaw =  clusterNode.nodeName,
                            modulePath = parentModule
                        )
                        clusterNode.languageType[OutputLanguage.Rust] = languageType
                    }
                    else -> throw Exception(
                        "Can only cluster nodes which are messages, not $nodeType." +
                                " How would that even work for anything else?"
                    )
                }
            }

            // mark cluster as resolved
            totalCluster.forEach { node -> node.clusterHandled[OutputLanguage.Rust] = true }

            // no return or modified branching, we've handled the cluster and normal processing can continue
        }

        when (val nodeType = node.nodeType) {
            is NodeType.Message -> {
                logger.debug("Processing message $node")
                if (node.languageType[OutputLanguage.Rust] != null) {
                    logger.info("Node ${node.nodeName} already has Rust language type attached, skipped")
                } else {
                    // is nested when parent present and parent is a Struct too
                    val parentModule = getModulePath(parentNode)

                    node.languageType[OutputLanguage.Rust] = RustEntry.RustStruct(
                        typeName = node.nodeName,
                        _typeNameRaw = node.nodeName,
                        modulePath = parentModule
                    )
                }
            }
            is NodeType.Parameter -> {
                logger.debug("Processing parameter ${nodeType.parentNode?.nodeName}")
                when (val parentLanguageType = parentNode?.let { it.languageType[OutputLanguage.Rust] }) {
                    is RustEntry.RustOneOf -> {
                        val parameterName = determineEnumParameterName(node, nodeType)
                        val parameterType = nodeType.parameterType.parent!!.languageType[OutputLanguage.Rust] as RustEntry.RustStruct

                        // determine if this parameterNode references the OneOf again
                        val box = isRecursiveNode(parentNode, node)

                        node.languageType[OutputLanguage.Rust] = RustEntry.RustOneOfParameter(
                            rustType =  parameterType,
                            parameterName = parameterName,
                            box = box
                        )
                    }
                    else -> {
                        val parameterName = determineParameterName(node, nodeType)
                        // determine the nesting level of the parameter
                        val parameterNestingLevel = when (parentLanguageType) {
                            is RustEntry.RustStruct -> parentLanguageType.modulePath?.split("::")?.count() ?: 0
                            else -> 0
                        }

                        when(val parameterType = nodeType.parameterType.parent!!.languageType[OutputLanguage.Rust]) {
                            is RustEntry.RustStringEnumeration -> {
                                node.languageType[OutputLanguage.Rust] = RustEntry.RustParameter(
                                    rustType = parameterType,
                                    parameterName = parameterName,
                                    list = nodeType.list,
                                    optional =  nodeType.optional,
                                    nestingLevel = parameterNestingLevel
                                )
                            }
                            is RustEntry.RustStruct -> {
                                val box = parentNode?.let { isRecursiveNode(it, node) } ?: false

                                node.languageType[OutputLanguage.Rust] = RustEntry.RustParameter(
                                    rustType = parameterType,
                                    parameterName = parameterName,
                                    list = nodeType.list,
                                    optional =  nodeType.optional,
                                    nestingLevel = parameterNestingLevel,
                                    box = box
                                )
                            }
                            else -> {
                                throw Exception("Unsupported language type $parameterType")
                            }
                        }
                    }
                }
            }
            is NodeType.StringEnumeration -> {
                // is nested when parent present and parent is a Struct too
                val parentModule = getModulePath(parentNode)

                node.languageType[OutputLanguage.Rust] = RustEntry.RustStringEnumeration(
                    enumName = "EnumType",
                    enumValues = nodeType.values.toList(),
                    modulePath = parentModule
                )
            }
            is NodeType.OneOf -> {
                node.languageType[OutputLanguage.Rust] = RustEntry.RustOneOf(oneOfName = nodeType.parent!!.nodeName)
            }
            // pass
            is NodeType.BuiltinType -> {}
            else -> {
                throw Exception("Handle unknown node type ${node.nodeType}")
            }
        }

        node.children.forEach { processNode(it, node) }
    }

    private fun restructureNesting(node: BaseNode) {
        logger.debug { "restructureNesting $node" }
        node.children.forEach {
            // fix nesting in children first, before we change things around
            restructureNesting(it)
        }

        when (val languageType = node.languageType[OutputLanguage.Rust]) {
            is RustEntry.RustStruct -> {
                // determine if this is a oneof container
                val isOneOfContainer = node.children.none {
                    it.languageType[OutputLanguage.Rust] !is RustEntry.RustOneOf
                } && node.children.count {
                    it.languageType[OutputLanguage.Rust] is RustEntry.RustOneOf
                } == 1

                val isUnderRoot = tree.children.contains(node)

                if (isOneOfContainer) {
                    // tag DataClass as skip so we do not generate it
                    languageType.onlyGenerateNested = !isUnderRoot // only when not directly below root do we unnest
                    languageType.skipType = true
                } else {
                    // take out any messages in the children and attach them to the DataClass
                    val nestedTypes = node.children.filter {
                        when (it.languageType[OutputLanguage.Rust]) {
                            is RustEntry.RustStruct -> {
                                true
                            }
                            is RustEntry.RustStringEnumeration, is RustEntry.RustOneOf -> true
                            else -> false
                        }
                    }.toList()

                    if (nestedTypes.isNotEmpty()) {
                        logger.info { "Restructuring nesting for $node, moving children $nestedTypes" }
                        nestedTypes.map { it.languageType[OutputLanguage.Rust]!! }
                            .forEach {
                                when (it) {
                                    is RustEntry.RustStruct -> it.onlyGenerateNested = true
                                    is RustEntry.RustStringEnumeration -> it.onlyGenerateNested = true
                                    is RustEntry.RustOneOf -> it.onlyGenerateNested = true
                                }
                            }
                        languageType.nestedTypes.addAll(nestedTypes)
                    }

                }
            }
            else -> {}
        }
    }

    fun generate() {

        // TODO LDe: Make output folder configurable
        val outputFolder = File("rust_out")
        outputFolder.deleteRecursively()
        outputFolder.mkdirs()

        val outputFile = File(outputFolder, "out.rs")


        // generate types for every "root" type, i.e. the one on depth 1
        // every other type is nested within those and will be covered by the generators
        tree.children.filter { it.nodeType !is NodeType.BuiltinType }.forEach { node ->
            processNode(node, null)
        }

        tree.children.filter { it.nodeType !is NodeType.BuiltinType }.forEach { node ->
            restructureNesting(node)
        }



        // TODO: determine imports?

        tree.children
            .filter { it.nodeType !is NodeType.BuiltinType }
            .filter {
                when (val languageType = it.languageType[OutputLanguage.Rust]) {
                    is RustEntry.RustStruct -> {
                        // filter out empty structs
                        val filter = languageType.nestedTypes.isNotEmpty() || it.children.isNotEmpty()
                        if (!filter) {
                            logger.warn { "Filtering out empty data class ${it.nodeName}" }
                        }
                        filter
                    }
                    else -> true
                }
            }
            .forEach { node ->

                // determine file for this message
                check(node.languageType[OutputLanguage.Rust] is RustEntry.RustStruct) { "Expected RustStruct, was ${node.languageType[OutputLanguage.Rust]}" }
                val languageType = node.languageType[OutputLanguage.Rust] as RustEntry.RustStruct

//                val fileName = fileNameFromType(languageType)
//                logger.info("Creating file ${fileName}")

                val os = ByteArrayOutputStream()
                PrintWriter(os).use { printWriter ->

//                    // add package declaration if present
//                    languageType.packagePath?.let {
//                        printWriter.println(KotlinGenerator.kotlinPackageDeclaration(it))
//                    }


                    generateRust(node, target = printWriter)
                }

                val result = os.toString()
                logger.debug { "--\n$result" }


                outputFile.appendText(os.toString())
            }
    }
}