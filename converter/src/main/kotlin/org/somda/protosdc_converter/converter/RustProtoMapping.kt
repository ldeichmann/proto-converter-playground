package org.somda.protosdc_converter.converter

import org.apache.logging.log4j.kotlin.Logging
import org.somda.protosdc_converter.converter.Proto3Generator.Companion.camelCaseToConstant
import org.somda.protosdc_converter.converter.RustGenerator.Companion.camelToSnakeCase
import org.somda.protosdc_converter.converter.RustGenerator.Companion.rustModuleName
import org.somda.protosdc_converter.xmlprocessor.BaseNode
import org.somda.protosdc_converter.xmlprocessor.NodeType
import org.somda.protosdc_converter.xmlprocessor.OutputLanguage
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.PrintWriter

typealias RustMapping = Pair<String, String>
typealias RustMapper = String // just to indicate this must be a fully qualified name


const val PROTO_TO_RUST_DURATION_MAPPER_NAME = "map_duration"
const val PROTO_TO_RUST_DURATION_MAPPER = """pub fn $PROTO_TO_RUST_DURATION_MAPPER_NAME(value: ::prost_types::Duration) -> Result<::std::time::Duration, MappingError> {
    Ok(::std::time::Duration::new(value.seconds as u64, value.nanos as u32))
}
"""

const val PROST_TO_DUMMY_ANY_MAPPER_NAME = "map_dummy_prost_any"
const val PROST_TO_DUMMY_ANY_MAPPER = """// TODO: This needs to replaced one proper extension handling is coming
pub fn $PROST_TO_DUMMY_ANY_MAPPER_NAME(_value: ::prost_types::Any) -> Result<String, MappingError> {
    Ok(String::new())
}
"""

const val DUMMY_ANY_TO_PROST_MAPPER_NAME = "map_prost_dummy_any"
const val DUMMY_ANY_TO_PROST_MAPPER = """// TODO: This needs to replaced one proper extension handling is coming
pub fn $DUMMY_ANY_TO_PROST_MAPPER_NAME(_value: String) -> ::prost_types::Any {
    panic!("Do not map any!")
}
"""

const val MAPPING_ERROR_FROM_PROTO_GENERIC = "FromProtoGeneric"
const val MAPPING_ERROR_FROM_PROTO_GENERIC_MESSAGE = "message"
const val MAPPING_ERROR_FROM_PROTO_MANDATORY_MISSING = "FromProtoMandatoryMissing"
const val MAPPING_ERROR_FROM_PROTO_MANDATORY_MISSING_FIELD_NAME = "field_name"
const val MAPPING_ERROR_ENUM_NAME = "MappingError"
const val MAPPING_ERROR_ENUM = """#[derive(Debug, Clone, PartialEq)]
pub enum $MAPPING_ERROR_ENUM_NAME {
    $MAPPING_ERROR_FROM_PROTO_GENERIC { $MAPPING_ERROR_FROM_PROTO_GENERIC_MESSAGE: String },
    $MAPPING_ERROR_FROM_PROTO_MANDATORY_MISSING { $MAPPING_ERROR_FROM_PROTO_MANDATORY_MISSING_FIELD_NAME: String }
}
"""

class RustProtoMapping(
    val tree: BaseNode,
    val protoCustomTypesTree: BaseNode,
    val rustModelModule: String,
    val protoModelModule: String,
) {

    companion object: Logging {

        const val GOOGLE_PROTO_PREFIX = "google.protobuf"

        private const val INDENT = "    "
        private const val IT = "it"
        private const val VALUE = "value"
        private const val MATCH = "match"
        private const val SOME = "Some"
        private const val BOX = "Box"
        private const val BOX_NEW = "$BOX::new"
        private const val SOME_VAL = "some_val"
        private const val NONE = "None"
        private const val CASE_VAL = "case_val"
        private const val INTO = "into()"
        private const val OK = "Ok"
        private const val ERR = "Err"
        private const val Q = "?"

        fun rustMappingErrorMandatoryMissing(field_name: String): String {
            return "$ERR($MAPPING_ERROR_ENUM_NAME${RustGenerator.MOD_SEP}$MAPPING_ERROR_FROM_PROTO_MANDATORY_MISSING { $MAPPING_ERROR_FROM_PROTO_MANDATORY_MISSING_FIELD_NAME: \"$field_name\".to_string() })"
        }

        fun mapFunctionName(functionName: String) = "map_$functionName"

        // Rust -> Proto will succeed
        fun mapFunctionStart(indent: Int, functionName: String, targetType: String, sourceType: String) =
            "${INDENT.repeat(indent)}pub fn $functionName(value: $sourceType) -> $targetType {"
        // Proto -> Rust can fail
        fun mapFunctionStartResult(indent: Int, functionName: String, targetType: String, sourceType: String) =
            "${INDENT.repeat(indent)}pub fn $functionName(value: $sourceType) -> Result<$targetType, MappingError> {"

        fun mapFunctionEnd(indent: Int) = "${INDENT.repeat(indent)}}"

        fun rustReturnStartOk(indent: Int, targetType: String) = "${INDENT.repeat(indent)}$OK($targetType {"
        fun rustReturnEndOk(indent: Int) = "${INDENT.repeat(indent)}})"
        fun rustReturnStart(indent: Int, targetType: String) = "${INDENT.repeat(indent)}$targetType {"
        fun rustReturnEnd(indent: Int) = "${INDENT.repeat(indent)}}"

        fun rustFromProtoList(indent: Int, rustField: String, protoField: String, mappingFunction: String? = null, into: Boolean = false): String {
            val prefix = "${INDENT.repeat(indent)}$rustField: $VALUE.$protoField.into_iter().map(|$IT| "
            val body = when (mappingFunction != null) {
                true -> "$mappingFunction($IT)"
                false -> if (into) "$IT.$INTO" else IT
            }
            return when (mappingFunction != null) {
                true -> "$prefix$body).collect::<Result<Vec<_>,_>>()$Q,"
                false -> "$prefix$body).collect(),"
            }
        }

        fun protoFromRustList(indent: Int, rustField: String, protoField: String, mappingFunction: String? = null, into: Boolean = false): String {
            val prefix = "${INDENT.repeat(indent)}$protoField: $VALUE.$rustField.into_iter().map(|$IT| "
            val body = when (mappingFunction != null) {
                true -> "$mappingFunction($IT)"
                false -> if (into) "$IT.$INTO" else IT
            }
            return "$prefix$body).collect(),"
        }


        fun rustFromOptionalProto(indent: Int, rustField: String, protoField: String): String {
            return "${INDENT.repeat(indent)}$rustField: $MATCH $VALUE.$protoField {"
        }
        fun protoFromOptionalRust(indent: Int, rustField: String, protoField: String): String {
            return "${INDENT.repeat(indent)}$protoField: $MATCH $VALUE.$rustField {"
        }

        fun rustFromOptionalProtoEnum(indent: Int, rustField: String, protoField: String, enumConverter: String): String {
            return "${INDENT.repeat(indent)}$rustField: $MATCH $enumConverter($VALUE.$protoField) {"
        }

        fun protoFromEnum(indent: Int, rustField: String, protoField: String, enumConverter: String, mappingFunction: String?, into: Boolean = false): String {
            val value = when (mappingFunction != null) {
                true -> "$mappingFunction($VALUE.$rustField)"
                false -> if (into) "$VALUE.$INTO" else VALUE
            }

            return "${INDENT.repeat(indent)}$protoField: $value $enumConverter,"
        }

//        fun protoFromOptionalRustSome(indent: Int, mappingFunction: String?, into: Boolean = false, box: Boolean = false): String {
//            val prefix = "${INDENT.repeat(indent)}$SOME($SOME_VAL) => "
//            val someVal = when (box) {
//                true -> "*$SOME_VAL"
//                false -> SOME_VAL
//            }
//            val body = when (mappingFunction != null) {
//                true -> "$mappingFunction($someVal)$Q"
//                false -> if (into) "$someVal.$INTO" else someVal
//            }
//            val boxBody = when (box) {
//                true -> "$SOME($BOX_NEW($body))"
//                false -> "$SOME($body)"
//            }
//
//            return "$prefix$boxBody,"
//        }

        fun protoOptionalFromRust(indent: Int, mappingFunction: String?, into: Boolean = false, box: Boolean = false): String {
            val prefix = "${INDENT.repeat(indent)}$SOME($SOME_VAL) => "
            val someVal = when (box) {
                true -> "*$SOME_VAL"
                false -> SOME_VAL
            }
            val body = when (mappingFunction != null) {
                true -> "$mappingFunction($someVal)"
                false -> if (into) "$someVal.$INTO" else someVal
            }
            val boxBody = when (box) {
                true -> "$SOME($BOX_NEW($body))"
                false -> "$SOME($body)"
            }

            return "$prefix$boxBody,"
        }

        fun rustFromOptionalProtoSome(indent: Int, mappingFunction: String?, into: Boolean = false, box: Boolean = false): String {
            val prefix = "${INDENT.repeat(indent)}$SOME($SOME_VAL) => "
            val someVal = when (box) {
                true -> "*$SOME_VAL"
                false -> SOME_VAL
            }
            val body = when (mappingFunction != null) {
                true -> "$mappingFunction($someVal)$Q"
                false -> if (into) "$someVal.$INTO" else someVal
            }
            val boxBody = when (box) {
                true -> "$SOME($BOX_NEW($body))"
                false -> "$SOME($body)"
            }

            return "$prefix$boxBody,"
        }

        fun protoOptionalFromRustNone(indent: Int) = rustFromOptionalProtoNone(indent)
        fun rustFromOptionalProtoNone(indent: Int): String {
            return "${INDENT.repeat(indent)}$NONE => $NONE,"
        }
        fun protoOptionalFromRustEnd(indent: Int) = rustFromOptionalProtoEnd(indent)
        fun rustFromOptionalProtoEnd(indent: Int): String {
            return "${INDENT.repeat(indent)}},"
        }

        fun rustFromOptionalProtoMandatorySome(indent: Int, mappingFunction: String?, into: Boolean = false, box: Boolean = false): String {
            val prefix = "${INDENT.repeat(indent)}$SOME($SOME_VAL) => "
            val someVal = when (box) {
                true -> "*$SOME_VAL"
                false -> SOME_VAL
            }
            val body = when (mappingFunction != null) {
                true -> "$mappingFunction($someVal)$Q"
                false -> if (into) "$someVal.$INTO" else "$someVal"
            }
            val boxBody = when (box) {
                true -> "$BOX_NEW($body)"
                false -> body
            }

            return "$prefix$boxBody,"
        }
        fun rustFromOptionalProtoMandatoryNone(indent: Int, fieldName: String): String {
            return "${INDENT.repeat(indent)}$NONE => return ${rustMappingErrorMandatoryMissing(fieldName)}"
        }

        fun protoFromRustField(indent: Int, rustField: String, protoField: String, mappingFunction: String?, into: Boolean = false, box: Boolean = false): String {
            val prefix = "${INDENT.repeat(indent)}$protoField: "
            val value = when (box) {
                true -> "*$VALUE"
                false -> VALUE
            }
            val body = when (mappingFunction != null) {
                true -> "$mappingFunction($value.$rustField)"
                false -> if (into) "$value.$rustField.$INTO" else "$value.$rustField"
            }
            val boxBody = when (box) {
                true -> "$BOX_NEW($body)"
                false -> body
            }

            return "$prefix$boxBody,"
        }

        fun protoFromRustFieldOptionalTarget(indent: Int, rustField: String, protoField: String, mappingFunction: String?, into: Boolean = false, box: Boolean = false): String {
            val prefix = "${INDENT.repeat(indent)}$protoField: $SOME("
            val value = when (box) {
                true -> "*$VALUE"
                false -> VALUE
            }
            val body = when (mappingFunction != null) {
                true -> "$mappingFunction($value.$rustField)"
                false -> if (into) "$value.$rustField.$INTO" else "$value.$rustField"
            }
            val boxBody = when (box) {
                true -> "$BOX_NEW($body)"
                false -> body
            }

            return "$prefix$boxBody),"
        }

        fun rustFromProtoField(indent: Int, rustField: String, protoField: String, mappingFunction: String?, into: Boolean = false, box: Boolean = false): String {
            val prefix = "${INDENT.repeat(indent)}$rustField: "
            val value = when (box) {
                true -> "*$VALUE"
                false -> VALUE
            }
            val body = when (mappingFunction != null) {
                true -> "$mappingFunction($value.$protoField)$Q"
                false -> if (into) "$value.$protoField.$INTO" else "$value.$protoField"
            }
            val boxBody = when (box) {
                true -> "$BOX_NEW($body)"
                false -> body
            }

            return "$prefix$boxBody,"
        }

        fun rustFromProtoEnumStart(indent: Int): String =
            "${INDENT.repeat(indent)}$MATCH $VALUE {"
        fun rustFromProtoEnumCase(
            indent: Int,
            sourceType: String,
            sourceValue: String,
            targetType: String,
            targetValue: String
        ) = "${INDENT.repeat(indent)}$sourceType${RustGenerator.MOD_SEP}$sourceValue => $OK($targetType${RustGenerator.MOD_SEP}$targetValue),"
        fun rustFromProtoEnumElse(indent: Int) = "${INDENT.repeat(indent)}_ => panic!(\"Unknown enum value {:?}\", $VALUE)" // TODO
        fun rustFromProtoEnumEnd(indent: Int): String =
            "${INDENT.repeat(indent)}}"

        fun protoFromRustEnumCase(
            indent: Int,
            sourceType: String,
            sourceValue: String,
            targetType: String,
            targetValue: String
        ) = "${INDENT.repeat(indent)}$sourceType${RustGenerator.MOD_SEP}$sourceValue => $targetType${RustGenerator.MOD_SEP}$targetValue,"


        fun protoFromRustOneOfStart(indent: Int, typeName: String) = "${INDENT.repeat(indent)}$MATCH $VALUE {"

        fun rustFromProtoOneOfStart(indent: Int, typeName: String) = "${INDENT.repeat(indent)}$MATCH $VALUE.$typeName {"
        fun protoFromRustOneOfCase(indent: Int, protoCase: String, rustCase: String, mappingFunction: String? = null, into: Boolean = false, box: Boolean = false, protoFieldName: String, protoStructName: String): String {
            val prefix = "${INDENT.repeat(indent)}$rustCase($CASE_VAL) => $protoStructName { $protoFieldName: $SOME($protoCase("
            val caseVal = when (box) {
                true -> "*$CASE_VAL"
                false -> CASE_VAL
            }

            val body = when (mappingFunction != null) {
                true -> "$mappingFunction($caseVal)"
                false -> if (into) "$caseVal.$INTO" else caseVal
            }
            val boxBody = when (box) {
                true -> "$BOX_NEW($body)"
                false -> body
            }
            return "$prefix$boxBody)) },"
        }
        fun rustFromProtoOneOfOptionalCase(indent: Int, protoCase: String, rustCase: String, mappingFunction: String? = null, into: Boolean = false, box: Boolean = false): String {
            val prefix = "${INDENT.repeat(indent)}$SOME($protoCase($CASE_VAL)) => $OK($rustCase("
            val caseVal = when (box) {
                true -> "*$CASE_VAL"
                false -> CASE_VAL
            }

            val body = when (mappingFunction != null) {
                true -> "$mappingFunction($caseVal)$Q"
                false -> if (into) "$caseVal.$INTO" else caseVal
            }
            val boxBody = when (box) {
                true -> "$BOX_NEW($body)"
                false -> body
            }
            return "$prefix$boxBody)),"
        }
//        fun rustFromProtoOneOfOptionalNone(indent: Int) = "${INDENT.repeat(indent)}None => panic!(\"Unknown enum value {:?}\", $VALUE)" // TODO
//        fun rustFromProtoOneOfOptionalNone(indent: Int) = "${INDENT.repeat(indent)}None => panic!(\"Unknown enum value case\")"
        fun rustFromProtoOneOfOptionalNone(indent: Int, field_name: String = "unknown"): String {
            return "${INDENT.repeat(indent)}None => return ${rustMappingErrorMandatoryMissing(field_name)}"
        }
        fun protoFromRustOneOfEnd(indent: Int) = rustFromProtoOneOfEnd(indent)
        fun rustFromProtoOneOfEnd(indent: Int) = "${INDENT.repeat(indent)}}"

        val PROTO_COMPILED_TO_RUST_PRIMITIVE = mapOf(
            "double" to "f64",
            "float" to "f32",
            "int32" to "i32",
            "int64" to "i64",
            "uint32" to "u32",
            "uint64" to "u64",
            "sint32" to "i32",
            "sint64" to "i64",
            "fixed32" to "u32",
            "fixed64" to "u64",
            "sfixed32" to "i32",
            "sfixed64" to "i64",
            "bool" to "bool"
        )

        val PROTO_COMPILED_TO_RUST_MAP = PROTO_COMPILED_TO_RUST_PRIMITIVE + mapOf(
            "string" to "String",
            "bytes" to "Vec<u8>",
        )

        val WKT_TO_RUST_TYPE = mapOf(
            // obviously these are all optionals then
            "${GOOGLE_PROTO_PREFIX}.BoolValue" to "bool",
            "${GOOGLE_PROTO_PREFIX}.BytesValue" to "Vec<u8>",
            "${GOOGLE_PROTO_PREFIX}.DoubleValue" to "f64",
            "${GOOGLE_PROTO_PREFIX}.FloatValue" to "f32",
            "${GOOGLE_PROTO_PREFIX}.Int32Value" to "i32",
            "${GOOGLE_PROTO_PREFIX}.Int64Value" to "i64",
            "${GOOGLE_PROTO_PREFIX}.StringValue" to "String",
            "${GOOGLE_PROTO_PREFIX}.UInt32Value" to "u32",
            "${GOOGLE_PROTO_PREFIX}.UInt64Value" to "u64",

            "${GOOGLE_PROTO_PREFIX}.Duration" to "::prost_types::Duration",
            "${GOOGLE_PROTO_PREFIX}.Timestamp" to "::prost_types::Timestamp",
            "${GOOGLE_PROTO_PREFIX}.Any" to "::prost_types::Any",
        )

        val BUILTIN_TYPES = PROTO_COMPILED_TO_RUST_MAP.values.toSet()

        val STD_TYPE_MODULE_PREFIX: Set<String> = setOf("::std", "::prost_types")

        val FORBIDDEN_PARAMETER_NAMES_PROTO: Map<String, String> = RustGenerator.FORBIDDEN_PARAMETER_NAMES.map { it.key to "r#${it.key}" }.toMap()
    }

    private val protoToRustMapperMap: MutableMap<RustMapping, RustMapper> = mutableMapOf()
    private val rustToProtoMapperMap: MutableMap<RustMapping, RustMapper> = mutableMapOf()

    private val intoConversionsSet: MutableSet<RustMapping> = mutableSetOf()

    init {
        protoToRustMapperMap.put(Pair("::prost_types::Duration", "::std::time::Duration"), PROTO_TO_RUST_DURATION_MAPPER_NAME)
        protoToRustMapperMap.put(Pair("::prost_types::Any", "String"), PROST_TO_DUMMY_ANY_MAPPER_NAME)

        intoConversionsSet.add(Pair("::std::time::Duration", "::prost_types::Duration"))
        rustToProtoMapperMap.put(Pair("String", "::prost_types::Any"), DUMMY_ANY_TO_PROST_MAPPER_NAME)
    }

    private fun protoModelModule(data: String): String {
        return when (PROTO_COMPILED_TO_RUST_MAP.contains(data) || WKT_TO_RUST_TYPE.contains(data)) {
            true -> data
            false -> when (data.startsWith(protoModelModule + RustGenerator.MOD_SEP)) {
                true -> data
                false -> protoModelModule + RustGenerator.MOD_SEP + data // do not duplicate
            }
        }
    }
    private fun rustModelModule(data: String): String {
        return when (BUILTIN_TYPES.contains(data)) {
            true -> data
            false -> when (data.startsWith(rustModelModule + RustGenerator.MOD_SEP)) {
                true -> data
                false -> rustModelModule + RustGenerator.MOD_SEP + data // do not duplicate
            }
        }
    }

    private fun lookupProtoToRustType(proto: String): String {
        return PROTO_COMPILED_TO_RUST_MAP[proto] ?: run {
            when (proto.startsWith(GOOGLE_PROTO_PREFIX)) {
                // well known type is just a base type in an option
                true -> WKT_TO_RUST_TYPE[proto] ?: proto
                false -> proto
            }
        }
    }

    private fun getProtoEntry(node: BaseNode): Proto3Generator.ProtoEntry {
        return node.languageType[OutputLanguage.Proto] as Proto3Generator.ProtoEntry
    }

    private fun getRustEntry(node: BaseNode): RustGenerator.RustEntry {
        return node.languageType[OutputLanguage.Rust] as RustGenerator.RustEntry
    }

    private fun getParentForNode(node: BaseNode): BaseNode? {
        if (tree.children.contains(node)) {
            return tree
        }
        tree.traversePreOrder().forEach { child ->
            // referential equality, yay
            child.children.forEach { megaChild ->
                if (node === megaChild) {
                    return child
                }
            }
        }
        // TODO LDe: This shouldn't be, it's hacky
        protoCustomTypesTree.traversePreOrder().forEach { child ->
            child.children.forEach { megaChild ->
                if (node === megaChild) {
                    return child
                }
            }
        }
        return null
    }

    private fun getNodeForProtoEntry(protoEntry: Proto3Generator.ProtoEntry): BaseNode? {
        tree.traversePreOrder().forEach { child ->
            if (child.nodeType !is NodeType.Root) {
                val nodeProtoEntry = getProtoEntry(child)
                if (protoEntry === nodeProtoEntry) {
                    return child
                }
            }
        }
        protoCustomTypesTree.traversePreOrder().forEach { child ->
            if (child.nodeType !is NodeType.Root) {
                val nodeProtoEntry = getProtoEntry(child)
                if (protoEntry === nodeProtoEntry) {
                    return child
                }
            }
        }
        return null
    }

    private fun getNodeForRustEntry(rustEntry: RustGenerator.RustEntry): BaseNode? {
        tree.traversePreOrder().forEach { child ->
            if (child.nodeType !is NodeType.Root) {
                val nodeRustEntry = getRustEntry(child)
                if (rustEntry === nodeRustEntry) {
                    return child
                }
            }
        }
        return null
    }

    private fun protoRustModuleName(data: String): String {
        return data.camelToSnakeCase().replace("::_", "::")
    }

    private fun getFullyQualifiedProtoTypeName(baseNode: BaseNode): String {
        val protoEntry = getProtoEntry(baseNode)
        // every node has a parent, unless you're asking for the root parent, which isn't a valid node for mapping anyway
        val parentOpt = getParentForNode(baseNode)
        val parent = parentOpt!!
        val parentIsRoot = parent.nodeType is NodeType.Root

        when (protoEntry) {
            is Proto3Generator.ProtoEntry.ProtoType -> {
                val localTypeName = when (parentIsRoot) {
                    true -> protoEntry.typeName
                    // if the parent isn't the root, we need to resolve the module name for all parents
                    false -> protoRustModuleName(getFullyQualifiedProtoTypeName(parent)) + RustGenerator.MOD_SEP + protoEntry.typeName
                }

                return protoModelModule(localTypeName)
            }
            is Proto3Generator.ProtoEntry.ProtoEnum -> {
                // these literally only exist in the context of their parent, we need to resolve it
                return protoModelModule(protoRustModuleName(getFullyQualifiedProtoTypeName(parent)) + RustGenerator.MOD_SEP + protoEntry.enumName)
            }
            is Proto3Generator.ProtoEntry.ProtoOneOf -> {
                // these literally only exist in the context of their parent, we need to resolve it
                return protoModelModule(protoRustModuleName(getFullyQualifiedProtoTypeName(parent)) + RustGenerator.MOD_SEP + protoEntry.oneOfName)
            }
            is Proto3Generator.ProtoEntry.ProtoParameter -> {
                // the fully qualified type is the target of the parameter
                return getFullyQualifiedProtoTypeName(getNodeForProtoEntry(protoEntry.protoType)!!)
            }
        }
   }

    private fun toProtoEnumName(value: String): String {
        // if all capital, only Capitalize it
        return if (value.any { it.isLowerCase()}) {
            val segments = value.camelCaseToConstant().split("_")
            segments.map { it.lowercase().capitalize() }
                .joinToString(separator = "")
        } else {
            value.lowercase().capitalize()
        }
    }

    private fun getFullyQualifiedRustTypeName(baseNode: BaseNode): String {
        val rustEntry = getRustEntry(baseNode)
        // every node has a parent, unless you're asking for the root parent, which isn't a valid node for mapping anyway
        val parent = getParentForNode(baseNode)!!
        val parentIsRoot = parent.nodeType is NodeType.Root

        val parentIsSkip = when (parentIsRoot) {
            false -> when (val parentLanguageType = getRustEntry(parent)) {
                is RustGenerator.RustEntry.RustStruct -> parentLanguageType.skipType
                else -> false
            }
            true -> false
        }

        when (rustEntry) {
            is RustGenerator.RustEntry.RustStruct -> {
                return when (parentIsRoot) {
                    true -> when (rustEntry.modulePath != null) {
                        true -> {
                            rustEntry.modulePath + RustGenerator.MOD_SEP + rustEntry.typeName
                        }
                        false -> rustModelModule(rustEntry.typeName)
                    }
                    // if the parent isn't the root, we need to resolve the module name for all parents
                    false -> when (rustEntry.modulePath != null) {
                        true -> {
                            // determine if this is an std module path, if not attach rustModelModule
                            val isStd = STD_TYPE_MODULE_PREFIX.any { rustEntry.modulePath!!.startsWith(it) }
                            when (isStd) {
                                true -> rustEntry.modulePath + RustGenerator.MOD_SEP + rustEntry.typeName
                                false -> rustModelModule(rustEntry.modulePath + RustGenerator.MOD_SEP + rustEntry.typeName)
                            }

                        }
                        false -> rustModelModule(rustModuleName(getFullyQualifiedRustTypeName(parent)) + RustGenerator.MOD_SEP + rustEntry.typeName)
                    }
                }
            }
            is RustGenerator.RustEntry.RustStringEnumeration -> {
                // these literally only exist in the context of their parent, we need to resolve it
                return rustModelModule(rustModuleName(getFullyQualifiedRustTypeName(parent)) + RustGenerator.MOD_SEP + rustEntry.enumName)
            }
            is RustGenerator.RustEntry.RustOneOf -> {
                // if the parent is skipped, we are the fully resolved path, else we're just nested
                if (parentIsSkip) {
                    // if the OneOf is unwrapped, it has the exact same name as the parent
                    return rustModelModule(getFullyQualifiedRustTypeName(parent))
                }
                // these literally only exist in the context of their parent, we need to resolve it
                return rustModelModule(rustModuleName(getFullyQualifiedRustTypeName(parent)) + RustGenerator.MOD_SEP + rustEntry.oneOfName)
            }
            is RustGenerator.RustEntry.RustOneOfParameter -> {
                // the fully qualified type is the target of the parameter
                return getFullyQualifiedRustTypeName(getNodeForRustEntry(rustEntry.rustType)!!)
            }
            is RustGenerator.RustEntry.RustParameter -> {
                // the fully qualified type is the target of the parameter
                return getFullyQualifiedRustTypeName(getNodeForRustEntry(rustEntry.rustType)!!)
            }
        }

    }

    private fun qualifiedNameToMapperName(name: String) =
        name.replace(RustGenerator.MOD_SEP, "_").replace("__", "_").lowercase()


    private fun getRustTypeName(rust: RustGenerator.RustEntry): String {
        return when (rust) {
            is RustGenerator.RustEntry.RustStruct -> {
                rust.typeName
            }
            is RustGenerator.RustEntry.RustParameter -> {
                getRustTypeName(rust.rustType)
            }
            is RustGenerator.RustEntry.RustStringEnumeration -> {
                // TODO LDe: Fully qualify?
                rust.enumName
            }
            is RustGenerator.RustEntry.RustOneOf -> {
                rust.oneOfName
            }
            is RustGenerator.RustEntry.RustOneOfParameter -> {
                getRustTypeName(rust.rustType)
            }
        }
    }

    private fun getRustFieldName(rust: RustGenerator.RustEntry): String {
        return when (rust) {
            is RustGenerator.RustEntry.RustParameter -> {
                rust.parameterName
            }
            else -> throw Exception("Handle $rust")
        }
    }

    private fun getProtoTypeName(proto: Proto3Generator.ProtoEntry): String {
        return when (proto) {
            is Proto3Generator.ProtoEntry.ProtoType -> {
                proto.typeName
            }
            is Proto3Generator.ProtoEntry.ProtoParameter -> {
                getProtoTypeName(proto.protoType)
            }
            is Proto3Generator.ProtoEntry.ProtoEnum -> {
                // TODO LDe: Fully qualify?
                proto.enumName
            }
            is Proto3Generator.ProtoEntry.ProtoOneOf -> {
                proto.oneOfName
            }
        }
    }

    private fun getProtoFieldName(proto: Proto3Generator.ProtoEntry): String {
        return when (proto) {
            is Proto3Generator.ProtoEntry.ProtoParameter -> {
                val name = proto.parameterName
                FORBIDDEN_PARAMETER_NAMES_PROTO[name] ?: name
            }
            else -> throw Exception("Handle $proto")
        }
    }

    private fun isOptionalRustParameter(rustLanguageType: RustGenerator.RustEntry): Boolean {
        return when (rustLanguageType) {
            is RustGenerator.RustEntry.RustParameter -> rustLanguageType.optional
            else -> false
        }
    }

    private fun isBoxRustParameter(rustLanguageType: RustGenerator.RustEntry): Boolean {
        return when (rustLanguageType) {
            is RustGenerator.RustEntry.RustParameter -> rustLanguageType.box
            is RustGenerator.RustEntry.RustOneOfParameter -> rustLanguageType.box
            else -> false
        }
    }

    private fun processNodeToProto(
        node: BaseNode, target: PrintWriter,
        level: Int = 0,
        parentNode: BaseNode? = null
    ) {
        val protoLanguageType = node.languageType[OutputLanguage.Proto] as Proto3Generator.ProtoEntry
        val rustLanguageType = node.languageType[OutputLanguage.Rust] as RustGenerator.RustEntry

        val qualifiedProtoTypeName = lookupProtoToRustType(getFullyQualifiedProtoTypeName(node))
        val qualifiedRustTypeName = getFullyQualifiedRustTypeName(node)

        node.clusteredTypes?.let { cluster ->
            when (cluster.isNotEmpty()) {
                true -> {
                    logger.info { "processNodeToProto: Encountered cluster ${node.clusteredTypes}" }

                    if (node.clusterHandled[OutputLanguage.RustToProtoMapper] == true) {
                        logger.info { "processNodeToProto: Encountered cluster ${node.clusteredTypes} is already handled" }
                        return@let
                    }
                    logger.info { "processNodeToProto: Encountered cluster ${node.clusteredTypes} is new" }
                    logger.info { "processNodeToProto: Adding cluster mappers all at once to map" }

                    (listOf(node) + cluster).forEach { clusterType ->
                        clusterType.clusterHandled[OutputLanguage.RustToProtoMapper] = true

                        // create mappers
                        val protoName = getFullyQualifiedProtoTypeName(clusterType)
                        val rustName = getFullyQualifiedRustTypeName(clusterType)

                        val functionName = mapFunctionName(qualifiedNameToMapperName(rustName))

                        val clusterTypeMapping = Mapping(rustName, protoName)
                        rustToProtoMapperMap[clusterTypeMapping] = functionName
                    }

                }
                false -> {}
            }
        }


        // lookup if mapper is already present
        val mapper = rustToProtoMapperMap[Mapping(
            qualifiedRustTypeName, qualifiedProtoTypeName
        )]

        val intoConversion = intoConversionsSet.contains(Mapping(
            qualifiedRustTypeName, qualifiedProtoTypeName
        ))

        when (val nodeType = node.nodeType) {
            is NodeType.Message -> {
                if ((mapper != null || intoConversion) && node.clusteredTypes == null) {
                    logger.error { "Not generating mapper from non clustered Message $qualifiedRustTypeName to $qualifiedProtoTypeName, already present" }
                    return
                }

                // separate parameter children from nested message children
                val parameters = node.children.filter { it.nodeType is NodeType.Parameter }.toList()
                val nestedTypes = node.children - parameters

                val rustLanguageTypeSkip = when (rustLanguageType) {
                    is RustGenerator.RustEntry.RustStruct -> rustLanguageType.skipType
                    else -> false
                }

                // check if nested is only a oneof, this is a special case in rust
                val isOnlyOneOfWrapper = nestedTypes.all { it.nodeType is NodeType.OneOf }
                        && nestedTypes.isNotEmpty()
                        && rustLanguageTypeSkip
                val isOneOf = node.nodeType is NodeType.OneOf

                // we need to generate nested types first
                nestedTypes.forEach { child ->
                    processNodeToProto(node = child, target = target, level = level, parentNode = node)
                }

                if (isOnlyOneOfWrapper) {
                    // bail out if this was only a oneof, but create mapper from the msg to the nested type
                    return
                }

                // create mapper
                val functionName = mapFunctionName(qualifiedNameToMapperName(qualifiedRustTypeName))

                rustToProtoMapperMap[Mapping(
                    qualifiedRustTypeName,
                    qualifiedProtoTypeName
                )] = functionName

                target.println(
                    mapFunctionStart(
                        level,
                        functionName = functionName,
                        targetType = qualifiedProtoTypeName,
                        sourceType = qualifiedRustTypeName,
                    )
                )

                target.println(
                    rustReturnStart(
                        level + 1,
                        targetType = qualifiedProtoTypeName
                    )
                )

                // only for non oneOfs
                if (!isOneOf) {
                    parameters.forEach { child ->
                        processNodeToProto(
                            node = child, target = target, level = level + 1,
                            parentNode = node
                        )
                    }
                }

                target.println(rustReturnEnd(level + 1))
                target.println(mapFunctionEnd(level))
            }
            is NodeType.Parameter -> {
                check(qualifiedProtoTypeName == qualifiedRustTypeName || intoConversion || mapper != null) {
                    "Cannot map from parameter $qualifiedRustTypeName to $qualifiedProtoTypeName, no mapper!"
                }

                val rustIsBoxed = isBoxRustParameter(rustLanguageType)

                when {
                    (!nodeType.optional && nodeType.list) || (nodeType.optional && nodeType.list) -> {
                        // there is no such thing as an optional (nullable) list, so these can be merged
                        target.println(
                            protoFromRustList(
                                indent = level,
                                rustField = getRustFieldName(rustLanguageType),
                                protoField = getProtoFieldName(protoLanguageType),
                                mappingFunction = mapper,
                            )
                        )
                    }
                    (nodeType.optional && !nodeType.list) -> {
                        target.println(
                            protoFromOptionalRust(
                                indent = level,
                                rustField = getRustFieldName(rustLanguageType),
                                protoField = getProtoFieldName(protoLanguageType),
                            )
                        )
                        target.println(
                            protoOptionalFromRust(
                                indent = level + 1,
                                mappingFunction = mapper,
                                into = intoConversion,
                                box = rustIsBoxed
                            )
                        )
                        target.println(protoOptionalFromRustNone(indent = level + 1))
                        target.println(protoOptionalFromRustEnd(indent = level))
                    }
                    (!nodeType.optional && !nodeType.list) -> {
                        // determine if parameter is optional in proto, because its not a primitive type
                        val qualifiedParameterType = getFullyQualifiedProtoTypeName(node)
                        val isOptionalTarget = !PROTO_COMPILED_TO_RUST_MAP.containsKey(qualifiedParameterType)

                        // determine if target is enum, we need to mangle that through as i32
                        val targetIsEnum = nodeType.parameterType is NodeType.StringEnumeration

                        when {
                            targetIsEnum -> {
                                val targetType = "as i32"
                                target.println(
                                    protoFromEnum(
                                        indent = level + 1,
                                        rustField = getRustFieldName(rustLanguageType),
                                        protoField = getProtoFieldName(protoLanguageType),
                                        enumConverter = targetType,
                                        mappingFunction = mapper,
                                        into = intoConversion,
                                    )
                                )
                            }
                            isOptionalTarget -> {
                                target.println(
                                    protoFromRustFieldOptionalTarget(
                                        indent = level,
                                        rustField = getRustFieldName(rustLanguageType),
                                        protoField = getProtoFieldName(protoLanguageType),
                                        mappingFunction = mapper,
                                        into = intoConversion,
                                        box = rustIsBoxed,
                                    )
                                )
                            }
                            else -> {
                                target.println(
                                    protoFromRustField(
                                        indent = level,
                                        rustField = getRustFieldName(rustLanguageType),
                                        protoField = getProtoFieldName(protoLanguageType),
                                        mappingFunction = mapper,
                                        into = intoConversion,
                                        box = rustIsBoxed,
                                    )
                                )
                            }
                        }

                    }
                }
            }
            is NodeType.StringEnumeration -> {
                // create mapper
                val functionName = mapFunctionName(qualifiedNameToMapperName(qualifiedRustTypeName))

                rustToProtoMapperMap[Mapping(
                    qualifiedRustTypeName, qualifiedProtoTypeName
                )] = functionName

                target.println(
                    mapFunctionStart(
                        level,
                        functionName = functionName,
                        targetType = qualifiedProtoTypeName,
                        sourceType = qualifiedRustTypeName,
                    )
                )

                target.println(rustFromProtoEnumStart(level + 1))
                nodeType.values.forEach { value ->
                    target.println(
                        protoFromRustEnumCase(
                            indent = level + 2,
                            targetType= qualifiedProtoTypeName,
                            targetValue= toProtoEnumName(value),
                            sourceType = qualifiedRustTypeName,
                            sourceValue = value
                        )
                    )
                }
                target.println(rustFromProtoEnumEnd(level + 1))
                target.println(mapFunctionEnd(level))
            }
            is NodeType.OneOf -> {
                check(getRustEntry(node) is RustGenerator.RustEntry.RustOneOf)
                val protoOneOf = getProtoEntry(node) as Proto3Generator.ProtoEntry.ProtoOneOf
                val rustOneOf = getRustEntry(node) as RustGenerator.RustEntry.RustOneOf

                // retrieve the fully qualified parent node name here for proto
                val oneOfQualifiedProtoTypeName = getFullyQualifiedProtoTypeName(parentNode!!)

                // create mapper
                val functionName = mapFunctionName(qualifiedNameToMapperName(qualifiedRustTypeName))

                rustToProtoMapperMap[Mapping(
                    qualifiedRustTypeName, oneOfQualifiedProtoTypeName
                )] = functionName

                target.println(
                    mapFunctionStart(
                        level,
                        functionName = functionName,
                        sourceType = qualifiedRustTypeName,
                        targetType = oneOfQualifiedProtoTypeName,
                    )
                )

                val rustTypeName = getRustTypeName(rustOneOf)
                val matchName = RustGenerator.toParameterName(rustTypeName)

                target.println(protoFromRustOneOfStart(level + 1, matchName))

                node.children.forEach { child ->
                    check(child.nodeType is NodeType.Parameter)

                    val rustChild = child.languageType[OutputLanguage.Rust]
                    val protoChild = child.languageType[OutputLanguage.Proto]

                    check(rustChild is RustGenerator.RustEntry.RustOneOfParameter)
                    check(protoChild is Proto3Generator.ProtoEntry.ProtoParameter)

                    val childProtoTypeName = lookupProtoToRustType(getFullyQualifiedProtoTypeName(child))
                    val childRustTypeName = getFullyQualifiedRustTypeName(child)

                    // generate oneof parameters
                    val childMapper = rustToProtoMapperMap[Mapping(
                        childRustTypeName, childProtoTypeName
                    )]

                    val childIntoConversion = intoConversionsSet.contains(
                        Mapping(
                            childRustTypeName, childProtoTypeName
                        )
                    )

                    check(childProtoTypeName == childRustTypeName || childIntoConversion || childMapper != null) {
                        "Cannot map from oneof parameter $childRustTypeName to $childProtoTypeName, no mapper!"
                    }

                    val protoCaseName = qualifiedProtoTypeName + RustGenerator.MOD_SEP + protoChild.parameterName.split('_')
                        .joinToString("", transform = String::capitalize)
                    val rustCaseName = qualifiedRustTypeName + RustGenerator.MOD_SEP + rustChild.parameterName
                    val rustIsBoxed = isBoxRustParameter(rustChild)

                    target.println(
                        protoFromRustOneOfCase(
                            indent = level + 2,
                            protoCase = protoCaseName,
                            rustCase = rustCaseName,
                            mappingFunction = childMapper,
                            into = childIntoConversion,
                            box = rustIsBoxed,
                            protoStructName = oneOfQualifiedProtoTypeName,
                            protoFieldName = matchName
                        )
                    )
                }

                target.println(protoFromRustOneOfEnd(level + 1))
                target.println(mapFunctionEnd(level))
            }
            is NodeType.BuiltinType -> {
                logger.debug { "Handle builtin type ${node.nodeName}" }
            }
            NodeType.Root -> TODO()
            null -> TODO()
        }

    }

    private fun processNodeToRust(
        node: BaseNode, target: PrintWriter,
        level: Int = 0,
        parentNode: BaseNode? = null
    ) {

        val protoLanguageType = node.languageType[OutputLanguage.Proto] as Proto3Generator.ProtoEntry
        val rustLanguageType = node.languageType[OutputLanguage.Rust] as RustGenerator.RustEntry

        val qualifiedProtoTypeName = lookupProtoToRustType(getFullyQualifiedProtoTypeName(node))
        val qualifiedRustTypeName = getFullyQualifiedRustTypeName(node)

        node.clusteredTypes?.let { cluster ->
            when (cluster.isNotEmpty()) {
                true -> {
                    logger.info { "processNodeToRust: Encountered cluster ${node.clusteredTypes}" }

                    if (node.clusterHandled[OutputLanguage.ProtoToRustMapper] == true) {
                        logger.info { "processNodeToRust: Encountered cluster ${node.clusteredTypes} is already handled" }
                        return@let
                    }
                    logger.info { "processNodeToRust: Encountered cluster ${node.clusteredTypes} is new" }
                    logger.info { "processNodeToRust: Adding cluster mappers all at once to map" }

                    (listOf(node) + cluster).forEach { clusterType ->
                        clusterType.clusterHandled[OutputLanguage.ProtoToRustMapper] = true

                        // create mappers
                        val protoName = getFullyQualifiedProtoTypeName(clusterType)
                        val rustName = getFullyQualifiedRustTypeName(clusterType)

                        val functionName = mapFunctionName(qualifiedNameToMapperName(protoName))

                        val clusterTypeMapping = Mapping(protoName, rustName)
                        protoToRustMapperMap[clusterTypeMapping] = functionName
                    }

                }
                false -> {}
            }
        }

        // lookup if mapper is already present
        val mapper = protoToRustMapperMap[Mapping(
            qualifiedProtoTypeName,
            qualifiedRustTypeName
        )]

        val intoConversion = intoConversionsSet.contains(Mapping(
            qualifiedProtoTypeName,
            qualifiedRustTypeName
        ))

        when (val nodeType = node.nodeType) {
            is NodeType.Message -> {
                if ((mapper != null || intoConversion) && node.clusteredTypes == null) {
                    logger.error { "Not generating mapper from non clustered Message $qualifiedProtoTypeName to $qualifiedRustTypeName, already present" }
                    return
                }

                // separate parameter children from nested message children
                val parameters = node.children.filter { it.nodeType is NodeType.Parameter }.toList()
                val nestedTypes = node.children - parameters

                val rustLanguageTypeSkip = when (rustLanguageType) {
                    is RustGenerator.RustEntry.RustStruct -> rustLanguageType.skipType
                    else -> false
                }

                // check if nested is only a oneof, this is a special case in rust
                val isOnlyOneOfWrapper = nestedTypes.all { it.nodeType is NodeType.OneOf }
                        && nestedTypes.isNotEmpty()
                        && rustLanguageTypeSkip
                val isOneOf = node.nodeType is NodeType.OneOf

                // we need to generate nested types first
                nestedTypes.forEach { child ->
                    processNodeToRust(node = child, target = target, level = level, parentNode = node)
                }

                if (isOnlyOneOfWrapper) {
                    // bail out if this was only a oneof, but create mapper from the msg to the nested type
                    return
                }

                // create mapper
                val functionName = mapFunctionName(qualifiedNameToMapperName(qualifiedProtoTypeName))

                protoToRustMapperMap[Mapping(
                    qualifiedProtoTypeName,
                    qualifiedRustTypeName
                )] = functionName

                target.println(
                    mapFunctionStartResult(
                        level,
                        functionName = functionName,
                        targetType = qualifiedRustTypeName,
                        sourceType = qualifiedProtoTypeName,
                    )
                )


                target.println(
                    rustReturnStartOk(
                        level + 1,
                        targetType = qualifiedRustTypeName
                    )
                )

                // only for non oneOfs
                if (!isOneOf) {
                    parameters.forEach { child ->
                        processNodeToRust(
                            node = child, target = target, level = level + 2,
                            parentNode = node
                        )
                    }
                }

                target.println(rustReturnEndOk(level + 1))
                target.println(mapFunctionEnd(level))

            }
            is NodeType.Parameter -> {
                check(qualifiedProtoTypeName == qualifiedRustTypeName || intoConversion || mapper != null) {
                    "Cannot map from parameter $qualifiedProtoTypeName to $qualifiedRustTypeName, no mapper!"
                }

                val rustIsBoxed = isBoxRustParameter(rustLanguageType)

                when {
                    (!nodeType.optional && nodeType.list) || (nodeType.optional && nodeType.list) -> {
                        // there is no such thing as an optional (nullable) list, so these can be merged
                        target.println(
                            rustFromProtoList(
                                indent = level,
                                rustField = getRustFieldName(rustLanguageType),
                                protoField = getProtoFieldName(protoLanguageType),
                                mappingFunction = mapper,
                            )
                        )
                    }
                    (nodeType.optional && !nodeType.list) -> {
                        target.println(
                            rustFromOptionalProto(
                                indent = level,
                                rustField = getRustFieldName(rustLanguageType),
                                protoField = getProtoFieldName(protoLanguageType),
                            )
                        )
                        target.println(
                            rustFromOptionalProtoSome(
                                indent = level + 1,
                                mappingFunction = mapper,
                                into = intoConversion,
                                box = rustIsBoxed
                            )
                        )
                        target.println(rustFromOptionalProtoNone(indent = level + 1))
                        target.println(rustFromOptionalProtoEnd(indent = level))
                    }
                    (!nodeType.optional && !nodeType.list) -> {
                        // determine if target is parameter in proto, because its not a primitive type
                        val qualifiedParameterType = getFullyQualifiedProtoTypeName(node)
                        val isOptionalSource = !PROTO_COMPILED_TO_RUST_MAP.containsKey(qualifiedParameterType)

                        // determine if target is enum, we need to fully resolve that for from_i32
                        val targetIsEnum = nodeType.parameterType is NodeType.StringEnumeration

                        when {
                            targetIsEnum -> {
                                val targetType = qualifiedProtoTypeName + RustGenerator.MOD_SEP + "from_i32"
                                target.println(
                                    rustFromOptionalProtoEnum(
                                        indent = level,
                                        rustField = getRustFieldName(rustLanguageType),
                                        protoField = getProtoFieldName(protoLanguageType),
                                        enumConverter = targetType
                                    )
                                )
                                target.println(
                                    rustFromOptionalProtoMandatorySome(
                                        indent = level + 1,
                                        mappingFunction = mapper,
                                        into = intoConversion,
                                    )
                                )
                                target.println(rustFromOptionalProtoMandatoryNone(indent = level + 1, fieldName = getProtoFieldName(protoLanguageType)))
                                target.println(rustFromOptionalProtoEnd(indent = level))
                            }
                            isOptionalSource -> {
                                target.println(
                                    rustFromOptionalProto(
                                        indent = level,
                                        rustField = getRustFieldName(rustLanguageType),
                                        protoField = getProtoFieldName(protoLanguageType),
                                    )
                                )

                                target.println(
                                    rustFromOptionalProtoMandatorySome(
                                        indent = level + 1,
                                        mappingFunction = mapper,
                                        into = intoConversion,
                                        box = rustIsBoxed,
                                    )
                                )
                                target.println(rustFromOptionalProtoMandatoryNone(indent = level + 1, fieldName = getProtoFieldName(protoLanguageType)))
                                target.println(rustFromOptionalProtoEnd(indent = level))
                            }
                            else -> {
                                target.println(
                                    rustFromProtoField(
                                        indent = level,
                                        rustField = getRustFieldName(rustLanguageType),
                                        protoField = getProtoFieldName(protoLanguageType),
                                        mappingFunction = mapper,
                                        into = intoConversion,
                                        box = rustIsBoxed,
                                    )
                                )
                            }
                        }
                    }
                }
            }
            is NodeType.StringEnumeration -> {

                // create mapper
                val functionName = mapFunctionName(qualifiedNameToMapperName(qualifiedProtoTypeName))

                protoToRustMapperMap[Mapping(
                    qualifiedProtoTypeName,
                    qualifiedRustTypeName
                )] = functionName

                target.println(
                    mapFunctionStartResult(
                        level,
                        functionName = functionName,
                        targetType = qualifiedRustTypeName,
                        sourceType = qualifiedProtoTypeName,
                    )
                )

                target.println(rustFromProtoEnumStart(level + 1))

                nodeType.values.forEach { value ->
                    target.println(
                        rustFromProtoEnumCase(
                            indent = level + 2,
                            sourceType = qualifiedProtoTypeName,
                            sourceValue = toProtoEnumName(value),
                            targetType = qualifiedRustTypeName,
                            targetValue = value
                        )
                    )
                }
//                target.println(rustFromProtoEnumElse(indent = level + 2))

                target.println(rustFromProtoEnumEnd(level + 1))
                target.println(mapFunctionEnd(level))

            }
            is NodeType.OneOf -> {
                check(getRustEntry(node) is RustGenerator.RustEntry.RustOneOf)
                val protoOneOf = getProtoEntry(node) as Proto3Generator.ProtoEntry.ProtoOneOf

                // retrieve the fully qualified parent node name here for proto
                val oneOfQualifiedProtoTypeName = getFullyQualifiedProtoTypeName(parentNode!!)

                // create mapper
                val functionName = mapFunctionName(qualifiedNameToMapperName(oneOfQualifiedProtoTypeName))

                protoToRustMapperMap[Mapping(
                    oneOfQualifiedProtoTypeName,
                    qualifiedRustTypeName
                )] = functionName

                target.println(
                    mapFunctionStartResult(
                        level,
                        functionName = functionName,
                        sourceType = oneOfQualifiedProtoTypeName,
                        targetType = qualifiedRustTypeName,
                    )
                )

                val protoTypeName = getProtoTypeName(protoOneOf)
                val matchName = RustGenerator.toParameterName(protoTypeName)

                target.println(rustFromProtoOneOfStart(level + 1, matchName))

                node.children.forEach { child ->
                    check(child.nodeType is NodeType.Parameter)

                    val rustChild = child.languageType[OutputLanguage.Rust]
                    val protoChild = child.languageType[OutputLanguage.Proto]

                    check(rustChild is RustGenerator.RustEntry.RustOneOfParameter)
                    check(protoChild is Proto3Generator.ProtoEntry.ProtoParameter)

                    val childProtoTypeName = lookupProtoToRustType(getFullyQualifiedProtoTypeName(child))
                    val childRustTypeName = getFullyQualifiedRustTypeName(child)

                    // generate oneof parameters
                    val childMapper = protoToRustMapperMap[Mapping(
                        childProtoTypeName,
                        childRustTypeName
                    )]

                    val childIntoConversion = intoConversionsSet.contains(Mapping(
                        childProtoTypeName,
                        childRustTypeName
                    ))

                    check(childProtoTypeName == childRustTypeName || childIntoConversion || childMapper != null) {
                        "Cannot map from oneof parameter $childProtoTypeName to $childRustTypeName, no mapper!"
                    }

                    val protoCaseName = qualifiedProtoTypeName + RustGenerator.MOD_SEP + protoChild.parameterName.split('_')
                        .joinToString("", transform = String::capitalize)
                    val rustCaseName = qualifiedRustTypeName + RustGenerator.MOD_SEP + rustChild.parameterName
                    val rustIsBoxed = isBoxRustParameter(rustChild)

                    val protoFieldName = RustGenerator.toParameterName(child.nodeName)

                    target.println(
                        rustFromProtoOneOfOptionalCase(
                            indent = level + 2,
                            protoCase = protoCaseName,
//                            protoField = protoFieldName,
                            rustCase = rustCaseName,
                            mappingFunction = childMapper,
                            into = childIntoConversion,
                            box = rustIsBoxed
                        )
                    )
                }

                target.println(rustFromProtoOneOfOptionalNone(indent = level + 2))

                target.println(rustFromProtoOneOfEnd(level + 1))
                target.println(mapFunctionEnd(level))

            }
            is NodeType.BuiltinType -> {
                logger.debug { "Handle builtin type ${node.nodeName}" }
            }
            NodeType.Root -> TODO()
            null -> TODO()
            else -> TODO()
        }

    }

    fun generate() {
        logger.info("Creating Rust <-> Proto Mapping")

//        // TODO LDe: Make output folder configurable
        val outputFolder = File("proto_rust_out")
        outputFolder.deleteRecursively()
        outputFolder.mkdirs()


        run {
            val protoToRustFileName = "proto_to_rust"
            val protoToRustFile = File(outputFolder, "$protoToRustFileName.rs")

            val os = ByteArrayOutputStream()

            // builtin mappers
            protoToRustFile.appendText(MAPPING_ERROR_ENUM)
            protoToRustFile.appendText(PROTO_TO_RUST_DURATION_MAPPER)
            protoToRustFile.appendText(PROST_TO_DUMMY_ANY_MAPPER)

            tree.children.forEach { node ->
                os.reset()

                PrintWriter(os).use { printWriter ->
                    processNodeToRust(node, level = 0, target = printWriter)
                }

                logger.debug { os.toString() }

                protoToRustFile.appendText(os.toString())
            }
        }

        run {
            val rustToProtoFileName = "rust_to_proto"
            val protoToRustFile = File(outputFolder, "$rustToProtoFileName.rs")

            val os = ByteArrayOutputStream()

            // builtin mappers
            protoToRustFile.appendText(DUMMY_ANY_TO_PROST_MAPPER)

            tree.children.forEach { node ->
                os.reset()

                PrintWriter(os).use { printWriter ->
                    processNodeToProto(node, level = 0, target = printWriter)
                }

                logger.debug { os.toString() }

                protoToRustFile.appendText(os.toString())
            }
        }
    }
}