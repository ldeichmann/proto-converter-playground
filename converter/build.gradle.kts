plugins {
    id("org.somda.sdc.proto_converter.deploy")
}

val baseMappers: Configuration by configurations.creating

val subprojects: Configuration by configurations.creating


dependencies {
    baseMappers(project(":kotlin-proto-base-mappers"))
    // more to come
    subprojects(project(":xmlprocessor"))
    api(baseMappers)
    implementation(subprojects)

    // https://mvnrepository.com/artifact/com.github.ajalt/clikt
    implementation(group = "com.github.ajalt", name = "clikt", version = "2.8.0")
}

tasks.withType<Jar> {
    // But why? It seems insane to have to explicitly depend on the
    // dependencies here so that the jars exist for this task.
    // That's why they're called dependencies, not wishes.
    // I suppose all tasks has their own dependencies, so it kinda makes sense,
    // super unintuitive though.
    dependsOn(baseMappers)
    dependsOn(subprojects)

    manifest {
        attributes["Main-Class"] = "org.somda.protosdc_converter.converter.MainKt"
    }

    // This line of code recursively collects and copies all of a project's files
    // and adds them to the JAR itself.
    // This resolves the following issue when launching the jar:
    // Exception in thread "main" java.lang.NoClassDefFoundError: kotlin/jvm/internal/Intrinsics
    from(configurations.runtimeClasspath.get().map { if (it.isDirectory) it else zipTree(it) })
}
