rootProject.name = "protosdc-converter"

include ("xmlprocessor", "kotlin-proto-base-mappers", "converter")
