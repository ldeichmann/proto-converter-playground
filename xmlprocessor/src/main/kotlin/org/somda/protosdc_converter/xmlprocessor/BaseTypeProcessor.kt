package org.somda.protosdc_converter.xmlprocessor

import org.apache.logging.log4j.kotlin.Logging
import org.xmlet.xsdparser.xsdelements.*

/**
 * Wrapper class to differentiate the return values of generators such as [BaseTypeProcessor.generateElement],
 * which can be both simple parameters and actual messages.
 */
sealed class ReturnType {

    /**
     * Generator created a new message
     */
    data class Message(val node: BaseNode) : ReturnType()

    /**
     * Generator created multiple parameters. These can also contain messages, but in that case the parameter
     * referencing the new message is part of the list.
     */
    data class Parameters(val nodes: List<BaseNode>) : ReturnType()

    /**
     * Extension containing the baseNode which was extended, as well as
     * all new parameters which are in the new extension.
     */
    data class Extension(val parameters: Parameters, val baseNode: BaseNode) : ReturnType()

}


class BaseTypeProcessor(
    private val tree: Node,
    private val attributeSuffix: String
) {

    companion object : Logging {

        const val RESOLUTION_ITERATIONS: Int = 10
        const val ENUM_TYPE_NAME: String = "EnumType"

        fun isList(element: XsdElement) = element.maxOccurs != null && element.maxOccurs == "unbounded"

        fun isOptional(element: XsdElement) = element.minOccurs == 0
    }

    /** map used to remember which [BaseNode] was created for which [Node], see [createBaseNode] */
    private val nodeMap: MutableMap<Node, BaseNode> = mutableMapOf()

    init {
        // create nodes for base
        BuiltinTypes.values().forEach {
            val node = createBaseNode(tree.searchQNameType(it.qname)!!, it.qname.localPart)
            node.nodeType = NodeType.BuiltinType(node, it.qname)
        }
    }

    /**
     * Creates a new [BaseNode].
     *
     * @param xmlNode to create node for
     * @param nodeName name of the new node
     * @param nodeType type of the new node, can be set later on
     * @param children of the new node, can be added later on
     * @param documentation of the node TODO LDe: Currently unused
     * @param register if true, this node will be registered as the node responsible for the [xmlNode] passed.
     *  Only register messages, not parameters, they're not the responsible nodes for a type!
     */
    private fun createBaseNode(
        xmlNode: Node?,
        nodeName: String,
        nodeType: NodeType? = null,
        children: MutableList<BaseNode> = mutableListOf(),
        documentation: String? = null,
        register: Boolean = true
    ): BaseNode {
        val baseNode = BaseNode(nodeName, children, documentation)
        baseNode.nodeType = nodeType
        if (register) {
            xmlNode?.let {
                nodeMap[xmlNode] = baseNode
            }
        }
        return baseNode
    }

    /**
     * Parses an xsd:simpleType's parameter, used in multiple branches of [generateSimpleType].
     */
    private fun resolveSimpleTypeParameter(
        itemType: String,
        simpleType: XsdSimpleType,
        child: Node,
        list: Boolean = false
    ): Pair<BaseNode, NodeType.Parameter> {
        val itemTypeQName = SchemaPostProcessor.resolveQName(itemType, simpleType)
        val itemTypeNode = child.searchQNameType(itemTypeQName)
        checkNotNull(itemTypeNode)
        val itemTypeBaseNode = nodeMap[itemTypeNode]!!
        val itemTypeBaseNodeType = itemTypeBaseNode.nodeType!!

        val member = createBaseNode(child, itemTypeBaseNode.nodeName)
        val parameter = NodeType.Parameter(member, itemTypeBaseNodeType, list = list)
        member.nodeType = parameter

        return Pair(member, parameter)
    }

    /**
     * Parses an xsd:simpleType node.
     */
    private fun generateSimpleType(
        xmlNode: Node,
        simpleType: XsdSimpleType,
        typeName: String? = null
    ): ReturnType {

        val parameters: List<BaseNode> = xmlNode.children.map { child ->
            when (val element = child.element) {
                is XsdRestriction -> {
                    when (val restriction = generateRestriction(child, element)) {
                        is ReturnType.Message -> {
                            // create a parameter for the new restriction type
                            val parameterNode = createBaseNode(child, restriction.node.nodeName, register = false)
                            val parameter = NodeType.Parameter(parameterNode, restriction.node.nodeType!!)
                            parameterNode.nodeType = parameter

                            listOf(restriction.node, parameterNode)
                        }
                        is ReturnType.Parameters -> {
                            restriction.nodes
                        }
                        else -> {
                            throw Exception("Cannot handle parameters here")
                        }
                    }
                }
                is XsdList -> {
                    val parameter = resolveSimpleTypeParameter(
                        itemType = element.itemType!!,
                        simpleType,
                        child = child,
                        list = true
                    )

                    listOf(parameter.first)
                }
                is XsdUnion -> {

                    simpleType.union.unionElements.forEach {
                        throw Exception("Handle inline simple types $it")
                    }

                    val unionNodeTypes = simpleType.union.memberTypesList.map {
                        resolveSimpleTypeParameter(itemType = it, simpleType, child = child, list = false).first
                    }.toList()

                    // this might be the one case where looking up the parent name is ok
                    val nodeName = typeName
                        ?: xmlNode.nodeName?.localPart
                        ?: xmlNode.parent?.nodeName?.localPart
                        ?: run {
                            throw Exception("Cannot determine union node name for $xmlNode")
                        }

                    val unionBaseNode = createBaseNode(child, nodeName)
                    val union = NodeType.OneOf(unionBaseNode)
                    unionBaseNode.nodeType = union

                    unionBaseNode.children.addAll(unionNodeTypes)

                    listOf(unionBaseNode)
                }
                else -> {
                    logger.error("Couldn't handle ${child.element}")
                    throw Exception("Couldn't handle ${child.element}")
                }
            }
        }.flatten()

        // a simple type without name is anonymous and not a message, but instead a collection of parameters

        when (xmlNode.nodeName != null) {
            true -> {
                val actualTypeName = typeName
                    ?: xmlNode.nodeName.localPart
                    ?: run {
                        throw Exception("Could not determine type name for complex type $xmlNode")
                    }

                val baseNodeType = NodeType.Message(null, xmlNode.nodeName)
                val baseNode = createBaseNode(xmlNode, actualTypeName, baseNodeType, mutableListOf())
                baseNodeType.parentNode = baseNode

                baseNode.children.addAll(parameters)

                return ReturnType.Message(baseNode)
            }
            false -> {
                return ReturnType.Parameters(parameters)
            }
        }
    }

    /**
     * Parses an xsd:complexType node.
     */
    private fun generateComplexType(
        xmlNode: Node,
        typeName: String? = null
    ): ReturnType {

        val potentialTypeName = xmlNode.nodeName?.localPart ?: "anonymous complex type"

        var baseType: BaseNode? = null

        val parameters: List<BaseNode> = xmlNode.children.map { child ->
            when (child.element) {
                is XsdSimpleContent -> {
                    logger.debug("Parsing XsdSimpleContent in $potentialTypeName")
                    when (val simpleContent = generateSimpleContent(child)) {
                        is ReturnType.Message -> {
                            listOf(simpleContent.node)
                        }
                        is ReturnType.Parameters -> {
                            simpleContent.nodes
                        }
                        is ReturnType.Extension -> {
                            // register this type as an extension of the base type
                            logger.info("Registering $potentialTypeName as extension of ${simpleContent.baseNode.nodeName}")
                            baseType = simpleContent.baseNode
                            simpleContent.parameters.nodes
                        }
                        else -> {
                            throw Exception("Cannot handle unknown return type $simpleContent")
                        }
                    }
                }
                is XsdComplexContent -> {
                    logger.debug("Parsing XsdComplexContent in $potentialTypeName")
                    when (val complexContent = generateComplexContent(child)) {
                        is ReturnType.Message -> {
                            listOf(complexContent.node)
                        }
                        is ReturnType.Parameters -> {
                            complexContent.nodes
                        }
                        is ReturnType.Extension -> {
                            // register this type as an extension of the base type
                            logger.info("Registering $potentialTypeName as extension of ${complexContent.baseNode.nodeName}")
                            baseType = complexContent.baseNode
                            complexContent.parameters.nodes
                        }
                        else -> {
                            throw Exception("Cannot handle unknown return type $complexContent")
                        }
                    }
                }
                is XsdSequence -> {
                    logger.debug("Parsing XsdSequence in $potentialTypeName")
                    generateSequence(child)
                }
                is XsdAttribute -> {
                    logger.debug("Parsing XsdAttribute in $potentialTypeName")
                    when (val result = generateAttribute(child, child.element)) {
                        is ReturnType.Message -> listOf(result.node)
                        is ReturnType.Parameters -> result.nodes
                        else -> throw Exception("Unknown type $result")
                    }
                }
                is XsdAttributeGroup -> {
                    logger.debug("Parsing XsdAttributeGroup in $potentialTypeName")
                    listOf(generateAttributeGroup(child, child.element))
                }
                else -> {
                    throw Exception("Unhandled type ${child.element}")
                }
            }
        }.flatten()

        // a complex type without name is anonymous and not a message, but instead a collection of parameters
        when (xmlNode.nodeName != null) {
            true -> {
                val actualTypeName = typeName
                    ?: xmlNode.nodeName.localPart
                    ?: run {
                        logger.warn("Could not determine type name for complex type $xmlNode")
                        "FIXME_ANONYMOUS_COMPLEX_TYPE"
                    }

                val baseNodeType = NodeType.Message(null, xmlNode.nodeName)
                baseNodeType.extensionBaseNode = baseType
                val baseNode = createBaseNode(xmlNode, actualTypeName, baseNodeType, mutableListOf())
                baseNodeType.parentNode = baseNode

                baseNode.children.addAll(parameters)
                nodeMap[xmlNode] = baseNode

                return ReturnType.Message(baseNode)
            }
            false -> {
                val returnTypeParameters = ReturnType.Parameters(parameters)
                return baseType?.let {
                    ReturnType.Extension(returnTypeParameters, it)
                } ?: returnTypeParameters
            }
        }
    }

    /**
     * Parses an xsd:simpleContent node.
     */
    private fun generateSimpleContent(xmlNode: Node): ReturnType {
        xmlNode.children.forEach { child ->
            return when (child.element) {
                is XsdExtension -> {
                    generateExtension(child, child.element)
                }
                is XsdRestriction -> {
                    generateRestriction(child, child.element)
                }
                else -> {
                    throw Exception("Handle generateSimpleContent ${child.element}")
                }
            }
        }
        throw Exception("Handle falling through cases in generateSimpleContent")
    }

    /**
     * Parses an xsd:complexContent node.
     */
    private fun generateComplexContent(xmlNode: Node): ReturnType {
        xmlNode.children.forEach { child ->
            when (child.element) {
                is XsdExtension -> {
                    return generateExtension(child, child.element)
                }
                else -> {
                    throw Exception("Handle generateComplexContent ${child.element}")
                }
            }
        }
        throw Exception("Handle falling through cases in generateComplexContent")
    }

    /**
     * Parses an xsd:attribute node.
     */
    private fun generateAttribute(xmlNode: Node, attribute: XsdAttribute): ReturnType {
        check(attribute.name != null) { "attribute name is null, makes no sense" }
        val attributeRequired = attribute.attributesMap[XsdAttribute.USE_TAG]?.let { it -> "required" == it }
            ?: false

        // attribute is defined here
        attribute.type?.let {
            val resolvedType = SchemaPostProcessor.resolveQName(it, attribute)
            val xmlTypeNode = xmlNode.searchQNameType(resolvedType)
            checkNotNull(xmlTypeNode)
            val typeParentNode: BaseNode = nodeMap[xmlTypeNode]!!
            val typeNode = typeParentNode.nodeType!!

            // if we're first level, generate a message, parameter otherwise
            xmlNode.parent?.element?.let {
                logger.info("${attribute.name} is embedded attribute, generating parameter")
                val baseNode = createBaseNode(xmlNode, attribute.name)
                val parameter =
                    NodeType.Parameter(baseNode, typeNode, wasAttribute = true, optional = !attributeRequired)
                baseNode.nodeType = parameter
                return ReturnType.Parameters(listOf(baseNode))
            } ?: run {
                logger.info("${attribute.name} is root level attribute, generating type with parameter")
                val baseNode = createBaseNode(xmlNode, attribute.name)
                val message = NodeType.Message(baseNode, xmlNode.nodeName)
                baseNode.nodeType = message

                val parameterNode = createBaseNode(xmlNode, attribute.name, register = false)
                val parameter = NodeType.Parameter(parameterNode, typeNode, optional = !attributeRequired)
                parameterNode.nodeType = parameter
                baseNode.children.add(parameterNode)

                return ReturnType.Message(baseNode)
            }
        }

        // find referenced attribute and use it TODO LDe: merge with the above, almost the same
        attribute.attributesMap[XsdAbstractElement.REF_TAG]?.let {
            val resolvedType = SchemaPostProcessor.resolveQName(it, attribute)
            // TODO LDe: Should filter for only attribute types
            val xmlTypeNode = xmlNode.searchQName(resolvedType)
            checkNotNull(xmlTypeNode)
            val typeParentNode: BaseNode = nodeMap[xmlTypeNode]!!
            val typeNode = typeParentNode.nodeType!!

            val baseNode = createBaseNode(xmlNode, attribute.name)
            val parameter = NodeType.Parameter(baseNode, typeNode, wasAttribute = true, optional = !attributeRequired)
            baseNode.nodeType = parameter
            return ReturnType.Message(baseNode)
        }

        // attributes may define new types for them to use as attributes
        xmlNode.children.forEach { child ->
            when (child.element) {
                is XsdSimpleType -> {
                    when (val simpleType = generateSimpleType(child, child.element)) {
                        is ReturnType.Message -> {
                            throw Exception("Cannot handle non anonymous complex type embedded in attribute")
                        }
                        is ReturnType.Parameters -> {
                            // to avoid naming clashes, append a configured suffix to types which are from attributes.
                            // this is somewhat hacky, and _can_ fail, we just don't care right now
                            // TODO LDe: Handle clashing names between attributes and fields in a more refined manner
                            val baseNode = createBaseNode(xmlNode, "${attribute.name}$attributeSuffix")
                            val message = NodeType.Message(baseNode, xmlNode.nodeName)
                            baseNode.nodeType = message
                            baseNode.children.addAll(simpleType.nodes)

                            val parameterNode = createBaseNode(xmlNode, attribute.name, register = false)
                            val parameter = NodeType.Parameter(
                                parameterNode,
                                message,
                                wasAttribute = true,
                                optional = !attributeRequired
                            )
                            parameterNode.nodeType = parameter

                            return ReturnType.Parameters(listOf(baseNode, parameterNode))

                        }
                    }
                    // TODO LDe: If we want to remove some indirections, this would have to handle restrictions as well
                }
                else -> {
                    throw Exception("Unhandled type ${child.element}")
                }
            }
        }

        throw Exception("Handle falling through cases in generateAttribute")
    }

    /**
     * Resolves a ref attribute on an element.
     *
     * @return Pair in which the key is the node of the referenced type, value the [NodeType].
     */
    private fun resolvedRefTag(ref: String, element: XsdAbstractElement, xmlNode: Node): Pair<BaseNode, NodeType> {
        val resolvedType = SchemaPostProcessor.resolveQName(ref, element)
        // TODO LDe: Should filter for only attribute group types
        val xmlTypeNode = xmlNode.searchQName(resolvedType)
        checkNotNull(xmlTypeNode)
        val typeParentNode: BaseNode = nodeMap[xmlTypeNode]!!
        val typeNode = typeParentNode.nodeType!!

        return Pair(typeParentNode, typeNode)
    }

    /**
     * Parses an xsd:attributeGroup node.
     */
    private fun generateAttributeGroup(xmlNode: Node, attributeGroup: XsdAttributeGroup): BaseNode {
        when {
            attributeGroup.attributesMap[XsdAbstractElement.REF_TAG] != null -> {
                val (typeParentNode, typeNode) = resolvedRefTag(
                    ref = attributeGroup.attributesMap[XsdAbstractElement.REF_TAG]!!,
                    element = attributeGroup,
                    xmlNode
                )

                val baseNode = createBaseNode(xmlNode, attributeGroup.name ?: typeParentNode.nodeName)
                val parameter = NodeType.Parameter(baseNode, typeNode, wasAttribute = true)
                baseNode.nodeType = parameter
                return baseNode
            }
            xmlNode.children.isNotEmpty() -> {
                val nodes: List<BaseNode> = xmlNode.children.map {
                    when (it.element) {
                        is XsdAttribute -> {
                            generateAttribute(it, it.element)
                        }
                        else -> {
                            throw Exception("Only attributes are allowed as children in attributeGroup")
                        }
                    }
                }.map {
                    when (it) {
                        is ReturnType.Message -> listOf(it.node)
                        is ReturnType.Parameters -> it.nodes
                        else -> throw Exception("Unknown return type $it")
                    }
                }.flatten()

                val baseNode = createBaseNode(xmlNode, attributeGroup.name)
                val type = NodeType.Message(baseNode, xmlNode.nodeName)
                baseNode.nodeType = type
                baseNode.children.addAll(nodes)
                return baseNode
            }
        }

        throw Exception("Handle falling through cases generateAttributeGroup")
    }

    /**
     * Parses an xsd:extension node.
     */
    private fun generateExtension(xmlNode: Node, extension: XsdExtension): ReturnType {

        // find referenced base type node
        val baseQName = SchemaPostProcessor.getExtensionBase(extension)
        val baseXmlNode = xmlNode.searchQNameType(baseQName)!!
        val baseNode = nodeMap[baseXmlNode]!!

        // create related node
        val extensionNode = createBaseNode(xmlNode, baseNode.nodeName)
        val extensionNodeType = NodeType.Parameter(extensionNode, baseNode.nodeType!!)
        extensionNode.nodeType = extensionNodeType

        // add child for referenced base type
        val extensionParameters = xmlNode.children.map { child ->
            when (child.element) {
                is XsdSequence -> {
                    generateSequence(child)
                }
                is XsdAttribute -> {
                    when (val result = generateAttribute(child, child.element)) {
                        is ReturnType.Message -> listOf(result.node)
                        is ReturnType.Parameters -> result.nodes
                        else -> throw Exception("Unknown return type $result")
                    }
                }
                is XsdAttributeGroup -> {
                    listOf(generateAttributeGroup(child, child.element))
                }
                is XsdAll, is XsdChoice -> throw Exception("Handle ${child.element}")
                else -> throw Exception("Handle ${child.element}")
            }
        }.flatten()

        return ReturnType.Extension(
            parameters = ReturnType.Parameters(listOf(extensionNode) + extensionParameters),
            baseNode = baseNode
        )
    }

    /**
     * Parses an xsd:element node.
     */
    private fun generateElement(xmlNode: Node, element: XsdElement, isRootLevel: Boolean = false): BaseNode {
        element.id?.let {
            throw Exception("Cannot handle id in generateElement")
        }

        when {
            element.type != null -> {
                val resolvedType = SchemaPostProcessor.resolveQName(element.type, element)
                val xmlTypeNode = xmlNode.searchQNameType(resolvedType)
                checkNotNull(xmlTypeNode)
                val typeParentNode: BaseNode = nodeMap[xmlTypeNode]!!
                val typeNode = typeParentNode.nodeType!!

                val baseNode = createBaseNode(xmlNode, element.name)

                when (isRootLevel) {
                    true -> {
                        logger.info("Creating message parent node for root element type ${element.name}")
                        // add a message node to wrap the parameter in
                        val message = NodeType.Message(baseNode, qname = xmlNode.nodeName)
                        baseNode.nodeType = message

                        val typeName = xmlNode.nodeName?.localPart!!
                        val parameterNode = createBaseNode(xmlNode, typeName, register = false)
                        val parameter = NodeType.Parameter(
                            parameterNode, typeNode,
                            list = isList(element), optional = isOptional(element)
                        )
                        parameterNode.nodeType = parameter

                        baseNode.children.add(parameterNode)

                        return baseNode

                    }
                    false -> {
                        val parameter = NodeType.Parameter(
                            baseNode, typeNode,
                            list = isList(element), optional = isOptional(element)
                        )
                        baseNode.nodeType = parameter
                        return baseNode
                    }
                }
            }
            XsdAbstractElement.REF_TAG in element.attributesMap -> {
                val (_, typeNode) = resolvedRefTag(
                    ref = element.attributesMap[XsdAbstractElement.REF_TAG]!!,
                    element = element,
                    xmlNode
                )

                val elementName = element.name ?: run {
                    val newName = "${typeNode.parent!!.nodeName}Element"
                    logger.warn("Could not extract element name, setting to $newName")
                    newName
                }
                val baseNode = createBaseNode(xmlNode, elementName)
                val parameter =
                    NodeType.Parameter(baseNode, typeNode, optional = isOptional(element), list = isList(element))
                baseNode.nodeType = parameter
                return baseNode
            }
        }

        val baseNode = createBaseNode(xmlNode, element.name)
        val message = NodeType.Message(baseNode, xmlNode.nodeName)
        baseNode.nodeType = message

        xmlNode.children.forEach { child ->
            when (child.element) {
                is XsdComplexType -> {
                    when (val returnType = generateComplexType(child)) {
                        is ReturnType.Extension -> {
                            baseNode.children.addAll(returnType.parameters.nodes)
                            message.extensionBaseNode = returnType.baseNode
                        }
                        is ReturnType.Parameters -> {
                            baseNode.children.addAll(returnType.nodes)
                        }
                        is ReturnType.Message -> {
                            throw Exception("Cannot process not anonymous complex type as child of element")
                        }
                    }
                }
                is XsdSimpleType -> {
                    when (val returnType = generateSimpleType(child, child.element)) {
                        is ReturnType.Parameters -> {
                            baseNode.children.addAll(returnType.nodes)
                        }
                        is ReturnType.Message -> {
                            throw Exception("Cannot process not anonymous simple type as child of element")
                        }
                    }
                }
                is XsdElement -> {
                    baseNode.children.add(generateElement(child, child.element))
                }
                else -> throw Exception("Handle else for ${child.element}")
            }
        }

        return baseNode
    }

    /**
     * Parses an xsd:sequence node.
     */
    private fun generateSequence(xmlNode: Node): List<BaseNode> {

        val nodes = mutableListOf<BaseNode>()

        xmlNode.children.forEach { child ->
            when (child.element) {
                is XsdSequence -> {
                    // first of all, I hate you for having a sequence in a sequence. I really just want you to know that
                    // matter of fact, I'm gonna log this as a warning for you
                    logger.warn("A sequence within a sequence is stupid.")
                    nodes.addAll(generateSequence(child))
                }
                is XsdElement -> {
                    val element = generateElement(child, child.element)
                    nodes.add(element)

                    when (val elementType = element.nodeType) {
                        is NodeType.Message -> {
                            // add a parameter for this node, since we've gotten an embedded message
                            logger.debug { "Generating parameter for element $element" }

                            val baseNode = createBaseNode(child, element.nodeName)
                            val parameter = NodeType.Parameter(
                                baseNode,
                                elementType,
                                list = isList(child.element),
                                optional = isOptional(child.element)
                            )
                            baseNode.nodeType = parameter

                            nodes.add(baseNode)
                        }
                    }
                }
                else -> throw Exception("Handle ${child.element}")
            }
        }

        // TODO LDe: Can we enable this?
//        check(nodes.isNotEmpty()) { "No nodes in sequence, sounds unreasonable" }

        return nodes
    }

    /**
     * Parses an xsd:restriction node.
     */
    private fun generateRestriction(xmlNode: Node, restriction: XsdRestriction): ReturnType {
        when {
            restriction.enumeration != null && restriction.enumeration.isNotEmpty() -> {

                val enumValues = restriction.enumeration.map {
                    check(it.value[0].isLetter()) {
                        "Enumeration values must begin with a letter, as we only support string enumerations." +
                                " ${it.value} does not"
                    }
                    it.value
                }.toList()
                val restrictionNode = createBaseNode(xmlNode, ENUM_TYPE_NAME)
                val nodeType = NodeType.StringEnumeration(restrictionNode, enumValues)
                restrictionNode.nodeType = nodeType

                return ReturnType.Message(restrictionNode)
            }

            restriction.base != null -> {
                // find the node for the base type and attach it to the node as a parameter
                val resolvedBase = SchemaPostProcessor.resolveQName(restriction.base, restriction)
                val baseXmlNode = xmlNode.searchQName(resolvedBase)!!
                val baseNode: BaseNode? = nodeMap[baseXmlNode]
                baseNode?.let {
                    val parameterNode = createBaseNode(xmlNode, baseNode.nodeName, register = false)
                    val parameter = NodeType.Parameter(parameterNode, baseNode.nodeType!!)
                    parameterNode.nodeType = parameter

                    return ReturnType.Parameters(listOf(parameterNode))
                }
                throw Exception("restriction.base failed to find base node for $xmlNode")
            }
        }

        throw Exception("generateRestriction fell through")
    }


    /**
     * Generates [NodeType]s for all "root" types,  i.e. the one below the root node.
     *
     * Every other type is nested within those and will be covered by the generators while traversing.
     */
    private fun generateRootTypes(): List<BaseNode> {
        val nodes = mutableListOf<BaseNode>()

        tree.children.forEach { it ->
            when (BuiltinTypes.isBuiltinType(it.nodeName!!)) {
                true -> {
                    logger.warn(
                        "Skipping builtin type. This shouldn't happen, you might've apparently loaded a document containing a builtin" +
                                " type such as xsd:string - did you try to convert the XML Schema itself? Or this is our fault, I don't know anymore" +
                                " Type is ${it.nodeName}"
                    )
                }
                false -> {
                    when (it.element) {
                        is XsdSimpleType -> {
                            when (val result = generateSimpleType(it, it.element)) {
                                is ReturnType.Message -> {
                                    logger.info("Generated simple type for ${result.node.nodeName}")
                                    nodes.add(result.node)
                                }
                                is ReturnType.Parameters -> {
                                    throw Exception("Cannot process anonymous complex type on root level")
                                }
                                else -> throw Exception("Unknown type $result")
                            }
                        }
                        is XsdComplexType -> {
                            when (val result = generateComplexType(it)) {
                                is ReturnType.Message -> {
                                    logger.info("Generated complex type for ${result.node.nodeName}")
                                    nodes.add(result.node)
                                }
                                is ReturnType.Parameters -> {
                                    throw Exception("Cannot process anonymous complex type on root level")
                                }
                                else -> throw Exception("Unknown type $result")
                            }
                        }
                        is XsdElement -> {
                            val result = generateElement(it, it.element, isRootLevel = true)
                            logger.info("Generated element for ${result.nodeName}")
                            nodes.add(result)
                        }
                        is XsdAttributeGroup -> {
                            val result = generateAttributeGroup(it, it.element)
                            logger.info("Generated attribute group for ${result.nodeName}")
                            nodes.add(result)
                        }
                        is XsdAttribute -> {
                            when (val result = generateAttribute(it, it.element)) {
                                is ReturnType.Message -> {
                                    logger.info("Generated attribute for ${result.node.nodeName}")
                                    nodes.add(result.node)
                                }
                                is ReturnType.Parameters -> {
                                    logger.info("Generated attribute for nodes ${result.nodes.joinToString { it.nodeName }}")
                                    nodes.addAll(result.nodes)
                                }
                                else -> throw Exception("Unknown type $result")
                            }
                        }
                        else -> {
                            if (it.element != null) {
                                throw Exception("Handle unknown type ${it.element::class.simpleName}")
                            } else {
                                throw Exception("Cannot parse null element passed")
                            }
                        }
                    }
                }
            }
        }
        return nodes
    }


    /**
     * Determine for all used types whether they have extensions which can take their place as well.
     *
     * This is essentially building a forest of extensions, and determines which of these forests need to be used
     * in the place of the original type to model object oriented inheritance behavior.
     *
     * @param rootNode root of the tree in which to resolve inheritance
     * @return map in which the key is a node, and the values are all types which extend the base node
     */
    //
    private fun buildInheritanceMap(rootNode: BaseNode): Map<BaseNode, List<BaseNode>> {
        // map in which the key is a base node, and the value is an extension
        val inheritanceMap = mutableMapOf<BaseNode, MutableSet<BaseNode>>()
        logger.info("Generating inheritance map")
        rootNode.traversePreOrder()
                // we only care about inheritance for nodes which are children of the root, or we would
                // generate OneOfs targeting nodes which are not in the scope of all types
            .filter { rootNode.children.contains(it) }
            .forEach { node ->
            when (val message = node.nodeType) {
                is NodeType.Message -> {
                    message.extensionBaseNode?.let { baseNode ->
                        inheritanceMap.getOrPut(baseNode) { mutableSetOf() }.add(node)
                    }
                }
            }
        }

        logger.debug { "Inheritance map: $inheritanceMap" }

        val flattenInheritance = inheritanceMap.mapValues { (base, _) ->
            getExtensionNodes(inheritanceMap, base).toList().distinct()
        }

        logger.debug { "Flattened inheritance map: $flattenInheritance" }

        // determine which of the types is actually used in a parameter. (Hint: Probably all of them. Shit.)
        val usedInheritanceParameters = rootNode.traversePreOrder().mapNotNull { node ->
            when (val nodeType = node.nodeType) {
                is NodeType.Parameter -> {
                    if (flattenInheritance.containsKey(nodeType.parameterType.parent)) {
                        nodeType.parameterType.parent
                    } else {
                        null
                    }
                }
                else -> null
            }
        }.toList().distinct()
        logger.debug { "Actually used parameters $usedInheritanceParameters" }

        return flattenInheritance
    }

    /**
     * Attaches newly generated OneOfs as nodes in the tree and replaces occurrences of the base type with OneOfs.
     *
     * @param rootNode root of the tree to which to attach OneOfs
     * @param inheritanceMap from which to derive new OneOf types
     */
    private fun attachInheritanceMap(rootNode: BaseNode, inheritanceMap: Map<BaseNode, List<BaseNode>>) {
        // generate nodes, attach them to the tree
        inheritanceMap.forEach { (baseNode, list) ->
            logger.info { "Creating inheritance OneOf for ${baseNode.nodeName}" }

            // create parameters for all types
            val parameters = list.map { node ->
                val parameterNode = createBaseNode(null, node.nodeName)
                val parameter = NodeType.Parameter(parameterNode, node.nodeType!!)
                parameterNode.nodeType = parameter
                parameterNode
            }.toList()

            // base node is in parameters too
            val parameterBaseNode = createBaseNode(null, baseNode.nodeName)
            val baseNodeParameter = NodeType.Parameter(baseNode, baseNode.nodeType!!)
            parameterBaseNode.nodeType = baseNodeParameter


            // create message holding OneOfs
            val oneOfNode = createBaseNode(tree, "${baseNode.nodeName}OneOf", null, register = false)
            val oneOf = NodeType.OneOf(oneOfNode)
            oneOfNode.nodeType = oneOf
            oneOfNode.children.addAll(listOf(parameterBaseNode) + parameters)

            val messageNode = createBaseNode(tree, "${baseNode.nodeName}OneOf", register = false)
            val message = NodeType.Message(messageNode, qname = null)
            messageNode.nodeType = message
            messageNode.children.add(oneOfNode)

            // replace all occurrences of the base type as a parameter with this oneOf Node
            // except of course in the types which inherit from the base
            rootNode.children.forEach {
                replaceWithOneOfs(it, baseNode, message)
            }

            rootNode.children.add(messageNode)
        }
    }

    /**
     * Resolves cycles in the nodes by clustering nodes which have circular references.
     *
     * Languages need to handle these clusters when traversing the tree, as can be retrieved from [BaseNode.clusteredTypes]
     *
     * @param childNodes unresolved nodes containing cycles
     * @param resolved already resolved nodes
     * @return Pair of lists, in which the first list contains all still unresolved nodes, while the second list
     *  contains all previously resolved nodes as well as the removed clusters.
     *  TODO LDe: I don't think returning unresolved nodes makes sense, we are failing in this method on unresolved nodes
     */
    private fun resolveClusters(
        childNodes: List<BaseNode>,
        resolved: List<BaseNode>
    ): Pair<List<BaseNode>, List<BaseNode>> {
        // copy lists
        var rootChildNodes = childNodes.toMutableList()
        var resolvedNodes = resolved.toMutableList()

        // if there is something left, find the cycles causing it
        var clusterIterCountCount = 0
        while (rootChildNodes.isNotEmpty() && clusterIterCountCount <= RESOLUTION_ITERATIONS) {
            clusterIterCountCount++
            val deClusterBaseNode = rootChildNodes[0]
            val cycle = findCycle(deClusterBaseNode, mutableListOf())
                ?: throw Exception("Could not find cycle from node $deClusterBaseNode, you sure you didn't mess up before?")

            // the last element is the the one that appeared twice, find the first occurrence
            val cycleEncounterNode = cycle[cycle.lastIndex]
            val firstIndex = cycle.indexOfFirst { it == cycleEncounterNode }

            val shortCycle: List<BaseNode> = cycle.subList(firstIndex, cycle.lastIndex)
            logger.warn {
                "Found cycle ${(shortCycle + cycleEncounterNode).joinToString(separator = " -> ") { it.nodeName }}," +
                        " clustering them"
            }

            logger.info("Setting types into a cluster")

            // add all nodes except the node itself to the cluster
            shortCycle.forEach { cycleNode ->
                cycleNode.clusteredTypes = (shortCycle - cycleNode).toList()
            }

            // now sort the nodes in the cycle alphabetically to have some sort of deterministic output
            val sortedShortCycle = shortCycle.sortedBy { it.nodeName }

            // retry resolution by removing one node from the cycle. try for each node until it works, or doesn't
            for (nodeToRemove in sortedShortCycle) {
                // move the base node into resolved types and check whether the resolving now finds a solution
                logger.debug { "Moving $nodeToRemove from unresolved to resolved types" }
                val resolvedCopy = resolvedNodes.toMutableList()
                val rootChildNodesCopy = rootChildNodes.toMutableList()

                resolvedCopy.add(nodeToRemove)
                rootChildNodesCopy.remove(nodeToRemove)

                // try solving again
                val (rootChildNodes2, resolvedNodes2) = resolveNodes(
                    iterations = RESOLUTION_ITERATIONS,
                    childNodes = rootChildNodesCopy,
                    resolved = resolvedCopy
                )

                if (resolvedCopy.size == resolvedNodes2.size) {
                    logger.info("Removing $nodeToRemove did not solve the clustering issue :(")
                    continue
                }

                // now check that the node we removed resolves as well
                val (_, resolvedNodes3) = resolveNodes(
                    iterations = 1,
                    childNodes = rootChildNodes2 + nodeToRemove,
                    resolved = resolvedNodes2 - nodeToRemove
                )

                require(resolvedNodes3.contains(nodeToRemove)) { "$nodeToRemove could still not be resolved, I'm out" }

                // cluster resolved, update nodes
                rootChildNodes = rootChildNodes2
                resolvedNodes = resolvedNodes2
                break
            }
        }

        return Pair(rootChildNodes, resolvedNodes)
    }

    /**
     * Generates a tree of [BaseNode] with a root, fully removing any inheritance in favor of composition.
     *
     * @return root node in which children are the types
     */
    fun generate(): BaseNode {

        val nodes = mutableListOf<BaseNode>()

        // add builtin type nodes, which have been created in init
        nodes.addAll(nodeMap.values.toList())

        // add all nodes below root
        nodes.addAll(generateRootTypes())
        logger.info("Processing to base nodes complete")

        val rootNode = createBaseNode(tree, "Converter: RootNode", NodeType.Root, nodes, register = false)

        val inheritanceMap = buildInheritanceMap(rootNode)
        attachInheritanceMap(rootNode = rootNode, inheritanceMap = inheritanceMap)

        // reorder the tree to represent the new hierarchy required
        val (rootChildNodes, resolvedNodes) = resolveNodes(
            iterations = RESOLUTION_ITERATIONS,
            childNodes = rootNode.children.toMutableList(),
            resolved = mutableListOf()
        )

        val (rootChildNodesNoCluster, resolvedNodesNoCluster) = resolveClusters(rootChildNodes, resolvedNodes)

        if (rootChildNodesNoCluster.isNotEmpty()) {
            throw Exception("Could not reorder tree, remaining elements: $rootChildNodesNoCluster")
        }

        rootNode.children.clear()
        rootNode.children.addAll(resolvedNodesNoCluster)

        logger.info("Final order of children: ${rootNode.children.joinToString { it.nodeName }}")

        return rootNode

    }

    /**
     * Replaces all occurrences of the base type as a parameter with a oneOf Node
     *
     * Except of course in the types which inherit from the base.
     *
     * @param node in which to insert OneOfs
     * @param baseNode the node which is to be replaced with a OneOf type
     * @param message the OneOf to use for occurrences of [baseNode]
     */
    private fun replaceWithOneOfs(node: BaseNode, baseNode: BaseNode, message: NodeType.Message) {
        node.children.forEach { childNode ->
            when (val nodeType = childNode.nodeType) {
                is NodeType.Message -> {
                    replaceWithOneOfs(childNode, baseNode, message)
                }
                is NodeType.Parameter -> {
                    var mustReplace = nodeType.parameterType.parent == baseNode
                    val parentMessage = node.nodeType as NodeType.Message // has to be!
                    // skip nodes which are actually extensions of our type, we don't want to replace there
                    mustReplace = mustReplace && parentMessage.extensionBaseNode != baseNode

                    if (mustReplace) {
                        logger.info { "Replacing parameter type ${baseNode.nodeName} in ${node.nodeName} with ${baseNode.nodeName}OneOf" }
                        nodeType.parameterType = message
                    }
                }
            }
        }
    }

    /**
     * Resolves all nodes and puts them in an order matching their resolution, i.e. sorted so dependencies come before their use.
     *
     * _Caveat: Does not handle cycles! See [resolveClusters] and [findCycle]._
     *
     * @param childNodes nodes which are not yet resolved
     * @param resolved nodes which are already resolved (i.e. builtins or previous runs before removing a cluster)
     * @return Pair of lists, in which the first list contains all still unresolved nodes, while the second list
     *  contains all previously and now resolved nodes in the correct order.
     */
    private fun resolveNodes(
        childNodes: List<BaseNode>,
        resolved: List<BaseNode>,
        iterations: Int
    ): Pair<MutableList<BaseNode>, MutableList<BaseNode>> {
        var iterationCount = 0
        var rootChildNodesCopy = childNodes.toMutableList()
        val resolvedNodesCopy = resolved.toMutableList()
        while (rootChildNodesCopy.isNotEmpty() && iterationCount < iterations) {
            rootChildNodesCopy = rootChildNodesCopy.mapNotNull { node ->
                if (isFullyResolvedNode(node, resolvedNodesCopy)) {
                    resolvedNodesCopy.add(node)
                    null
                } else {
                    node
                }
            }.toMutableList()
            iterationCount++
            logger.info("Reordering tree loop round $iterationCount done, resolved ${resolvedNodesCopy.size}, remaining ${rootChildNodesCopy.size}")
        }

        return Pair(rootChildNodesCopy, resolvedNodesCopy)
    }


    // flatten the map to contain every single extension down the tree for all nodes
    /**
     * Generates a sequence of all extensions for a given node.
     *
     * @param map of all known extension
     * @param node to find all extensions for
     * @return sequence of all extensions applicable to the node
     */
    private fun getExtensionNodes(map: Map<BaseNode, Set<BaseNode>>, node: BaseNode): Sequence<BaseNode> {
        return sequence {
            map[node]?.let { extensionNodes ->
                extensionNodes.forEach { extensionNode ->
                    yield(extensionNode)
                    yieldAll(getExtensionNodes(map, extensionNode))
                }
            }
        }
    }

    /**
     * Determines whether a parameter has already been resolved.
     *
     * @param node parent node of the parameter, used to check whether siblings are the parameter type
     * @param nodeType the parameter to check for
     * @param resolvedNodes list of already resolved nodes
     * @return true if parameter is resolved, false otherwise
     */
    private fun resolveParameter(node: BaseNode, nodeType: NodeType.Parameter, resolvedNodes: List<BaseNode>): Boolean {
        // resolved externally
        var resolved = resolvedNodes.contains(nodeType.parameterType.parent)
        logger.debug { "${node.nodeName} has resolved type: $resolved" }
        // builtin type
        resolved = resolved || nodeType.parameterType is NodeType.BuiltinType
        logger.debug { "${node.nodeName} has builtin (or resolved) type: $resolved" }
        // resolved within the same scope of the message
        resolved = resolved || node.children.contains(nodeType.parameterType.parent)
        logger.debug { "${node.nodeName} has nested (or builtin or resolved) type: $resolved" }
        if (!resolved) {
            logger.warn {
                "Could not resolve parameter ${nodeType.parent?.nodeName} in ${node.nodeName}," +
                        " type ${nodeType.parameterType.parent} was unresolved"
            }
        }
        return resolved
    }

    /**
     * Determines whether a node has been fully resolved.
     *
     * _This can only be used for message nodes!_
     *
     * @param node to resolve message for
     * @param resolvedNodes list of already resolved nodes
     * @return true if message is fully resolved, false otherwise
     */
    private fun isFullyResolvedNode(node: BaseNode, resolvedNodes: List<BaseNode>): Boolean {
        val filtered = node.children.filter { childNode ->
            when (val nodeType = childNode.nodeType) {
                is NodeType.Parameter -> {
                    !resolveParameter(node, nodeType, resolvedNodes)
                }
                is NodeType.Message -> {
                    !isFullyResolvedNode(childNode, resolvedNodes)
                }
                is NodeType.OneOf -> {
                    !isFullyResolvedNode(childNode, resolvedNodes)
                }
                is NodeType.BuiltinType, is NodeType.StringEnumeration -> false // remove nodes which have no deps
                else -> true // unknown types should not be resolved blindly
            }
        }.toList()
        return filtered.isEmpty()
    }

    /**
     * Recursively finds the *longest* cycle starting from the given base node.
     *
     * @param node to start cycle search from
     * @param visited list containing the current search path of nodes
     * @return list of nodes mapping the path which was used do find the cycle, null if no cycle was found
     */
    private fun findCycle(node: BaseNode, visited: MutableList<BaseNode>): List<BaseNode>? {
        // we don't care about OneOfs, they're nested inside messages, we count those
        if (node.nodeType !is NodeType.OneOf) {
            if (visited.contains(node)) {
                // cycle found, add this node so we can determine the cycle in the return value
                visited.add(node)
                return visited
            }
            visited.add(node)
        }

        // collect all cycles, find the longest  one
        return node.children.mapNotNull { childNode ->
            when (val nodeType = childNode.nodeType) {
                is NodeType.Parameter -> {
                    findCycle(nodeType.parameterType.parent!!, visited.toMutableList())
                }
                is NodeType.Message -> {
                    findCycle(childNode, visited.toMutableList())
                }
                is NodeType.OneOf -> {
                    findCycle(childNode, visited.toMutableList())
                }
                else -> null
            }
        }.takeIf { it.isNotEmpty() }
            // use longest cycle
            ?.reduce { acc, list -> if (acc.size > list.size) acc else list }
    }
}