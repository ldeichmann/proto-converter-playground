package org.somda.protosdc_converter.xmlprocessor

import javax.xml.namespace.QName

object Constants {
    const val XSD_NAMESPACE = "http://www.w3.org/2001/XMLSchema"
    const val PM_NAMESPACE = "http://standards.ieee.org/downloads/11073/11073-10207-2017/participant"
    const val EXT_NAMESPACE = "http://standards.ieee.org/downloads/11073/11073-10207-2017/extension"

    fun xsdQName(type: String) = QName(XSD_NAMESPACE, type)
}
