package org.somda.protosdc_converter.xmlprocessor

import org.apache.logging.log4j.kotlin.Logging
import org.xmlet.xsdparser.xsdelements.XsdAbstractElement
import org.xmlet.xsdparser.xsdelements.XsdComplexType
import org.xmlet.xsdparser.xsdelements.XsdSimpleType
import javax.xml.namespace.QName

class Node(
    val nodeName: QName?,
    val element: XsdAbstractElement?,
    var typeParentNode: Node? = null,
    val typeChildNodes: MutableList<Node> = mutableListOf()
) : Cloneable {

    companion object : Logging

    var parent: Node? = null
    var children: MutableList<Node> = mutableListOf()

    fun addChild(node: Node) {
        children.add(node)
        node.parent = this
    }

    fun searchQName(qname: QName): Node? {
        val qnames = searchQNameSequence(qname).distinct().toList()
        if (qnames.size == 1) {
            return qnames[0]
        }
        return null
    }

    fun searchQNameType(qname: QName): Node? {
        val qnames = searchQNameSequence(qname).distinct().filter {
            it.element is XsdComplexType
                    || it.element is XsdSimpleType
                    || it.nodeName?.let { name -> BuiltinTypes.isBuiltinType(name) } ?: false
        }.toList()
        if (qnames.size == 1) {
            return qnames[0]
        }
        return null
    }

    private fun searchQNameSequence(qname: QName): Sequence<Node> {
        return sequence {
            yieldAll(searchQNameSiblings(qname))
            parent?.let {
                yieldAll(it.searchQNameSequence(qname))
            }
            // we're at the root, search all children
            if (parent == null) {
                yieldAll(searchQNameInChildren(qname))
            }
        }
    }

    private fun searchQNameInChildren(qname: QName, leftOf: Node? = null): Sequence<Node> {
        return sequence {
            children.forEach {
                if (it === leftOf) {
                    // only search left of this node
                    return@forEach
                }
                it.nodeName?.let { childValue ->
                    if (qname == childValue) {
                        yield(it)
                    }
                }
            }
        }
    }

    private fun searchQNameSiblings(qname: QName): Sequence<Node> {
        return sequence {
            parent?.let {
                yieldAll(it.searchQNameInChildren(qname, this@Node))
            }
        }
    }

    public override fun clone(): Node {
        return clone(parent)
    }

    private fun clone(parent: Node?): Node {
        // create the new node first
        val newNode = Node(nodeName, element)
        newNode.parent = parent

        // then clone all children
        children.forEach {
            newNode.children.add(it.clone(newNode))
        }

        // do not clone type children list, instead reattach children to parents once we clone children
        val newParentTypeRef = typeParentNode?.let { it.nodeName?.let { name -> newNode.searchQName(name) } }
        newParentTypeRef?.let {
            it.typeChildNodes.add(newNode)
            newNode.typeParentNode = it
        }

        return newNode
    }

    fun traverseInorder(): Sequence<Node> {
        return sequence {
            children.forEach {
                yieldAll(it.traverseInorder())
            }
            yield(this@Node)
        }
    }

    override fun toString(): String {
        return nodeName?.toString() ?: "" + super.toString()
    }
}
