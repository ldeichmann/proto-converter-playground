package org.somda.protosdc_converter.xmlprocessor

import org.apache.logging.log4j.kotlin.Logging
import org.xmlet.xsdparser.core.XsdParser
import org.xmlet.xsdparser.xsdelements.*
import org.xmlet.xsdparser.xsdelements.elementswrapper.UnsolvedReference
import org.xmlet.xsdparser.xsdelements.visitors.XsdComplexTypeVisitor
import org.xmlet.xsdparser.xsdelements.visitors.XsdExtensionVisitor
import org.xmlet.xsdparser.xsdelements.visitors.XsdRestrictionsVisitor
import java.util.stream.Stream
import javax.xml.namespace.QName
import kotlin.streams.asSequence
import kotlin.streams.toList

class SchemaPostProcessor(
    private val filePath: String,
    private val loopThreshold: Int
) {
    private val parser: XsdParser = XsdParser(filePath)
    companion object : Logging {
        var dependencyTree = Node(null, null)

        fun resolveQName(name: String, element: XsdAbstractElement) =
            when (name.contains(':')) {
                true -> {
                    val split = name.split(":")
                    if (split.size != 2) throw Exception("Unexpected number of split elements for $name")
                    QName(resolveNamespace("${XsdAbstractElement.XMLNS}:${split[0]}", element), split[1])
                }
                false -> QName(resolveNamespace(XsdAbstractElement.XMLNS, element), name)
            }

        fun resolveName(name: String, element: XsdAbstractElement) =
            QName(resolveNamespace(XsdAbstractElement.TARGET_NAMESPACE, element), name)

        fun resolveNamespace(attrName: String, element: XsdAbstractElement): String =
            element.attributesMap[attrName] ?: (resolveNamespace(
                attrName,
                (element.parent ?: throw Exception("Namespace for prefix attribute $attrName missing"))
            ))


        fun getExtensionBase(type: XsdExtension): QName {
            return type.base?.let {
                resolveName(it.name, it)
            } ?: run {
                val field = XsdExtension::class.java.getDeclaredField("base")
                field.isAccessible = true
                val refBase = field.get(type)
                if (refBase !is UnsolvedReference) {
                    throw Exception("Could not get extension base from type $type")
                }
                resolveQName(refBase.ref, type)
            }
        }
    }

    init {
        BuiltinTypes.values().forEach {
            val node = Node(it.qname, null)
            dependencyTree.addChild(node)
        }
    }

    fun resolve() {
        logger.info("Resolving...")
        resolveAllOf(
            unresolvedSimpleTypes = createUnresolved(),
            unresolvedComplexTypes = createUnresolved(),
            unresolvedElements = createUnresolved(),
            unresolvedAttributeGroups = createUnresolved(),
            unresolvedAttributes = createUnresolved()
        )

//        logger.info("Resolved QNames, grouped by namespaces...")
//        parser.resultXsdSchemas.forEach { xmlSchema ->
//            resolvedTypes
//                .filter { it.namespaceURI == xmlSchema.targetNamespace }
//                .forEach(::println)
//        }
    }

    private fun resolveSimpleType(type: XsdSimpleType?, parent: Node): Boolean {
        if (type == null) {
            return true
        }

        val node = createNode(type, parent)

        val resolved = resolveRestriction(type.restriction, node) &&
                resolveUnion(type.union, node) &&
                resolveList(type.list, node)

        return attachToParent(resolved, node, parent)
    }

    private fun resolveAttribute(type: XsdAttribute, parent: Node): Boolean {
        val node = createNode(type, parent)
        val resolved = when (type.type) {
            null -> resolveSimpleType(type.xsdSimpleType, node)
            else -> parent.searchQName(resolveQName(type.type, type)) != null
        } && resolveFromAttrRef(type, node)

        return attachToParent(resolved, node, parent)
    }

    private fun resolveAttributeGroup(type: XsdAttributeGroup, parent: Node): Boolean {
        val node = createNode(type, parent)
        val resolved = resolveAttributedType(type.attributeGroups, type.allAttributes.toList(), node)
        return attachToParent(resolved, node, parent)
    }

    private fun resolveComplexType(type: XsdComplexType?, parent: Node): Boolean {
        if (type == null) {
            return true
        }
        val node = createNode(type, parent)
        val resolved = resolveSimpleContent(type.simpleContent, node) &&
                resolveComplexContent(type.complexContent, node) &&
                (type.xsdChildElement?.let {
                    resolveGroup(type.childAsGroup, node) &&
                            resolveAll(type.childAsAll, node) &&
                            resolveChoice(type.childAsChoice, node) &&
                            resolveSequence(type.childAsSequence, node)
                } ?: true) &&
                resolveAttributedType(extractAttributeGroup(type), extractAttributes(type), node)
        return attachToParent(resolved, node, parent)
    }

    private fun resolveFromAttrType(type: XsdAbstractElement, tree: Node) =
        type.attributesMap[XsdAbstractElement.TYPE_TAG]?.let {
            tree.searchQNameType(resolveQName(it, type)) != null
        } ?: true

    private fun resolveFromAttrRef(type: XsdAbstractElement, tree: Node) =
        type.attributesMap[XsdAbstractElement.REF_TAG]?.let {
            tree.searchQName(resolveQName(it, type)) != null
        } ?: true



    private fun resolveElements(types: Stream<XsdElement>?, tree: Node): Boolean {
        return types?.let { theTypes ->
            val elements = theTypes.toList()
            return@let elements.all { resolveElement(it, tree) }
        } ?: true
    }

    private fun resolveSimpleTypes(types: Stream<XsdSimpleType>?, tree: Node) =
        types?.let { theTypes -> theTypes.allMatch { resolveSimpleType(it, tree) } } ?: true

    private fun resolveElement(type: XsdElement, parent: Node): Boolean {
        val node = createNode(type, parent)
        val resolved = resolveFromAttrRef(type, node) &&
                resolveFromAttrType(type, node) &&
                resolveSimpleType(type.xsdSimpleType, node) &&
                resolveComplexType(type.xsdComplexType, node)
        return attachToParent(resolved, node, parent)
    }

    private fun resolveSimpleContent(type: XsdSimpleContent?, parent: Node): Boolean {
        if (type == null) {
            return true
        }
        val node = createNode(type, parent)
        val resolved = resolveRestriction(type.xsdRestriction, node) && resolveExtension(type.xsdExtension, node)
        return attachToParent(resolved, node, parent)
    }


    private fun resolveComplexContent(type: XsdComplexContent?, parent: Node): Boolean {
        if (type == null) {
            return true
        }
        val node = createNode(type, parent)
        val resolved = resolveRestriction(type.xsdRestriction, node) && resolveExtension(type.xsdExtension, node)
        return attachToParent(resolved, node, parent)
    }

    private fun resolveGroups(types: Stream<XsdGroup>?, tree: Node): Boolean =
        types?.let { theTypes -> theTypes.allMatch { resolveGroup(it, tree) } } ?: true

    private fun resolveGroup(type: XsdGroup?, parent: Node): Boolean {
        if (type == null) {
            return true
        }
        val node = createNode(type, parent)
        val resolved = resolveFromAttrRef(type, node) &&
                resolveAll(type.childAsAll, node) &&
                resolveChoice(type.childAsChoice, node) &&
                resolveSequence(type.childAsSequence, node)
        return attachToParent(resolved, node, parent)
    }

    private fun resolveAll(type: XsdAll?, parent: Node): Boolean {
        if (type == null) {
            return true
        }
        val node = createNode(type, parent)
        val resolved = resolveElements(type.childrenElements, node)
        return attachToParent(resolved, node, parent)
    }

    private fun resolveChoices(types: Stream<XsdChoice>?, tree: Node): Boolean =
        types?.let { theTypes -> theTypes.allMatch { resolveChoice(it, tree) } } ?: true

    private fun resolveChoice(type: XsdChoice?, parent: Node): Boolean {
        if (type == null) {
            return true
        }
        val node = createNode(type, parent)
        val resolved = resolveElements(type.childrenElements, node) &&
                resolveGroups(type.childrenGroups, node) &&
                resolveChoices(type.childrenChoices, node) &&
                resolveSequences(type.childrenSequences, node)
        return attachToParent(resolved, node, node)
    }

    private fun resolveSequences(types: Stream<XsdSequence>?, tree: Node): Boolean =
        types?.let { theTypes -> theTypes.allMatch { resolveSequence(it, tree) } } ?: true

    private fun resolveSequence(type: XsdSequence?, parent: Node): Boolean {
        if (type == null) {
            return true
        }
        val node = createNode(type, parent)
        val resolved = resolveElements(type.childrenElements, node) &&
                resolveGroups(type.childrenGroups, node) &&
                resolveChoices(type.childrenChoices, node) &&
                resolveSequences(type.childrenSequences, node)
        return attachToParent(resolved, node, parent)
    }

    private fun resolveExtension(type: XsdExtension?, parent: Node): Boolean {
        if (type == null) {
            return true
        }
        val node = createNode(type, parent)

        val baseQName = getExtensionBase(type)
        val parentTypeNode = parent.searchQName(baseQName)
        parentTypeNode?.let {
            parent.typeParentNode = it
            it.typeChildNodes.add(parent)
        } ?: return false

        val resolved = (type.xsdChildElement?.let {
            resolveGroup(type.childAsGroup, node) &&
                    resolveAll(type.childAsAll, node) &&
                    resolveChoice(type.childAsChoice, node) &&
                    resolveSequence(type.childAsSequence, node)
        } ?: true) && resolveAttributedType(extractAttributeGroup(type), extractAttributes(type), node)
        return attachToParent(resolved, node, parent)
    }

    private fun resolveRestriction(type: XsdRestriction?, parent: Node): Boolean {
        if (type == null) {
            return true
        }
        val node = createNode(type, parent)
        val resolved = parent.searchQName(resolveQName(type.base, type)) != null &&
                resolveAttributedType(extractAttributeGroup(type), extractAttributes(type), parent)
        return attachToParent(resolved, node, parent)
    }

    private fun resolveAttributedType(
        attrGroups: List<XsdAttributeGroup>,
        attrs: List<XsdAttribute>,
        tree: Node
    ): Boolean =
        (attrGroups
            .fold(true) { acc, attrGroup -> acc && resolveAttributeGroup(attrGroup, tree) }) &&
                (attrs
                    .fold(true) { acc, attr -> acc && resolveAttribute(attr, tree) })

    private fun resolveUnion(type: XsdUnion?, parent: Node): Boolean {
        if (type == null) {
            return true
        }
        val node = createNode(type, parent)
        val resolved = (type.memberTypesList?.let {
            it.fold(true) { acc, str -> acc && parent.searchQName(resolveQName(str, type)) != null }
        } ?: true) && (type.unionElements?.let { resolveSimpleTypes(it.stream(), parent) } ?: true)
        return attachToParent(resolved, node, parent)
    }

    private fun resolveList(type: XsdList?, parent: Node): Boolean {
        if (type == null) {
            return true
        }
        val node = createNode(type, parent)
        val resolved = (type.itemType?.let {
            parent.searchQName(resolveQName(it, type)) != null
        } ?: true) && resolveSimpleType(type.xsdSimpleType, parent)
        return attachToParent(resolved, node, parent)
    }

    private fun resolveAllOf(
        unresolvedSimpleTypes: MutableMap<QName, XsdSimpleType>,
        unresolvedComplexTypes: MutableMap<QName, XsdComplexType>,
        unresolvedElements: MutableMap<QName, XsdElement>,
        unresolvedAttributeGroups: MutableMap<QName, XsdAttributeGroup>,
        unresolvedAttributes: MutableMap<QName, XsdAttribute>
    ) {
        var count = 0
        while ((unresolvedSimpleTypes.isNotEmpty()
                    || unresolvedComplexTypes.isNotEmpty()
                    || unresolvedElements.isNotEmpty()
                    || unresolvedAttributeGroups.isNotEmpty()
                    || unresolvedAttributes.isNotEmpty())
            && count++ < loopThreshold
        ) {
            singleResolveLoop(unresolvedSimpleTypes) { type, treeCopy -> resolveSimpleType(type, treeCopy) }
            singleResolveLoop(unresolvedComplexTypes) { type, treeCopy -> resolveComplexType(type, treeCopy) }
            singleResolveLoop(unresolvedElements) { type, treeCopy -> resolveElement(type, treeCopy) }
            singleResolveLoop(unresolvedAttributes) { type, treeCopy -> resolveAttribute(type, treeCopy) }
            singleResolveLoop(unresolvedAttributeGroups) { type, treeCopy -> resolveAttributeGroup(type, treeCopy) }
            logger.info("loop round $count")
        }

        if (unresolvedSimpleTypes.isNotEmpty())
            throw Exception("Unresolved simple types (${unresolvedSimpleTypes.size}): $unresolvedSimpleTypes")

        if (unresolvedAttributes.isNotEmpty())
            throw Exception("Unresolved attributes (${unresolvedAttributes.size}): $unresolvedAttributes")

        if (unresolvedAttributeGroups.isNotEmpty())
            throw Exception("Unresolved attribute groups (${unresolvedAttributeGroups.size}): $unresolvedAttributeGroups")

        if (unresolvedComplexTypes.isNotEmpty())
            throw Exception("Unresolved complex types (${unresolvedComplexTypes.size}): $unresolvedComplexTypes")

        if (unresolvedElements.isNotEmpty())
            throw Exception("Unresolved elements (${unresolvedElements.size}): $unresolvedElements")
    }

    private fun createNode(type: XsdAbstractElement, parent: Node): Node {
        val node = if (type is XsdNamedElements && type.name != null) {
            if (type is XsdAttribute) {
                // attributes in biceps are unqualified
                // TODO: Detect this
                Node(QName(type.name), type)
            } else {
                Node(resolveName(type.name, type), type)
            }
        } else {
            Node(null, type)
        }

        node.parent = parent
        return node
    }

    private fun attachToParent(resolved: Boolean, node: Node, parent: Node): Boolean {
        if (resolved) {
            parent.addChild(node)
        }
        return resolved
    }


    private inline fun <reified T> singleResolveLoop(
        unresolvedSource: MutableMap<QName, T>,
        doResolve: (T, Node) -> Boolean
    ) {
        val unresolvedSourceCopy: MutableMap<QName, T> = HashMap(unresolvedSource)
        for ((qName, type) in unresolvedSourceCopy) {
            val nodeCopy = dependencyTree.clone()
            if (doResolve(type, nodeCopy)) {
                dependencyTree = nodeCopy
                unresolvedSource.remove(qName)
                logger.info("Resolved: $qName")
            }
        }
    }

    private inline fun <reified T : XsdNamedElements> createUnresolved(): MutableMap<QName, T> {
        val allTypes: MutableMap<QName, T> = HashMap()
        parser.resultXsdSchemas.forEach { schema ->
            val tns = schema.targetNamespace
            val elements: Map<QName, T> = schema.xsdElements
                .filter { element: XsdAbstractElement? -> element is T }
                .map { element: XsdAbstractElement? -> element as T? }
                .map { QName(tns, it!!.name) to it }
                .asSequence().toMap()
            allTypes.putAll(elements)
        }
        return allTypes
    }

    private fun extractAttributeGroup(element: XsdAbstractElement): List<XsdAttributeGroup> =
        when (element) {
            is XsdComplexType -> (element.visitor as XsdComplexTypeVisitor).attributeGroups.map { it.element as XsdAttributeGroup }.toList()
            is XsdRestriction -> (element.visitor as XsdRestrictionsVisitor).attributeGroups.map { it.element as XsdAttributeGroup }.toList()
            is XsdExtension -> (element.visitor as XsdExtensionVisitor).attributeGroups.map { it.element as XsdAttributeGroup }.toList()
            else -> emptyList()
        }

    private fun extractAttributes(element: XsdAbstractElement): List<XsdAttribute> =
        when (element) {
            is XsdComplexType -> (element.visitor as XsdComplexTypeVisitor).attributes.map { it.element as XsdAttribute }.toList()
            is XsdRestriction -> (element.visitor as XsdRestrictionsVisitor).attributes.map { it.element as XsdAttribute }.toList()
            is XsdExtension -> (element.visitor as XsdExtensionVisitor).attributes.map { it.element as XsdAttribute }.toList()
            else -> emptyList()
        }
}