plugins {
    `java-library`
    id("org.somda.sdc.proto_converter.deploy")
}

dependencies {
    // https://mvnrepository.com/artifact/com.github.xmlet/xsdParser
    implementation(group = "com.github.xmlet", name = "xsdParser", version = "1.0.28")
}