plugins {
    java
    kotlin("jvm") version "1.5.20"
}

val baseVersion = "0.0.3"
val buildId: String? = System.getenv("CI_PIPELINE_IID")

// append build id if present, make sure releases don't have build ids!
val actualVersion = buildId?.let { "${baseVersion}.${buildId}" } ?: baseVersion
val actualGroup = "org.somda.sdc.protosdc_converter"


group = actualGroup
version = actualVersion


allprojects {
    apply(plugin = "java")
    apply(plugin = "org.jetbrains.kotlin.jvm")

    repositories {
        mavenLocal()
        mavenCentral()
    }

    group = actualGroup
    version = actualVersion

    dependencies {
        implementation(kotlin("stdlib-jdk8"))

        // https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-api-kotlin
        implementation(group = "org.apache.logging.log4j", name = "log4j-api-kotlin", version = "1.0.0")

        // https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-api-kotlin
        implementation(group = "org.apache.logging.log4j", name = "log4j-core", version = "2.14.1")

        // https://mvnrepository.com/artifact/org.jetbrains.kotlin/kotlin-reflect
        implementation(group = "org.jetbrains.kotlin", name = "kotlin-reflect", version = "1.5.20")

        // https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter-api
        testImplementation(group = "org.junit.jupiter", name = "junit-jupiter-api", version = "5.7.1")
    }

    configure<JavaPluginConvention> {
        sourceCompatibility = JavaVersion.VERSION_1_8
    }

    tasks {
        compileKotlin {
            kotlinOptions.jvmTarget = "1.8"
        }
        compileTestKotlin {
            kotlinOptions.jvmTarget = "1.8"
        }
    }
}
